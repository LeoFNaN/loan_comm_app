   var oTable;
    $(document).ready(function() {
        oTable = $('#clients').dataTable( {
        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
          "sLengthMenu": "_MENU_ records per page",
          "sProcessing": "<img src='/assets/img/loader.gif'> processing..."
        },
        "aaSorting": [[3, 'desc']],
        "bProcessing": true,
        "bServerSide": false,
        "bStateSave" : true,
        "sAjaxSource": action,
        "cache" : true,
        "fnStateSaveParams": function (settings, data) {    
            if( data.selectedValue == 'All Statuses' || data.selectedValue == '' || data.selectedValue == 'All'){  
              data.selectedValue = 'All';       
            }
            else{
              data.selectedValue = $( "#statusSelect option:selected" ).val();       
            }
          },
          "fnStateLoadParams": function (settings, data) {  
              if( data.selectedValue == 'All Statuses' || data.selectedValue == '' || data.selectedValue == 'All'){  
                $("#statusSelect").val('All');
              }
              else{
                $("#statusSelect").val(data.selectedValue);
              }
          },
        "fnDrawCallback": function ( oSettings ) {
                $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
                $(".iframeQuickStatus").colorbox({iframe:true, width:"77%", height:"60%", current:' ', title:' ', transition: 'elastic', onClosed:function(){ location.reload(true); } });
          }

      })

          // Clear the filter. Unlike normal filters in Datatables,
          // custom filters need to be removed from the afnFiltering array.
          $('#clearFilter').on('click', function(e){
	            e.preventDefault();
	            oTable.fnFilterClear();
	            $("#statusSelect").val("All");
	            oTable.fnDraw();

          });

         $('select#statusSelect').on('change',function(){
            //var selectedValue = $(this).val();
            var selectedValue = $( "#statusSelect option:selected" ).text();
            if( selectedValue == 'All Statuses' || selectedValue == '')
            {
              oTable.fnFilterClear();
            }else{
              oTable.fnFilter("^"+selectedValue+"$",4, true); //Exact value, column, reg  
            }
        }
      );

    });
