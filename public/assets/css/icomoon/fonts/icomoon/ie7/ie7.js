/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referring to this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-home': '&#xe600;',
		'icon-home2': '&#xe601;',
		'icon-newspaper': '&#xe602;',
		'icon-pencil': '&#xe638;',
		'icon-pencil2': '&#xe639;',
		'icon-camera': '&#xe603;',
		'icon-camera2': '&#xe612;',
		'icon-phone': '&#xe613;',
		'icon-address-book': '&#xe604;',
		'icon-notebook': '&#xe605;',
		'icon-clock': '&#xe614;',
		'icon-calendar': '&#xe606;',
		'icon-print': '&#xe607;',
		'icon-mobile': '&#xe615;',
		'icon-drawer': '&#xe608;',
		'icon-drawer2': '&#xe609;',
		'icon-bubble': '&#xe63a;',
		'icon-bubbles': '&#xe63b;',
		'icon-bubbles2': '&#xe63c;',
		'icon-bubble2': '&#xe63d;',
		'icon-bubbles3': '&#xe63e;',
		'icon-bubbles4': '&#xe63f;',
		'icon-users': '&#xe60a;',
		'icon-users2': '&#xe60b;',
		'icon-user': '&#xe60c;',
		'icon-meter': '&#xe60d;',
		'icon-meter2': '&#xe60e;',
		'icon-dashboard': '&#xe60f;',
		'icon-remove': '&#xe640;',
		'icon-minus': '&#xe617;',
		'icon-plus': '&#xe618;',
		'icon-console': '&#xe610;',
		'icon-share': '&#xe611;',
		'icon-mail': '&#xe61a;',
		'icon-googleplus3': '&#xe616;',
		'icon-facebook2': '&#xe619;',
		'icon-instagram': '&#xe61b;',
		'icon-twitter2': '&#xe61d;',
		'icon-feed2': '&#xe620;',
		'icon-vimeo2': '&#xe622;',
		'icon-tumblr2': '&#xe62a;',
		'icon-yahoo': '&#xe62b;',
		'icon-skype': '&#xe62c;',
		'icon-reddit': '&#xe62d;',
		'icon-linkedin': '&#xe62e;',
		'icon-lastfm': '&#xe62f;',
		'icon-lastfm2': '&#xe630;',
		'icon-delicious': '&#xe631;',
		'icon-stumbleupon': '&#xe632;',
		'icon-pinterest2': '&#xe634;',
		'icon-paypal2': '&#xe636;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
