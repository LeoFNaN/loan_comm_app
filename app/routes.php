<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

//Log all Queries in and out of the app and log in Log file
if (Config::get('database.log', true)) {

    Event::listen('illuminate.query', function($query, $bindings, $time, $name) {

        $data = compact('bindings', 'time', 'name');

        // Format binding data for sql insertion
        foreach ($bindings as $i => $binding) {

            if ($binding instanceof \DateTime) {
           
                $bindings[$i] = $binding->format('\'Y-m-d H:i:s\''); 

            } else if (is_string($binding)) { 

                    $bindings[$i] = "'$binding'"; 
            
            }
        
        }

        // Insert bindings into query
        $query = str_replace(array('%', '?'), array('%%', '%s'), $query);

        $query = vsprintf($query, $bindings); 

        Log::info($query, $data);

    });

}

Route::when('*', 'csrf', array('post', 'put', 'patch', 'delete'));

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    # Comment Management
    Route::get('comments/{comment}/edit', 'AdminCommentsController@getEdit');
    Route::post('comments/{comment}/edit', 'AdminCommentsController@postEdit');
    Route::get('comments/{comment}/delete', 'AdminCommentsController@getDelete');
    Route::post('comments/{comment}/delete', 'AdminCommentsController@postDelete');
    Route::controller('comments', 'AdminCommentsController');

    # Blog Management
    Route::get('blogs/{post}/show', 'AdminBlogsController@getShow');
    Route::get('blogs/{post}/edit', 'AdminBlogsController@getEdit');
    Route::post('blogs/{post}/edit', 'AdminBlogsController@postEdit');
    Route::get('blogs/{post}/delete', 'AdminBlogsController@getDelete');
    Route::post('blogs/{post}/delete', 'AdminBlogsController@postDelete');
    Route::controller('blogs', 'AdminBlogsController');

    # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::post('users/create', 'AdminUsersController@postCreate');

    Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'AdminUsersController@postDelete');
    Route::controller('users', 'AdminUsersController');

    # Clients Views
    Route::get('client/{user}/show/', array('as' => 'showListOfClientsAdmin', 'uses' => 'AdminUsersController@getShowList'));
    
    Route::get('client/{client}/status', array('as' => 'clientStatusAdmin', 'uses' => 'AdminStatusUpdatesController@index'));
    Route::post('client/{client}/status/', 'AdminStatusUpdatesController@store');

    Route::get('client/{client}/quickstatus', 'AdminStatusUpdatesController@getQuickStatus');
    Route::post('client/{client}/quickstatus', 'AdminStatusUpdatesController@postQuickStatus');

    Route::controller('client', 'AdminUsersController');

    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::controller('roles', 'AdminRolesController');

    # Admin Dashboard
    Route::get('/dashboard', array('as' => 'adminDashboard', 'uses' => 'AdminUsersController@getIndex'));

    # Admin Dashboard
    Route::controller('/', 'AdminDashboardController');
});

/** ------------------------------------------
 *  Agent/User Routes
 *  ------------------------------------------
 */
//:: User Account Login Route ::
Route::post('user/login', 'UserController@postLogin');

#User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');
Route::post('user/reset/{token}', 'UserController@postReset');

Route::group(array('prefix' => 'user', 'before' => 'auth'), function()
{

    Route::get('/dashboard', array('as' => 'agentDashboard','uses' => 'UserController@getIndex'));
    Route::get('/profile', 'UserController@getSettings');
    Route::post('/{user}/edit', 'UserController@postEdit');

    Route::get('/client/{client}/status', array('as' => 'clientStatus', 'uses' => 'StatusUpdatesController@index'));
    Route::post('/client/{client}/status', 'StatusUpdatesController@store');

    Route::get('/client/create', array('as' => 'createClient','uses' => 'UserController@getCreateClient'));
    Route::post('/client/create', 'UserController@postClient');

    Route::get('/marketing', array('as' => 'marketing', function(){return View::make('site/user/dash/marketing');}));
});

# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

/** ------------------------------------------
 *  Inhouse Routes
 *  ------------------------------------------
 */

Route::group(array('prefix' => 'inhouse', 'before' => 'auth'), function()
{
    
    Route::get('/dashboard', array('as' => 'inhouseDashboard','uses' => 'InhouseController@getIndex'));

    #Display List of Agents
    Route::get('/agents', array('as' => 'showAgents', 'uses' => 'InhouseController@getShowAgents'));

    #Display agent clients
    Route::get('/show/{agent}', array('as' => 'showListOfClients', 'uses' => 'InhouseController@getShowList'));
    Route::get('/client/{client}/status', array('as' => 'clientStatusInhouse', 'uses' => 'InhouseStatusUpdatesController@index'));
    Route::post('/client/{client}/status', 'InhouseStatusUpdatesController@store');
    Route::get('/client/{client}/quickstatus', 'InhouseStatusUpdatesController@getQuickStatus');
    Route::post('/client/{client}/quickstatus', 'InhouseStatusUpdatesController@postQuickStatus');

    #User profile
    Route::get('/profile', 'InhouseController@getProfile');

    #Agent profile
    Route::get('/agent/{agent}/profile', array('as'=>'agentProfile', 'uses' => 'InhouseController@getAgentProfile'));

});

# User RESTful Routes
Route::controller('inhouse', 'InhouseController'); //For getListData function

/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */

//:: Application Routes ::
# Filter for detect language
Route::when('help','detectLang');
Route::resource('help', 'ContactController');

Route::get('about', function(){return View::make('site/about');});
Route::get('termsandconditions', function(){return View::make('site/termsandconditions');});
Route::get('privacypolicy', function(){return View::make('site/privacypolicy');});

Route::get('/signup', 'UserController@getCreate');
Route::post('/signup', 'UserController@postIndex');

# Posts - routes to site/blog/index
Route::get('news', array('before' => 'detectLang','uses' => 'BlogController@getIndex')); 
Route::get('{postSlug}', 'BlogController@getView');
Route::post('{postSlug}', 'BlogController@postView');

# Home Page - Contact Form
Route::post('/','ContactController@storeHomeInfo');

# Index Page - Last route, no matches
Route::get('/', function(){ return View::make('index');} );