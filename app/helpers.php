<?php

//Common Functions

    function formatPhone($string)
    {

        $number = trim(preg_replace('#[^0-9]#s', '', $string));

        $length = strlen($number);
        if($length == 7) {

            $regex = '/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/';
            $replace = '$1-$2';

        } elseif($length == 10) {

            $regex = '/([0-9]{3})([0-9]{3})([0-9]{4})/';
            $replace = '($1) $2-$3';

        } elseif($length == 11) {

            $regex = '/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/';
            $replace = '$1 ($2) $3-$4';

        }

        $formatted = preg_replace($regex, $replace, $number);

        return $formatted;
        
    } 

?>