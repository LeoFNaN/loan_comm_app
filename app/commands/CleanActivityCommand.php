<?php 

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Activity;
use Carbon\Carbon;

class CleanActivityCommand extends Command {

	/**
	 * The console command name.
	 * @var string
	 */
	protected $name = 'mvfloans:clean-activity';

	/**
	 * The console command description.
	 * @var string
	 */
	protected $description = 'Cleans activity log in DB older then 3 months.';

	/**
	 * Activity Model DI
	 * @var activity
	 */
	protected $activity;

	/**
	 * Create a new command instance.
	 * @return void
	 */
	public function __construct(Activity $activity)
	{
		parent::__construct();
		$this->activity = $activity;
	}

	/**
	 * Execute the console command.
	 * @return mixed
	 */
	public function fire()
	{
		$this->activity->cleanActivity();
		$this->info('Activity cleaning complete - '.Carbon::Now());
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
