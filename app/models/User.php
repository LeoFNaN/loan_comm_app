<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\ConfideUserInterface;
use Zizaco\Entrust\HasRole;
use Carbon\Carbon;
//use Illuminate\Auth\UserInterface;
//use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements ConfideUserInterface
{
    
    use ConfideUser;
    use HasRole;

    /**
     * Get list of Recent Activity
     * Args: $idList, $timeFrame, $pagination
     * @return list
     */
    public function getRecentActivityTimeFrame($idList, $timeFrame, $pagination)
    {

        if ($timeFrame == 0) { //alltime

            $results = DB::table('activity_log')
            ->select('activity_log.user_id', 'activity_log.content_type', 'activity_log.content_id', 'activity_log.details', 'activity_log.action', 'activity_log.created_at', 'users.first_name')
            ->join('users', function($join){
                    $join->on('activity_log.user_id', '=', 'users.id');
                })
            ->whereIn('content_id', $idList)
            ->orderBy('activity_log.created_at', 'DESC')
            ->paginate($pagination);

        } else {

            $results = DB::table('activity_log')
            ->select('activity_log.user_id', 'activity_log.content_type', 'activity_log.content_id', 'activity_log.details', 'activity_log.action', 'activity_log.created_at', 'users.first_name')
            ->join('users', function($join){
                    $join->on('activity_log.user_id', '=', 'users.id');
                })
            ->whereIn('content_id', $idList)
            ->whereBetween('activity_log.created_at', array(Carbon::now()->subDays($timeFrame), Carbon::now()))
            ->orderBy('activity_log.created_at', 'DESC')
            ->paginate($pagination);

        }

        return $results;

    }

    /**
     * Get list of Recent Activity for Admin
     * Args: $timeFrame, $pagination
     * @return list
     */
    public function getRecentActivityTimeFrameAdmin($timeFrame, $pagination)
    {

        if ($timeFrame == 0) { //alltime

            $results = DB::table('activity_log')
            ->select('activity_log.user_id', 'activity_log.content_type', 'activity_log.content_id', 'activity_log.details', 'activity_log.action', 'activity_log.created_at', 'users.first_name')
            ->leftJoin('users', function($leftJoin){
                $leftJoin->on('activity_log.user_id', '=', 'users.id');
            })
            ->orderBy('activity_log.created_at', 'DESC')
            ->paginate($pagination);

        } else {

            $pastTime = Carbon::now()->subDays($timeFrame);
            $today = Carbon::now();

            $results = DB::table('activity_log')
            ->select('activity_log.user_id', 'activity_log.content_type', 'activity_log.content_id', 'activity_log.details', 'activity_log.action', 'activity_log.created_at', 'users.first_name')
            ->leftJoin('users', function($leftJoin){
                $leftJoin->on('activity_log.user_id', '=', 'users.id');
            })
            ->whereBetween('activity_log.created_at', array($pastTime, $today))
            ->orderBy('activity_log.created_at', 'DESC')
            ->paginate($pagination);

        }

        return $results;

    }

    /**
     * Get the date the user was created.
     *
     * @return string
     */
    public function joined()
    {

        return String::date(Carbon::createFromFormat('Y-n-j G:i:s', $this->created_at));

    }

    /**
     * Save roles inputted from multiselect
     * @param $inputRoles
     */
    public function saveRoles($inputRoles)
    {

        if(! empty($inputRoles)) {

            $this->roles()->sync($inputRoles);

        } else {

            $this->roles()->detach();

        }

    }

    /**
     * Returns user's current role ids only.
     * @return array|bool
     */
    public function currentRoleIds()
    {

        $roles = $this->roles;

        $roleIds = false;

        if( !empty( $roles ) ) {

            $roleIds = array();

            foreach( $roles as &$role ) {

                $roleIds[] = $role->id;

            }

        }

        return $roleIds;

    }

    /**
     * Redirect after auth.
     * If ifValid is set to true it will redirect a logged in user.
     * @param $redirect
     * @param bool $ifValid
     * @return mixed
     */
    public function checkAuthAndRedirect($redirect, $ifValid=false)
    {

        // Get the user information
        $user = Auth::user();
        $redirectTo = false;

        if (empty($user->id) && ! $ifValid) { // Not logged in redirect, set session.
        
            Session::put('loginRedirect', $redirect);
        
            $redirectTo = Redirect::to('user/login')
                ->with( 'notice', Lang::get('user/user.login_first') );
        
        } elseif(!empty($user->id) && $ifValid) { // Valid user, we want to redirect.
        
            if ($user->hasRole('admin')) {

                $redirectTo = Redirect::to('admin/users');
            
            } elseif ($user->hasRole('inhouse')) {

                $redirectTo = Redirect::to('inhouse/dashboard');
            
            } else {

                $redirectTo = Redirect::to($redirect);
            
            }

        }

        return array($user, $redirectTo);

    }

    public function currentUser()
    {

        return Confide::user();

    }

    public function mtrandstr($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') 
    {

            for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);

            return $s;

     }

     
    /**
     * Get user by username
     * @param $username
     * @return mixed
     */
    public function getUserByUsername( $username )
    {

        return $this->where('username', '=', $username)->first();

    }

    /**
     * Get user by Email
     * @param $username
     * @return mixed
     */
    public function getUserByEmail( $email )
    {

        return $this->where('email', '=', $email)->first();

    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {

        return $this->email;
        
    }

    /**
     * Get the agent count for display
     *
     * @return string
     */
    public function getAgentCount()
    {

        return $this->select('office_id')->where('office_id', '<>', 1)->count('office_id');

    }

    /**
    * Relationships
    */
    public function statuses()
    {
        return $this->hasMany('StatusUpdate');
    }

    public function clients()
    {
        return $this->hasMany('Client');
    }

    public function assignedAgents()
    {
        return $this->belongsTo('AssignedAgent');
    }

    public function activityLog()
    {
        return $this->hasMany('ActivityLog');
    }

    public function leadReport()
    {
        return $this->hasOne('LeadReport');
    }

}