<?php

class LeadReport extends \Eloquent {

    protected $fillable = [];

    /*
    * Initial lead_reports record creation
    * This is called when the Agent first signs up/created
    */
    public function createLeadReportRecord($agentID)
    {

        $alreadyExists = $this->select('user_id')->where('user_id', '=' , $agentID)->first();

        if (count($alreadyExists)) {

            return 'Error: Already Exists';

        }

        $this->user_id = $agentID;
        $this->gross_count = 0;
        $this->active_count = 0;
        $this->process_count = 0;
        $this->process_rate = 0;
        $this->funded_count = 0;
        $this->funded_rate = 0;
        $this->failure_count = 0;
        $this->failure_rate = 0;
        $this->ovr_quality = 0;

        $this->save();

        return 'Success';
    }

    /*
    * Increase Counts and Process_count
    * When agent submits a new lead
    */
    public function newLeadSubmit($agentID)
    {

        //Record Check
        $updateAgent = $this->select()->where('user_id', '=', $agentID)->first();

        if (count($updateAgent)) {

            DB::statement('UPDATE lead_reports 
                SET gross_count = gross_count + 1, active_count = active_count + 1, process_count = process_count +1 
                WHERE user_id = '.$agentID);

            $this->ratesMath($agentID);
            
            return 'Success';
            
        }
        
        return 'Error';

    }

    /*
    * Migrate Agents already in the system into the Lead Reports table
    * Create record, get total count of clients, tally up their stages, update counts
    * One and done Method
    */
    public function migrateAgentsReport()
    {

        $count = 0;
        $users = new User;

        $exisitingAgents = $users->select(array(
            'users.id', 'assigned_roles.role_id'
        ))
        ->leftJoin('assigned_roles', 'users.id', '=', 'assigned_roles.user_id')
        ->where('assigned_roles.role_id', '=', 2)
        ->orWhere('assigned_roles.role_id', '=', 3)
        ->orWhere('users.office_id', '<>', '')
        ->get();

        foreach ($exisitingAgents as $key => $agentValue) {

            $alreadyExists = $this->select('user_id')->where('user_id', '=' , $agentValue['id'])->first();

            $leadReport = new LeadReport;

            if (count($alreadyExists)) {

            } else {

                $leadReport->user_id = $agentValue['id'];
                $leadReport->gross_count = 0;
                $leadReport->active_count = 0;
                $leadReport->process_count = 0;
                $leadReport->process_rate = 0;
                $leadReport->funded_count = 0;
                $leadReport->funded_rate = 0;
                $leadReport->failure_count = 0;
                $leadReport->failure_rate = 0;
                $leadReport->ovr_quality = 0;

                $leadReport->save();

            }

            //Get clients current stage
            $results2 = DB::select(
                    'SELECT status_actions.stage_id
                    FROM clients
                    LEFT JOIN status_updates
                        ON (clients.id = status_updates.client_id)
                        AND status_updates.updated_at = (SELECT max(updated_at) FROM status_updates where client_id = clients.id )
                    LEFT JOIN status_actions
                        ON (status_updates.action_id = status_actions.id)
                    WHERE clients.user_id IN ('.$agentValue['id'].')
                    ORDER BY status_updates.updated_at
                    '
            );

            $resultFinal = json_decode(json_encode($results2), true);

            $processCount = 0;
            $fundedCount = 0;
            $failureCount = 0;
            $decrementTotal = 0;

            //Tally
            foreach ($resultFinal as $value) {

                $process_stages = array(0, 1, 2, 3 ,4);
                if( in_array($value['stage_id'], $process_stages, false)) {
                    $processCount += 1;
                }

                if($value['stage_id'] == 6) {
                    $fundedCount += 1;
                    $decrementTotal--;
                }

                if($value['stage_id'] == 5) {
                    $failureCount += 1;
                    $decrementTotal--;
                }

            }

            $activeCount = count($results2) + $decrementTotal;

            DB::statement('UPDATE lead_reports SET gross_count = '.count($results2).', active_count = '.$activeCount.', process_count = '.$processCount.', funded_count = '.$fundedCount.', failure_count = '.$failureCount.'  where user_id = '.$agentValue['id']);

            $leadReport->ratesMath($agentValue['id']);

        }

        return 'Success';
    }

    /*
    * Update Quality Counts - Decrease and Increase
    *   Refactor separate qualityCount methods
    *  process, funded, failure
    */
    public function qualityCount($agentID, $prevActionID, $newActionID)
    {

            $statusAction = new StatusAction;

            $previousStageID = $statusAction->select('stage_id')->where('id', '=', $prevActionID)->first();
            $currentStageID = $statusAction->select('stage_id')->where('id', '=', $newActionID)->first();

            $process_stages = array(0, 1, 2, 3 ,4);

            //process_count
            if ( in_array($previousStageID->stage_id, $process_stages, false) && in_array($currentStageID->stage_id, $process_stages, false)) {

                $results = 'Same Stage - No Update Required';

            } elseif (in_array($currentStageID->stage_id, $process_stages, false)) { 

                //process_count - switching from funded or failure back to process
                DB::table('lead_reports')->where('user_id', $agentID)->increment('process_count', 1);

                if($previousStageID->stage_id == 6) {

                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('funded_count', 1);
                    DB::table('lead_reports')->where('user_id', $agentID)->increment('active_count', 1);

                } elseif ($previousStageID->stage_id == 5) { 

                    //If previous stage = failure then add back to active, when it was marked as failure, active_count was decreased
                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('failure_count', 1);
                    DB::table('lead_reports')->where('user_id', $agentID)->increment('active_count', 1);

                }

                $results = "Record Updated";

            }

            //funded_count
            if($currentStageID->stage_id == 6) {

                DB::table('lead_reports')->where('user_id', $agentID)->increment('funded_count', 1);

                if(in_array($previousStageID->stage_id, $process_stages, false)) {

                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('process_count', 1);
                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('active_count', 1);

                } elseif($previousStageID->stage_id == 5) {

                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('failure_count', 1);

                } else {

                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('active_count', 1);

                }

                $results = 'Record Updated';

            }

            //failure_count
            if($currentStageID->stage_id == 5)
            {
                DB::table('lead_reports')->where('user_id', $agentID)->increment('failure_count', 1);

                if(in_array($previousStageID->stage_id, $process_stages, false)) {

                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('process_count', 1);
                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('active_count', 1);

                } elseif($previousStageID->stage_id == 6) {

                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('funded_count', 1);

                } else {

                    DB::table('lead_reports')->where('user_id', $agentID)->decrement('active_count', 1);

                }

                $results = 'Record Updated';

            }
            
            $this->ratesMath($agentID);

            return $results;

    }

    /*
    *   Calculate Quality Rates
    *   Rates are based off Lifetime stats (gross_count) not active_count.
    *   Refactor separate rates methods
    *   return success 
    */
    public function ratesMath($agentID)
    {

        $mathCounts = $this->select('gross_count', 'process_count', 'funded_count', 'failure_count')->where('user_id', '=', $agentID)->first();

        if($mathCounts->process_count == 0 && $mathCounts->gross_count == 0) {

            $processRate = 0;

        } else {

            $processRate = ($mathCounts->process_count / $mathCounts->gross_count) * 100;

        }
        
        if($mathCounts->funded_count == 0 && $mathCounts->gross_count == 0) {

            $fundedRate = 0;

        } else {

            $fundedRate = ($mathCounts->funded_count / $mathCounts->gross_count) * 100;

        }

        if($mathCounts->failure_count == 0 && $mathCounts->gross_count == 0) {

            $failureRate = 0;

        } else {

            $failureRate = ($mathCounts->failure_count / $mathCounts->gross_count) * 100;

        }

       DB::statement('UPDATE lead_reports SET process_rate = '.$processRate.', funded_rate = '.$fundedRate.', failure_rate = '.$failureRate.' where user_id = '.$agentID);

        return 'Success';

    }

    public function getLeadReport($userID)
    {

        return $this->select()->where('user_id', '=', $userID)->first();

    }

    /* 
    * Relationships 
    */
    public function Users()
    {
        return $this->belongsTo('User', 'user_id');
    }

}