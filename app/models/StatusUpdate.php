<?php

class StatusUpdate extends Eloquent{

	/**
     * Get current Status Action of the client
     * args: $id (CLIENT ID)
     * @return Response
     */
	public function getCurrentActionID($id){

		$statusActionResult = $this->select('action_id')->where('client_id', '=', $id)->orderBy('created_at', 'DESC')->first();

            if( ! $statusActionResult){
                $previousStatusID = 21;
            }
            else{
                $previousStatusID = $statusActionResult->action_id;
            }

		return $previousStatusID;

	}

    /**
     * Get all status updates of client
     * args: $clientID
     * @return Response
     */

    public function getStatusUpdates($clientID){

    	$allStatusUpdates = 
    		DB::table('status_updates')
              ->select('status_updates.user_id', 'status_updates.status_content', 'status_actions.name', 'status_updates.created_at', 'users.first_name', 'users.last_name')
              ->join('users', function($join)
                  {
                      $join->on('status_updates.user_id', '=', 'users.id');
                  })
              ->join('status_actions', function($join)
              {
                $join->on('status_updates.action_id', '=', 'status_actions.id');
              })
              ->where('client_id', '=', $clientID)
              ->orderBy('status_updates.created_at', 'DESC')
              ->paginate(4);

              return $allStatusUpdates;
    }

  /**
     * Set the Quick Status update if user doesn't input a custom/manual comment
     * Checks if a comment is left if not then set a default quick status update
     * args: $statusContent (check for comment), statusAction (action_id)
     * @return Response
     */
    public function setStatusContent($statusContent, $statusAction){

	//If status change then set default comment based on option selected (REFACTOR THIS)
      if( ! $statusContent ){
      		
		if( $statusAction == '6' ) 
		{ 
			$statusContent = 'Status Changed: Contacted'; 
		}
		elseif( $statusAction == '7' )
		{ 
			$statusContent = 'Status Changed: Contact Attempt 2';
		}
		elseif( $statusAction == '8' )
		{ 
			$statusContent = 'Status Changed: Contact Attempt 3';
		}
		elseif( $statusAction == '9' )
		{ 
			$statusContent = 'Status Changed: Contact Attempt 4';
		}
		elseif( $statusAction == '21' )
		{ 
			$statusContent = 'Status Changed: Queued In System';
		}
		elseif( $statusAction == '16' )
		{ 
			$statusContent = 'Status Changed: Gathering Documents'; 
		}
		elseif( $statusAction == '12' )
		{ 
			$statusContent = 'Status Changed: Documents Received'; 
		}
		elseif( $statusAction == '25' )
		{ 
			$statusContent = 'Status Changed: Unqualified'; 
		}
		elseif( $statusAction == '4' )
		{ 
			$statusContent = 'Status Changed: Callback'; 
		}
		elseif( $statusAction == '14' )
		{ 
			$statusContent = 'Status Changed: Follow Up'; 
		}
		elseif( $statusAction == '24' )
		{ 
			$statusContent = 'Status Changed: Submitted for Approval'; 
		}
		elseif( $statusAction == '23' )
		{ 
			$statusContent = 'Status Changed: Stipulations Needed'; 
		}
		elseif( $statusAction == '2' )
		{ 
			$statusContent = 'Status Changed: Approved/Qualified'; 
		}
		elseif( $statusAction == '20' )
		{ 
			$statusContent = 'Status Changed: Declined (Nurture)'; 
		}
		elseif( $statusAction == '11' )
		{ 
			$statusContent = 'Status Changed: Declined'; 
		}
		elseif( $statusAction == '15' )
		{ 
			$statusContent = 'Status Changed: Funded'; 
		}
		elseif( $statusAction == '17' )
		{ 
			$statusContent = 'Status Changed: Nurture';
		}
		elseif( $statusAction == '19' )
		{ 
			$statusContent = 'Status Changed: Nurture - Approved but not interested';
		}
		elseif( $statusAction == '18' )
		{ 
			$statusContent = 'Status Changed: Nurture - Better Statements';
		}
		elseif( $statusAction == '13' )
		{ 
			$statusContent = 'Status Changed: Do Not Call'; 
		}
		elseif( $statusAction == '1' )
		{ 
			$statusContent = 'Status Changed: Appointment Set';
		}
		elseif( $statusAction == '3' )
		{ 
			$statusContent = 'Status Changed: Bad Email/Phone';
		}
		elseif( $statusAction == '10' )
		{ 
			$statusContent = 'Status Changed: Deal Closed';
		}
            elseif( $statusAction == '26' )
            { 
              $statusContent = 'Status Changed: Waiting on Documents';
            }
            elseif( $statusAction == '22' )
            { 
              $statusContent = 'Status Changed: Sending Contracts';
            }
      }
      else{

		$statusContent = $statusContent;
		
	}

	return $statusContent;

  }

  /* 
  * Relationships
  *
   */
	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function client()
	{
		return $this->belongsTo('Client', 'client_id');
	}

	public function statusAction()
	{
		return $this->belongsTo('StatusAction', 'action_id');
	}

}