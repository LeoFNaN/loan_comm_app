<?php

class StatusStage extends \Eloquent {
	protected $fillable = [];

        public function statusUpdate()
        {
            $this->belongsTo('StatusUpdate');
        }

        public function statusAction()
        {
            $this->hasMany('StatusAction');
        }
}