<?php

use Zizaco\Confide\UserValidator as ConfideUserValidator;
use Zizaco\Confide\UserValidatorInterface;
use Zizaco\Confide\ConfideUserInterface;

class UserValidator extends ConfideUserValidator implements UserValidatorInterface
{
    /**
     * Confide repository instance.
     *
     * @var \Zizaco\Confide\RepositoryInterface
     */
    public $repo;

    /**
    * Validation rules for this Validator.
    *
    * @var array
    */
    public $rules = [

        'create' => 
        [
            'username' => 'alpha_dash',
            'first_name' => 'required|min:1|alpha',
            'last_name' => 'required|min:1',
            'phone_number' => 'required|phone',
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:6',
        ],

        'update' => 
        [
            'username' => 'alpha_dash',
            'first_name' => 'required|min:1|alpha',
            'last_name' => 'required|min:1',
            'phone_number' => 'required|phone',
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]

    ];

    /**
     * Validates the given user. Should check if all the fields are correctly.
     *
     * @param ConfideUserInterface $user Instance to be tested.
     *
     * @return boolean True if the $user is valid.
     */
    public function validate(ConfideUserInterface $user, $ruleset = 'create')
    {
        // Set the $repo as a ConfideRepository object
        $this->repo = App::make('confide.repository');

        // Validate object
        $result = $this->validateAttributes($user, $ruleset) ? true : false;
        $result = ($this->validatePassword($user) && $result) ? true : false;
        $result = ($this->validateIsUnique($user) && $result) ? true : false;

        return $result;
    }

}