<?php

class AssignedAgent extends Eloquent {

	/**
	*  Return Agent Assigned Inhouse Information
	*  Gets first match.
	*/
	public function grabInhouse($data)
	{

		$user = new User();

		$assignedID = $this->where('user_id', '=', $data->id)->first(); //grab Inhouse agent_id

		if (count($assignedID)) {

			$inhouseData = $user->where('id', '=', $assignedID->agent_id)->first(); //grab Inhouse Agent Data
			return $inhouseData;

		} else {

			$inhouseData = null;
			return $inhouseData;

		}
		
	}

	/**
	* Return all Agents underneath particular Inhouse
	*/
	public function getAgentsAssignedToInhouse($inhouseID)
	{

		$assignedAgents = $this->where('agent_id', '=', $inhouseID)->get(); //grab Inhouse agent_id

		if (count($assignedAgents)) {
		
			return $assignedAgents;
		
		} else {

			$assignedAgents = null;
			return $assignedAgents;
		
		}
		
	}

   /**
   * Boolean True/False
   *  Return true if the Clients Agent is assigned to the Inhouse Agent
   */
     public function agentInhouseMatch($clientAgentID, $inhouseID)
     {

		$assignedID = $this->where('user_id', '=', $clientAgentID)->first();

		//Either the Agent doesn't exist or they don't have an Inhouse assigned to them.
		If (is_null($assignedID)) {

		     return false;

		} elseif ($assignedID->agent_id == $inhouseID) 	{
		     
		     return true;

		} else {

		     return false;
		
		}

      }

	public function user()
	{
		return $this->hasMany('User');
	}

}