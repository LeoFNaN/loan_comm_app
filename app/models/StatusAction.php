<?php

class StatusAction extends \Eloquent {

	protected $fillable = [];

	/**
     * Create MulitArray for Select Dropdown
     *
     * @return Response
     */
	public function getAllStatusActions(){

		$stages = new StatusStage();

		$allStatusesRaw = $this->select('id', 'name', 'stage_id')->get();
		$allStatuses = $allStatusesRaw->toArray();
		$allStages = $stages->orderBy('id')->lists('name', 'id');

		$options = array();

		foreach ($allStages as $stagesKey => $stagesValue) {

			foreach($allStatuses as $key => $value)
			{

				if($stagesKey == $value['stage_id'])
				{
					$options[' - '.strtoupper($stagesValue)][$value['id']] = $value['name'];
				}
			}

		}

		return $options;

	}


	/**
     * Get Set of Actions by Stage Value 
     * args: $stageID
     * @return Response
     */
	public function getActionsByStage($stageID){

		if($stageID == 1){

			$array = array('0' => 'Select One', '21' => 'Queued In System', '5' => 'Comment Only');

		}
		else{

			$array = array('0' => 'Select One');
		}

			$stageStatus = $array + $this->orderBy('stage_id', 'Desc')->where('stage_id', '=', $stageID)->lists('name','id');

			return $stageStatus;
	}

	/**
     * Get current status of the client
     * args: $id (CLIENT ID)
     * @return Response
     */
	public function getCurrentStatus($id){

		$statusUpdate = new StatusUpdate();

		$statusActionResult = $statusUpdate->with('StatusAction')->where('client_id', '=', $id)->orderBy('created_at', 'DESC')->first();
                    
             return $statusActionResult;

	}

	/**
	* Get Status Action by Name
	* args: $actionID
	* @return Response
	*/
	public function getActionByName($actionID)
	{
		$actionName = $this->select('name')->where('id', '=', $actionID)->first();

		return $actionName->name;
	}

	/**
     * RELATIONSHIPS
     */
	public function statusUpdate()
	{
		$this->hasMany('StatusUpdate');
	}

	public function statusStage()
	{
		$this->belongsTo('StatusStage', 'stage_id');
	}
}