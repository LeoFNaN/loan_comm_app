<?php namespace App\Models;

use LaravelBook\Ardent\Ardent;
use Carbon\Carbon;

class Activity extends Ardent{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'activity_log';

	/**
	* Delete all activity that are at least 3 months old
	*/
	public function cleanActivity(){
		$timeFrame = Carbon::now()->subMonths(3);
		Activity::where('created_at', '<=', $timeFrame)->delete();
	}

	public function user()
	{
		return $this->belongsTo('User');
	}
	
	public function client()
	{
		return $this->belongsTo('Client');
	}

}