<?php

use LaravelBook\Ardent\Ardent;

class Client extends Ardent{

    /**
     * Get list of Clients IDs by All Agents that belong to the Inhouse
     * Inhouse Dashboard
     * @return INT
     */
      public function getClientsIDByAllAgents($authorID)
      {
                
                $clientIDs =$this->select('clients.id')
                ->join('assigned_agents', function($join) use ($authorID) {

                    $join->on('clients.user_id', '=', 'assigned_agents.user_id');

                })
                ->where('assigned_agents.agent_id', '=', $authorID)
                ->distinct()
                ->lists('clients.id');

                if($clientIDs != null ){


                }else{

                    $clientIDs = array(0);

                }

                return $clientIDs;

      }

    /**
     * Get list of Clients IDs by Specific Agent
     * Inhouse List of Clients View
     * @return INT
     */
    public function getClientsIDByAgent($agentID){

      $listofClientsIds = DB::table('clients')->select('id')->where('clients.user_id', '=', $agentID)->lists('id');

            //Check if listofclients is null, can't pass a null array to whereIn
            if($listofClientsIds != null ){
                
            }else{
                $listofClientsIds = array(0);
            }

            return $listofClientsIds;

    }


    /**
     * Relationships
     */
        public function user()
        {
            return $this->belongsTo('User', 'user_id');
        }

        public function statuses()
        {
            return $this->hasMany('StatusUpdate');
        }

        public function activityLog()
        {
        	return $this->hasMany('ActivityLog');
        }


}