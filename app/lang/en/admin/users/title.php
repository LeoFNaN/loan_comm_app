<?php

return array(

	'user_management'    => 'Agent Management',
	'user_update'        => 'Update a Agent',
	'user_delete'        => 'Delete a Agent',
	'create_a_new_user'  => 'Create a New Agent',

);
