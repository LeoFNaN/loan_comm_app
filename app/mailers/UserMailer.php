<?php namespace MVF\Mailers;

use User;

class UserMailer extends Mailer
{

    public function newClientToInhouse(User $user, $data)
    {

        $subject = 'MVF: New Client Added';
        $view = 'emails.newclient';
        $title = 'MVF Inhouse Agent';

        return $this->sendTo($user, $subject, $view, $data, $title);

    }

    public function newClientToAdmin(User $user, $data)
    {

        $subject = 'MVF: New Client Added';
        $view = 'emails.newclient';
        $adminInfo = array('email' => 'admin@multivisionfinancial.com', 'title' => 'MVF Admin', 'ccEmail' => 'jim@multivisionfinancial.com', 'ccTitle' => 'MVF Referral Director'); 

        return $this->sendToCC($adminInfo, $subject, $view, $data);

    }

    /*
    *  New Referral Agent Registration/Sign Up to Agent
    */
    public function newSignUpToAgent(User $user, $data)
    {

        $subject = 'Thank you for signing up to MVFLoans, please read for your next steps';
        
        $title = $user->first_name.' '.$user->last_name;

        $view = 'emails.confirmation';

        return $this->sendTo($user, $subject, $view, $data, $title);

    }

    /*
    *  New Referral Agent Registration/Sign Up
    */
    public function newSignUpToAdmin(User $user, $data)
    {


        $subject = 'MVF: New Agent Sign Up';
        $view = 'emails.registration';
        $adminInfo = array('email' => 'admin@multivisionfinancial.com', 'title' => 'MVF Admin', 'ccEmail' => 'jim@multivisionfinancial.com', 'ccTitle' => 'MVF Referral Director'); 

        return $this->sendToCC($adminInfo, $subject, $view, $data);

    }

}
