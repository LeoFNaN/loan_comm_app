<?php namespace MVF\Mailers;

use User;

class InhouseStatusUpdatesMailer extends Mailer 
{

    public function newStatusUpdateToAgent(User $user, $data)
    {

        $subject = 'MVF: New Status Update ('.$data['client']['business_name'].')';
        $view = 'emails.statusupdate';
        $title = 'MVF Referral Agent';
        $replyTo = $data['agent']['email'];

        return $this->sendToReply($user, $subject, $view, $data, $title, $replyTo);

    }

    public function newStatusUpdateToAdmin(User $user, $data)
    {

        $subject = 'MVF: New Status Update ('.$data['client']['business_name'].')';
        $view = 'emails.admin.statusupdate';
        $adminInfo = array('email' => 'admin@multivisionfinancial.com', 'title' => 'MVF Admin', 'ccEmail' => 'jim@multivisionfinancial.com', 'ccTitle' => 'MVF Referral Director'); 

        return $this->sendToCC($adminInfo, $subject, $view, $data);

    }

}
