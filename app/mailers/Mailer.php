<?php namespace MVF\Mailers;

use Mail;

abstract class Mailer
{

    public function sendTo($user, $subject, $view, $data = [], $title)
    {

        Mail::send ($view, $data, function($message) use ($user, $subject, $title)
        {
            $message->to($user->email, $title)
            ->subject($subject);
        });

    }

    public function sendToReply($user, $subject, $view, $data = [], $title, $replyTo)
    {

        Mail::send ($view, $data, function($message) use ($user, $subject, $title, $replyTo)
        {
            $message->to($user->email, $title)
            ->replyTo($replyTo)
            ->subject($subject);
        });

    }

    /*
    * admin email Hard coded from AdminMailer Class
    */
    public function sendToCC2($user, $subject, $view, $data = [], $title, $title2, $cc)
    {

        Mail::send ($view, $data, function($message) use ($user, $subject, $title, $title2, $cc)
        {
            $message->to($user->email, $title)
            ->cc($cc, $title2)
            ->subject($subject);
        });

    }

    /*
    * admin email Hard coded from UserMailer Class
    */
    public function sendToCC($user, $subject, $view, $data = [])
    {

        Mail::send ($view, $data, function($message) use ($user, $subject)
        {
            $message->to($user['email'], $user['title'])
            ->cc($user['ccEmail'], $user['ccTitle'])
            ->subject($subject);
        });

    }

}
