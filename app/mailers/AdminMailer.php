<?php namespace MVF\Mailers;

use User;

class AdminMailer extends Mailer
{

    public function agentActivation(User $user, $data)
    {

        $subject = 'MVF: Account Activated';
        $view = 'emails.activation';
        $title = $user->email;

        return $this->sendTo($user, $subject, $view, $data, $title);

    }

    public function agentAssignmentTo(User $user, $data)
    {

        $subject = 'MVF: Your Inhouse Agent';
        $view = 'emails.assignmentAgent';
        $title = 'MVF Referral Agent';

        return $this->sendTo($user, $subject, $view, $data, $title);

    }

    public function inhouseAssignmentTo(User $user, $data)
    {

        $subject = 'MVF: New Agent Assignment';
        $view = 'emails.assignmentInhouse';

        $title = 'MVF Inhouse Agent';

        $cc = 'admin@multivisionfinancial.com';
        $title2 = 'MVF Agent Center';

        return $this->sendToCC2($user, $subject, $view, $data, $title, $title2, $cc);

    }

    public function newStatusUpdateToAgent(User $user, $data)
    {

        $subject = 'MVF: New Status Update ('.$data['client']['business_name'].')';
        $view = 'emails.statusupdate';
        $title = '';

        return $this->sendTo($user, $subject, $view, $data, $title);

    }

    public function newStatusUpdateToInhouse(User $user, $data)
    {

        $subject = 'MVF: New Status Update ('.$data['client']['business_name'].')';
        $view = 'emails.inhouse.statusupdate';
        $title = $user->first_name;

        return $this->sendTo($user, $subject, $view, $data, $title);

    }

    /*
    * They want it for record keeping
    */
    public function newStatusUpdateToAdmin(User $user, $data)
    {

        $subject = 'MVF: New Status Update ('.$data['client']['business_name'].')';
        $view = 'emails.inhouse.statusupdate';
        $adminInfo = array('email' => 'admin@multivisionfinancial.com', 'title' => 'MVF Admin', 'ccEmail' => 'jim@multivisionfinancial.com', 'ccTitle' => 'MVF Referral Director'); 

        return $this->sendToCC($adminInfo, $subject, $view, $data);

    }

}
