<?php namespace MVF\Mailers;

use Client;

class ClientMailer extends Mailer
{

    public function welcomeEmailToClient(Client $client, $data)
    {

        $emailTitle = $data['client']['first_name'];

        //Refactor this...bleh
        if ($data['agent']['office_id'] == '7E4BDF2') {

          $emailSubject = 'Thank you for your Business Loan Now Inquiry.';
          $view = 'emails.client.custom_welcome';

        } else {

          $emailSubject = 'Information regarding your business loan request';
          $view = 'emails.client.welcome';

        }

        $replyTo = $data['inhouse']['email'];

        return $this->sendToReply($client, $emailSubject, $view, $data, $emailTitle, $replyTo);

    }

}
