<?php namespace MVF\Mailers;

use User;

class StatusUpdatesMailer extends Mailer
{

    public function newStatusUpdateToInhouse(User $user, $data)
    {

        $subject = 'MVF: New Status Update ('.$data['client']['business_name'].')';
        $view = 'emails.statusupdate';
        $title = 'MVF Inhouse Agent';

        return $this->sendTo($user, $subject, $view, $data, $title);

    }

    public function newStatusUpdateToAdmin(User $user, $data)
    {

        $subject = 'MVF: New Status Update ('.$data['client']['business_name'].')';
        $view = 'emails.statusupdate';
        $adminInfo = array('email' => 'admin@multivisionfinancial.com', 'title' => 'MVF Admin', 'ccEmail' => 'jim@multivisionfinancial.com', 'ccTitle' => 'MVF Referral Director'); 

        return $this->sendToCC($adminInfo, $subject, $view, $data);

    }

}
