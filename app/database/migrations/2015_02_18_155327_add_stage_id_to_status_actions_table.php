<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddStageIdToStatusActionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('status_actions', function(Blueprint $table)
		{
			$table->integer('stage_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('status_actions', function(Blueprint $table)
		{
			$table->dropColumn('stage_id');
		});
	}

}
