<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveCurrentStatusFromStatusUpdatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('status_updates', function(Blueprint $table)
		{
			$table->dropColumn('current_status');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('status_updates', function(Blueprint $table)
		{
			$table->string('current_status');
		});
	}

}
