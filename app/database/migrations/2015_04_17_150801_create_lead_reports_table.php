<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLeadReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lead_reports', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('gross_count');
			$table->integer('active_count');
			$table->integer('process_count');
			$table->decimal('process_rate', 6,2);
			$table->integer('funded_count');
			$table->decimal('funded_rate', 6,2);
			$table->integer('failure_count');
			$table->decimal('failure_rate', 6,2);
			$table->decimal('ovr_quality', 6,2);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lead_reports');
	}

}
