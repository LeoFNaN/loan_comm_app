<?php

//use Faker\Factory as Faker;

class StatusStagesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('status_stages')->delete();

        /* See Notes */
        //$faker = Faker::create();
        //$faker->addProvider(new Faker\Provider\Internet($faker));

        DB::table('status_stages')->insert(array(

            /* ***************************************
            *  Status Actions
            *************************************** */
             // 1
             array(
                'name'=> 'Initial Contact', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ),

            // 2
             array(
                'name'=> 'Documents/Underwriting', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ),

             // 3
             array(
                'name'=> 'Approved/Negotiation', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ),

             // 4
             array(
                'name'=> 'Nurture', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ),

            // 5
             array(
                'name'=> 'Declined/Unqualified', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ),

            // 6
             array(
                'name'=> 'Funded', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
            ),

        ));
    }
}