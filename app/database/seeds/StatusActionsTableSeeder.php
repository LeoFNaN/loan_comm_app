<?php

//use Faker\Factory as Faker;

class StatusActionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('status_actions')->delete();

        /* See Notes */
        //$faker = Faker::create();
        //$faker->addProvider(new Faker\Provider\Internet($faker));

        DB::table('status_actions')->insert(array(

            /* ***************************************
            *  Status Actions
            *************************************** */
             // 1
             array(
                'name'=> 'Appointment', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '1',
            ),

            // 2
             array(
                'name'=> 'Approved/Qualified', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '3',
            ),

             // 3
             array(
                'name'=> 'Bad Email/Phone', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '1',
            ),

             // 4
             array(
                'name'=> 'Callback', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '1',
            ),

            // 5
             array(
                'name'=> 'Comment Only', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '0',
            ),

            // 6
             array(
                'name'=> 'Contact', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '1',
            ),

            // 7
             array(
                'name'=> 'Contact Attempt 2', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '1',
            ),

            // 8
             array(
                'name'=> 'Contact Attempt 3', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '1',
            ),

            // 9
             array(
                'name'=> 'Contact Attempt 4', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '1',
            ),

            // 10
             array(
                'name'=> 'Deal Closed', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '6',
            ),

            // 11
             array(
                'name'=> 'Declined', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '5',
            ),

            // 12
             array(
                'name'=> 'Documents Recevied', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '2',
            ),

            // 13
             array(
                'name'=> 'Do Not Contact', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '5',
            ),

            // 14
             array(
                'name'=> 'Follow Up', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '1',
            ),

            // 15
             array(
                'name'=> 'Funded', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '6',
            ),

            // 16
             array(
                'name'=> 'Gathering Documents', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '2',
            ),

            // 17
             array(
                'name'=> 'Nurture', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '4',
            ),

            // 18
             array(
                'name'=> 'Nurture - Better Statements', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '4',
            ),

            // 19
             array(
                'name'=> 'Nurture - Approved Not Interested', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '4',
            ),

            // 20
             array(
                'name'=> 'Nurture - Declined', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '4',
            ),

            // 21
             array(
                'name'=> 'Queued In System', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '0',
            ),

            // 22
             array(
                'name'=> 'Sending Contracts', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '3',
            ),

            // 23
             array(
                'name'=> 'Stipulations Needed', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '2',
            ),

            // 24
             array(
                'name'=> 'Submitted for Approval', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '2',
            ),

            // 25
             array(
                'name'=> 'Unqualified', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '5',
            ),

            // 26
             array(
                'name'=> 'Waiting on Documents', 
                'created_at'=> Carbon::now(),
                'updated_at'=> Carbon::now(),
                'stage_id' => '2',
            ),

        ));
    }
}