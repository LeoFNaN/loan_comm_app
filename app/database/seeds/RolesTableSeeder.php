<?php

class RolesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();

        $adminRole = new Role;
        $adminRole->name = 'admin';
        $adminRole->save();

        $agentRole = new Role;
        $agentRole->name = 'Agent';
        $agentRole->save();

        $inhouseRole = new Role;
        $inhouseRole->name = 'Inhouse';
        $inhouseRole->save();

       /* $commentRole = new Role;
        $commentRole->name = 'comment';
        $commentRole->save();*/

        $user = User::where('email','=','admin@mvfloans.dev')->first();
        $user->attachRole( $adminRole );

        $user = User::where('email','=','inhouse@mvfloans.dev')->first();
        $user->attachRole( $inhouseRole );

        $user = User::where('email','=','inhouse2@mvfloans.dev')->first();
        $user->attachRole( $inhouseRole );

        $user = User::where('email','=','agent1@example.org')->first();
        $user->attachRole( $agentRole );

        $user = User::where('email','=','agent2@example.org')->first();
        $user->attachRole( $agentRole );

        $user = User::where('email','=','agent3@example.org')->first();
        $user->attachRole( $agentRole );

        $user = User::where('email','=','agent4@example.org')->first();
        $user->attachRole( $agentRole );

/*        $user = User::where('username','=','user')->first();
        $user->attachRole( $commentRole );*/
    }

}
