<?php

class DatabaseSeeder extends Seeder {

    public function run()
    {

    /*  DB::table('Users')->truncate();
        DB::table('PostsTableSeeder')->truncate();
        DB::table('CommentsTableSeeder')->truncate();
        DB::table('RolesTableSeeder')->truncate();
        DB::table('PermissionsTableSeeder')->truncate();
        DB::table('StatusUpdatesTableSeeder')->truncate();*/

        Eloquent::unguard();

        // Add calls to Seeders here
        $this->call('UsersTableSeeder');
        $this->call('PostsTableSeeder');
        $this->call('CommentsTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('PermissionsTableSeeder');
        $this->call('AssignedAgentsTableSeeder');
        $this->call('ClientsTableSeeder');
        $this->call('StatusUpdatesTableSeeder');
        $this->call('ActivityLogSeeder');
        $this->call('StatusActionsTableSeeder');
        $this->call('StatusStagesTableSeeder');

        $this->command->info("Tables seeded :)");
    }

}