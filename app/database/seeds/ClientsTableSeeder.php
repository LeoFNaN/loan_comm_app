<?php

//use Faker\Factory as Faker;

class ClientsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('clients')->delete();

        /* See Notes */
        //$faker = Faker::create(); //$faker->addProvider(new Faker\Provider\Internet($faker));

        DB::table('clients')->insert(array(

             array( //Client 1
                'user_id'=> '4', //agent 1
                'first_name' => 'ClientOne',
                'last_name' => 'NumberOne',
                'phone' => '555-555-5555',
                'business_name' => 'YourBusiness123',
                'loan_amount' => '$20,000',
                'state' => 'CA',
                'business_years' => '5',
                'monthly_revenue' => '$40,000',
                'callback_time' => 'Immediately',
                'created_at' => Carbon::now()->subDay(),
                'updated_at' => Carbon::now()->subDay(),
                'email'=> 'client@example.org',
                'client_message' => 'This client was added by Agent 1'
            ),

             array( //Client 2
                'user_id'=> '4', //agent 1
                'first_name' => 'ClientTwo',
                'last_name' => 'NumberTwo',
                'phone' => '777-777-7777',
                'business_name' => 'ThisBusiness123',
                'loan_amount' => '$25,000',
                'state' => 'CA',
                'business_years' => '11',
                'monthly_revenue' => '$50,000',
                'callback_time' => 'Tomorrow',
                'created_at' => Carbon::now()->subDays(1),
                'updated_at' => Carbon::now()->subDays(1),
                'email'=> 'client2@example.org',
                'client_message' => 'This client was added by Agent 1'
            ),

            array( //Client 3
                'user_id'=> '4', //agent 1
                'first_name' => 'ClientThree',
                'last_name' => 'NumberThree',
                'phone' => '888-888-8888',
                'business_name' => 'ThatBusiness123',
                'loan_amount' => '$40,000',
                'state' => 'WY',
                'business_years' => '9',
                'monthly_revenue' => '$80,000',
                'callback_time' => 'Immediately',
                'created_at' => Carbon::now()->subDays(2),
                'updated_at' => Carbon::now()->subDays(2),
                'email'=> 'client3@example.org',
                'client_message' => 'This client was added by Agent 1'
            ),

            array( //Client 4
                'user_id'=> '5', //agent 2
                'first_name' => 'ClientFour',
                'last_name' => 'NumberFour',
                'phone' => '999-999-9999',
                'business_name' => 'OrBusiness123',
                'loan_amount' => '$30,000',
                'state' => 'CA',
                'business_years' => '2',
                'monthly_revenue' => '$60,000',
                'callback_time' => 'Morning',
                'created_at' => Carbon::now()->subDays(3),
                'updated_at' => Carbon::now()->subDays(3),
                'email'=> 'client4@example.org',
                'client_message' => 'This client was added by Agent 2'
            ),

            array( //Client 5
                'user_id'=> '5', //Agent 2
                'first_name' => 'ClientFive',
                'last_name' => 'NumberFive',
                'phone' => '141-999-9999',
                'business_name' => 'OrBusiness65',
                'loan_amount' => '$10,000',
                'state' => 'CA',
                'business_years' => '4',
                'monthly_revenue' => '$10,000',
                'callback_time' => 'Morning',
                'created_at' => Carbon::now()->subDays(4),
                'updated_at' => Carbon::now()->subDays(4),
                'email'=> 'client5@example.org',
                'client_message' => 'This client was added by Agent 2'
            ),

            array( //Client 5
                'user_id'=> '5', //Agent 2
                'first_name' => 'ClientSix',
                'last_name' => 'NumberSix',
                'phone' => '222-999-9999',
                'business_name' => 'Business13',
                'loan_amount' => '$100,000',
                'state' => 'CA',
                'business_years' => '9',
                'monthly_revenue' => '$242,000',
                'callback_time' => 'Morning',
                'created_at' => Carbon::now()->subDays(5),
                'updated_at' => Carbon::now()->subDays(5),
                'email'=> 'client6@example.org',
                'client_message' => 'This client was added by Agent 2'
            ),

            array( //Client 7
                'user_id'=> '6', //Agent 3
                'first_name' => 'ClientSeven',
                'last_name' => 'NumberSeven',
                'phone' => '333-399-9999',
                'business_name' => 'ACME8',
                'loan_amount' => '$35,000,000',
                'state' => 'CA',
                'business_years' => '4',
                'monthly_revenue' => '$100,246,004',
                'callback_time' => 'Anytime',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'email'=> 'client7@example.org',
                'client_message' => 'This client was added by Agent 3'
            ),

            array( //Client 8
                'user_id'=> '6', //Agent 3
                'first_name' => 'ClientEight',
                'last_name' => 'NumberEight',
                'phone' => '268-399-9999',
                'business_name' => 'ACM98',
                'loan_amount' => '$300,000',
                'state' => 'WY',
                'business_years' => '1',
                'monthly_revenue' => '$100,205',
                'callback_time' => 'Anytime',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'email'=> 'client8@example.org',
                'client_message' => 'This client was added by Agent 3'
            ),

          ));

    }

}
