<?php

//use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        /* See Notes */
        //$faker = Faker::create(); //$faker->addProvider(new Faker\Provider\Internet($faker));

        DB::table('users')->insert(array(
             array( //ADMIN USER
                'username'=>'',
                'email'=>'admin@mvfloans.dev',
                'password'=>Hash::make('admin'),
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '1', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'Patent',
                'last_name' => 'Admin',
                'phone_number' => '888-598-9951',
                'agent_reason' => 'Im admin 1',
                'referral_source' => 'Other',
                'office_id' => ''
            ),

            array( //INHOUSE USER
                'username'=>'',
                'email'=>'inhouse@mvfloans.dev',
                'password'=> Hash::make('inhouse'), 
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '1', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'Rinse',
                'last_name' => 'Inhouse',
                'phone_number' => '222-222-2222',
                'agent_reason' => 'Im inhouse',
                'referral_source' => 'Other',
                'office_id' => ''
            ),

            array( //INHOUSE USER 2
                'username'=>'',
                'email'=>'inhouse2@mvfloans.dev',
                'password'=> Hash::make('inhouse2'), 
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '1', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'Chase',
                'last_name' => 'Inhouse2',
                'phone_number' => '145-985-3333',
                'agent_reason' => 'Im inhouse 2',
                'referral_source' => '',
                'office_id' => ''
            ),

            array( //AGENT 1 USER
                'username'=>'',
                'email'=>'agent1@example.org',
                'password'=> Hash::make('agent1'), 
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '1', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'Urchent',
                'last_name' => 'AgentOne',
                'phone_number' => '333-333-3333',
                'agent_reason' => 'Im agent 1',
                'referral_source' => 'Inhouse 1',
                'office_id' => uniqid('mvf_')
            ),

            array( //AGENT 2 USER
                'username'=>'',
                'email'=>'agent2@example.org',
                'password'=> Hash::make('agent2'), 
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '1', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'Asmugah',
                'last_name' => 'AgentTwo',
                'phone_number' => '444-444-4444',
                'agent_reason' => 'Im agent 2',
                'referral_source' => 'Other',
                'office_id' => uniqid('mvf_')
            ),

            array( //AGENT 3 USER
                'username'=>'',
                'email'=>'agent3@example.org',
                'password'=> Hash::make('agent3'), 
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '1', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'Garner',
                'last_name' => 'AgentThree',
                'phone_number' => '555-444-4444',
                'agent_reason' => 'Im agent 3',
                'referral_source' => 'Google',
                'office_id' => uniqid('mvf_')
            ),

            array( //Agent 4
                'username'=>'',
                'email'=>'agent4@example.org',
                'password'=> Hash::make('agent4'), 
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '1', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'Ashlee',
                'last_name' => 'AgentFour',
                'phone_number' => '555-456-4854',
                'agent_reason' => 'Im a great agent and know a lot of business owners',
                'referral_source' => 'Bing',
                'office_id' => uniqid('mvf_')
            ),

            array( //Agent 5 >> Waiting for papers
                'username'=>'',
                'email'=>'agent5@example.org',
                'password'=> Hash::make('agent5'), 
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '0', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'Patrick',
                'last_name' => 'AgentFive',
                'phone_number' => '521-456-4854',
                'agent_reason' => 'Lets make money!',
                'referral_source' => 'Inhouse 2',
                'office_id' => uniqid('mvf_')
            ),

/*         array( //Team 1 Leader - Group Feature 3
                'username'=>'',
                'email'=>'agent3@example.org',
                'password'=> Hash::make('agent3'), 
                'confirmation_code' => md5(microtime().Config::get('app.key')), 
                'remember_token' => '', 
                'confirmed' => '1', 
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => 'AgentThree',
                'last_name' => 'Asmugah',
                'phone_number' => '555-444-4444',
                'agent_reason' => 'Im agent 3',
                'referral_source' => 'Google',
                'office_id' => uniqid('mvf_')
            ),*/

/*             array('username'=>'','email'=>'mark@example.dev','password'=>''),
             array('username'=>'','email'=>'karl@example.dev','password'=>''),
             array('username'=>'','email'=>'marl@example.dev','password'=>''),
             array('username'=>'','email'=>'mary@example.dev','password'=>''),
             array('username'=>'','email'=>'sels@example.dev','password'=>''),
             array('username'=>'','email'=>'taylor@example.dev','password'=>''),*/

          ));

/*        foreach(range(1, 15) as $index)
        {

            User::create([

                'username' => '',
                'email' => $faker->email(),
                'password' => $faker->password(),
                'confirmation_code' => $faker->md5(),
                'remember_token' => $faker->md5(),
                'confirmed' => '1',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'first_name' => $faker->firstName(),
                'last_name' => $faker->lastName(),
                'phone_number' => $faker->phoneNumber(),
                'agent_reason' => $faker->sentence(),
                'referral_source' => $faker->$faker->name(),
                'office_id' => $faker->unique()->randomDigit()


            ]);
        }*/

/*        

$users = array(
            array(
                'username'      => 'admin',
                'email'      => 'admin@example.org',
                'password'   => Hash::make('admin'),
                'confirmed'   => 1,
                'confirmation_code' => md5(microtime().Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'username'      => 'user',
                'email'      => 'user@example.org',
                'password'   => Hash::make('user'),
                'confirmed'   => 1,
                'confirmation_code' => md5(microtime().Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            )
        );

        DB::table('users')->insert( $users );*/
    }

}
