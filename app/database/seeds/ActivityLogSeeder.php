<?php

//use Faker\Factory as Faker;

class ActivityLogSeeder extends Seeder {

    public function run()
    {
        DB::table('activity_log')->delete();

        /* See Notes */
        //$faker = Faker::create(); //$faker->addProvider(new Faker\Provider\Internet($faker));

        DB::table('activity_log')->insert(array( //content_id  is the Client ID
             array(
                'user_id'=>'2', //Inhouse 1
                'content_id'=>'5', //Agent 2's client
                'content_type'=> 'Status',
                'action' => 'Updated', 
                'description' => 'Posted a new status', 
                'details' => 'updated ClientFive', 
                'developer' => '0',
                'ip_address' => '0.0.0.0',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 ',
                'created_at' => Carbon::now()->subHour()->addMinutes(5),
                'updated_at' => Carbon::now()->subHour()->addMinutes(5)
            ),

            array( //Inhouse 1 Updated Client 5 
                'user_id'=>'2',  //Inhouse 1
                'content_id'=>'5',  //Agent 2's client
                'content_type'=> 'Status',
                'action' => 'Updated', 
                'description' => 'Posted a new status', 
                'details' => 'updated ClientFive', 
                'developer' => '0',
                'ip_address' => '0.0.0.0',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 ',
                'created_at' => Carbon::now()->subHour()->addMinutes(7),
                'updated_at' => Carbon::now()->subHour()->addMinutes(7)
            ),

            array( //Inhouse 1 Updated Client 6 
                'user_id'=>'2',  //Inhouse 1
                'content_id'=>'6',  //Agent 2's client
                'content_type'=> 'Status',
                'action' => 'Updated', 
                'description' => 'Posted a new status', 
                'details' => 'updated ClientSix', 
                'developer' => '0',
                'ip_address' => '0.0.0.0',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 ',
                'created_at' => Carbon::now()->subHour()->addMinutes(8),
                'updated_at' => Carbon::now()->subHour()->addMinutes(8)
            ),

             array( //Inhouse1 Updated Client 7
                'user_id'=>'2',  //Inhouse 1
                'content_id'=>'7',  //Agent 3's client
                'content_type'=> 'Status',
                'action' => 'Updated', 
                'description' => 'Posted a new status', 
                'details' => 'updated ClientSeven.', 
                'developer' => '0',
                'ip_address' => '0.0.0.0',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 ',
                'created_at' => Carbon::now()->subHour()->addMinutes(10),
                'updated_at' => Carbon::now()->subHour()->addMinutes(10)
            ),

            array( //Agent 2 id 5 - content_id = 2 is create new client
                'user_id'=>'5', //Agent 2
                'content_id'=>'6', //Agent 2's client
                'content_type'=> 'Add',
                'action' => 'Create', 
                'description' => 'Added a new client', 
                'details' => 'added client ClientSix.', 
                'developer' => '0',
                'ip_address' => '0.0.0.0',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 ',
                'created_at' => Carbon::now()->subDays(4)->addMinutes(12),
                'updated_at' => Carbon::now()->subDays(4)->addMinutes(12)
            ),

            array( //Agent 1 id 4 - content_id = 2 is create new client
                'user_id'=>'4', //Agent1
                'content_id'=>'1', //Agent 1's client
                'content_type'=> 'Status',
                'action' => 'Updated', 
                'description' => 'Posted a new status', 
                'details' => 'updated Client1.', 
                'developer' => '0',
                'ip_address' => '0.0.0.0',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 ',
                'created_at' => Carbon::now()->subDays(5)->addMinutes(12),
                'updated_at' => Carbon::now()->subDays(5)->addMinutes(12)
            ),

            array( //Agent 1 id 4 - content_id = 2 is create new client
                'user_id'=>'4', //Agent1
                'content_id'=>'3', //Agent 1's client
                'content_type'=> 'Status',
                'action' => 'Updated', 
                'description' => 'Posted a new status', 
                'details' => 'updated Client3.', 
                'developer' => '0',
                'ip_address' => '0.0.0.0',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 ',
                'created_at' => Carbon::now()->subDays(5)->addMinutes(14),
                'updated_at' => Carbon::now()->subDays(5)->addMinutes(14)
            ),

            array( //Inhouse1 Updated Client 8
                'user_id'=>'2',  //Inhouse 2
                'content_id'=>'8',  //Agent 3's client
                'content_type'=> 'Status',
                'action' => 'Updated', 
                'description' => 'Posted a new status', 
                'details' => 'updated ClientEight.', 
                'developer' => '0',
                'ip_address' => '0.0.0.0',
                'user_agent' => 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 ',
                'created_at' => Carbon::now()->subHour()->addMinutes(10),
                'updated_at' => Carbon::now()->subHour()->addMinutes(10)
            ),

          ));

    }

}
