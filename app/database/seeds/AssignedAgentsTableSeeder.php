<?php

//use Faker\Factory as Faker;

class AssignedAgentsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('assigned_agents')->delete();

        /* See Notes */
        //$faker = Faker::create();
        //$faker->addProvider(new Faker\Provider\Internet($faker));

        $inhouseID = User::where('email', '=', 'inhouse@mvfloans.dev')->first()->id;
        $inhouseIDTwo = User::where('email', '=', 'inhouse2@mvfloans.dev')->first()->id;
        $agent_id = User::where('email', '=', 'agent1@example.org')->first()->id;
        $agent2_id = User::where('email', '=', 'agent2@example.org')->first()->id;
        $agent3_id = User::where('email', '=', 'agent3@example.org')->first()->id;
        $agent4_id = User::where('email', '=', 'agent4@example.org')->first()->id;

        DB::table('assigned_agents')->insert(array(

             array( //Inhouse ID 2
                'user_id'=> $agent_id, //agent
                'agent_id'=> $inhouseID, //inhouse
                'created_at'=> new DateTime,
                'updated_at'=> new DateTime
            ),

            array( //Inhouse ID 2
                'user_id'=> $agent2_id, //agent
                'agent_id'=> $inhouseID, //inhouse
                'created_at'=> new DateTime,
                'updated_at'=> new DateTime
            ),

            array( //Inhouse ID 2
                'user_id'=> $agent3_id, //agent
                'agent_id'=> $inhouseID, //inhouse
                'created_at'=> new DateTime,
                'updated_at'=> new DateTime
            ),

            array( //Inhouse ID 3 
                'user_id'=> $agent4_id, //agent
                'agent_id'=> $inhouseIDTwo, //inhouse
                'created_at'=> new DateTime,
                'updated_at'=> new DateTime
            ),

        ));
    }
}