<?php

//use Faker\Factory as Faker;

class StatusUpdatesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('status_updates')->delete();

        /* See Notes */
        //$faker = Faker::create();
        //$faker->addProvider(new Faker\Provider\Internet($faker));

        DB::table('status_updates')->insert(array(

            /* ***************************************
            *  Status Updates >> ClientOne (id = 1), Agent1
            *************************************** */
             array( //Client 1 Messagex1
                'user_id'=> '4', //owner of update (Agent1)
                'client_id'=> '1',
                'status_content'=> 'Here you go! Lets get him funded!',
                'action_id'=> '21', 
                'created_at'=> Carbon::now()->subMinutes(30),
                'updated_at'=> Carbon::now()->subMinutes(30)
            ),

            array( //Client 1 Messagex2
                'user_id'=> '2', //owner of update (Inhouse = 2)
                'client_id'=> '1',
                'status_content'=> 'Thank you, I will be contacting him shortly.',
                'action_id'=> '21', 
                'created_at'=> Carbon::now()->subMinutes(20),
                'updated_at'=> Carbon::now()->subMinutes(20)
            ),

            array( //Messagex3
                'user_id'=> '4', //owner of update (Agent1)
                'client_id'=> '1',
                'status_content'=> 'Ok, keep me posted',
                'action_id'=> '21', 
                'created_at'=> Carbon::now()->subMinutes(10),
                'updated_at'=> Carbon::now()->subMinutes(10)
            ),

            array( //Messagex4
                'user_id'=> '2', //owner of update (Agent = 3)
                'client_id'=> '1',
                'status_content'=> 'Spoke to him and doesnt even know what a loan is!',
                'action_id'=> '3', 
                'created_at'=> Carbon::now()->subMinutes(5),
                'updated_at'=> Carbon::now()->subMinutes(5)
            ),

            array( //Messagex5
                'user_id'=> '4', //owner of update (Agent1)
                'client_id'=> '1',
                'status_content'=> 'Oh man, ok ill have to learn how to submit better leads.........  ;( ',
                'action_id'=> '3', 
                'created_at'=> Carbon::now()->subMinutes(2),
                'updated_at'=> Carbon::now()->subMinutes(2)
            ),

            /* ***************************************
            *  Status Updates >> ClientTwo (id = 2) - Agent1
           *************************************** */
             array( //Client 2 Messagex1
                'user_id'=> '4', //owner of update (Agent1)
                'client_id'=> '2',
                'status_content'=> 'Here is another one, 2 in 1 day :)',
                'action_id'=> '21', 
                'created_at'=> Carbon::now()->subMinutes(1),
                'updated_at'=> Carbon::now()->subMinutes(1)
            ),

            array( //Client 2 Messagex2
                'user_id'=> '2', //owner of update (Inhouse = 2)
                'client_id'=> '2',
                'status_content'=> 'Awesome job, Keep going! I will contact her soon.',
                'action_id'=> '21', 
                'created_at'=> Carbon::now()->subHour(),
                'updated_at'=> Carbon::now()->subHour()
            ),

            array( //Client 2 Messagex3
                'user_id'=> '2', //owner of update (Inhouse = 2)
                'client_id'=> '2',
                'status_content'=> 'I called but got the answering machine',
                'action_id'=> '6', 
                'created_at'=> Carbon::now()->subHour()->subMinutes(30),
                'updated_at'=> Carbon::now()->subHour()->subMinutes(30)
            ),


            /* ********************************************************
            *  Status Updates >> ClientThree (id = 3) || AGENT1 (ID = 4)
            ********************************************************** */
                 array( //Client 3 Messagex1
                    'user_id'=> '2', //owner of update (Inhouse = 2) 
                    'client_id'=> '3',
                    'status_content'=> 'I contacted him but he said he wasnt interested in a loan',
                    'action_id'=> '7', 
                     'created_at'=> Carbon::now()->subMinutes(1),
                    'updated_at'=> Carbon::now()->subMinutes(1)
                ),

                array( //Client 3 Messagex2
                    'user_id'=> '4', //owner of update (Agent = 4)
                    'client_id'=> '3',
                    'status_content'=> 'Ok, ill submit a new one later',
                    'action_id'=> '6', 
                    'created_at'=> Carbon::now()->subMinutes(56),
                    'updated_at'=> Carbon::now()->subMinutes(56)
                ),

                array( //Messagex3
                    'user_id'=> '2', //owner of update (Inhouse = 2)
                    'client_id'=> '3',
                    'status_content'=> 'Looking forward to it!',
                    'action_id'=> 'DNC', 
                    'created_at'=> Carbon::now()->subHour()->subMinutes(25),
                    'updated_at'=> Carbon::now()->subHour()->subMinutes(25)
                ),

                /* ********************************************************
                *  Status Updates >> ClientFour (id = 4) || Agent2 ID = 5
                ********************************************************** */
                     array( //Messagex1
                        'user_id'=> '2', //Inhouse1 
                        'client_id'=> '4',
                        'status_content'=> 'I contacted her and he is willing to send in his application and BS to see what we can do for him. Will keep you posted ',
                        'action_id'=> '16', 
                         'created_at'=> Carbon::now()->subMinutes(22),
                        'updated_at'=> Carbon::now()->subMinutes(22)
                    ),

                    array( //Messagex2
                        'user_id'=> '5', //owner of update (Agent2)
                        'client_id'=> '4',
                        'status_content'=> 'Thank you for updating me, let me know how it goes',
                        'action_id'=> '', 
                        'created_at'=> Carbon::now()->subHour(),
                        'updated_at'=> Carbon::now()->subHour()
                    ),

                    array( //Messagex3
                        'user_id'=> '2', //Inhouse1
                        'client_id'=> '4',
                        'status_content'=> 'I received the docs and she is submitted for approval',
                        'action_id'=> '24', 
                        'created_at'=> Carbon::now()->subHour()->subMinutes(44),
                        'updated_at'=> Carbon::now()->subHour()->subMinutes(44)
                    ),

                    array( //Messagex4
                        'user_id'=> '5', //owner of update (Agent2)
                        'client_id'=> '4',
                        'status_content'=> 'Sweet jeebiz!',
                        'action_id'=> '24', 
                        'created_at'=> Carbon::now()->subHour()->subMinutes(46),
                        'updated_at'=> Carbon::now()->subHour()->subMinutes(46)
                    ),

                    array( //Messagex5
                        'user_id'=> '2', //inhouse1
                        'client_id'=> '4',
                        'status_content'=> 'Funded, will be sending your check',
                        'action_id'=> '15', 
                        'created_at'=> Carbon::now()->subHour()->subMinutes(50),
                        'updated_at'=> Carbon::now()->subHour()->subMinutes(50)
                    ),

                    array( //Messagex6
                        'user_id'=> '5', //owner of update (Agent2)
                        'client_id'=> '4',
                        'status_content'=> 'GREAT! Lets get my other submissions funded!',
                        'action_id'=> '15', 
                        'created_at'=> Carbon::now()->subHour()->subMinutes(54),
                        'updated_at'=> Carbon::now()->subHour()->subMinutes(54)
                    ),

                /* ********************************************************
                *  Status Updates >> ClientFive (id = 5) || AGENT2 (ID = 5)
                ********************************************************** */
                 array( //Messagex1
                    'user_id'=> '5', //owner of update (Agent2) 
                    'client_id'=> '5',
                    'status_content'=> 'She already has her papers ready, please give her a call',
                    'action_id'=> '21', 
                         'created_at'=> Carbon::now()->subMinutes(3),
                        'updated_at'=> Carbon::now()->subMinutes(3)
                ),


                 /* ********************************************************
                *  Status Updates >> ClientSeven (id = 7) || AGENT3 (ID = 6)
                ********************************************************** */
                 array( //Messagex1
                    'user_id'=> '6', //owner of update (Agent3) 
                    'client_id'=> '7',
                    'status_content'=> 'Sorry, wrong phone number.  The correct number is 456-456-5555',
                    'action_id'=> '21', 
                         'created_at'=> Carbon::now()->subMinutes(12),
                        'updated_at'=> Carbon::now()->subMinutes(12)
                ),

                array( //Messagex2
                    'user_id'=> '2', //Inhouse1
                    'client_id'=> '7',
                    'status_content'=> 'Ok got it, will be contacting soon',
                    'action_id'=> '21', 
                    'created_at'=> Carbon::now()->subMinutes(34),
                    'updated_at'=> Carbon::now()->subMinutes(34)
                ),


                /* ********************************************************
                *  Status Updates >> ClientEight (id = 8) || AGENT3 (ID = 6)
                ********************************************************** */
/*                 array( //Messagex1
                    'user_id'=> '6', //owner of update (Agent3) 
                    'client_id'=> '8',
                    'status_content'=> 'Please keep me posted',
                    'action_id'=> '', 
                    'created_at'=> Carbon::now()->subMinutes(14),
                    'updated_at'=> Carbon::now()->subMinutes(14)
                ),*/

        ));
    }
}