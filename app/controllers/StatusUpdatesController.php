<?php

use MVF\Mailers\StatusUpdatesMailer as Mailer;

class StatusUpdatesController extends BaseController {

   /**
     * User Model
     * @var User
     */
    protected $user;

    /**
    * AssignedAgent Model
    * @var AssignedAgent
    */
    protected $assignedAgent;

    /**
    * StatusUpdate Model
    * @var StatusUpdate
    */
    protected $statusUpdate;

        /**
    * StatusAction Model
    * @var StatusAction
    */
    protected $statusAction;

            /**
    * Client Model
    * @var client
    */
    protected $client;

    /**
    * StatusUpdatesMailer Model
    * @var mailer
    */
    protected $mailer;

    public function __construct(User $user, AssignedAgent $assignedAgent, StatusUpdate $statusUpdate, Mailer $mailer, StatusAction $statusAction, Client $client)
    {
            parent::__construct();
            $this->user = $user;
            $this->assignedAgent = $assignedAgent;
            $this->statusUpdate = $statusUpdate;
            $this->statusAction = $statusAction;
            $this->client = $client;
            $this->mailer = $mailer;
    }

    /**
     * Display a listing of the resource. Client Status/Profile View
     * GET /statusupdates - View individual Client/Lead status and info
     *
     * @return Response
     */
    public function index($client)
    {

        list($user,$redirect) = $this->user->checkAuthAndRedirect('user/settings');
            if($redirect){return $redirect;}

        $clientObj = $this->client->where('id', '=', $client)->first();

        $statusUpdates = $this->statusUpdate->getStatusUpdates($client);
        $currentStatus = $this->statusAction->getCurrentStatus($client);
        $userLoggedin = $this->user->currentUser();

        $title = 'Client: ';

        //User doesn't own or have clients
        if (! $clientObj) {
            
            return Redirect::to('user/dashboard')->with( 'error', ('No such client'));
        
        }

        //if Logged in user ID = Clients user_id  OR if logged has role of Inhouse - proceed
         if ($userLoggedin->id == $clientObj->user_id) {

            if ($user->count()) {
        
                $canComment = false;

                if(!empty($userLoggedin)) {
        
                    $canComment = $userLoggedin->can('post_comment');
        
                }

                return View::make('site/user/client/status')
                    ->with(compact('user', 'title', 'clientObj', 'statusUpdates', 'currentStatus', 'canComment'));
        
            }

                return App::abort(404);
        
        } else { //Client doesn't belong to currently logged in user

             return Redirect::to('user/dashboard')->with( 'error', ('No such client'));

        }

    }

    /**
     * Show the form for creating a new resource.
     * GET /statusupdates/create
     *
     * @return Response
     */
    public function create()
    {
    	//
    }

    /**
     * Store a newly created resource in storage/Save Status Update and Email
     * Validate, Save Status Update Pass Comment ID to save
     * POST /statusupdates
     *
     * @return Response
     */
    public function store($client)
    {

        $userAuthor = $this->user->currentUser();

        $canComment = $userAuthor->can('post_comment');

        if ( ! $canComment) {

            return Redirect::to('user/client/'.$client.'/status')->with('error', 'You need to be logged in to post a status!');

        }

        $rules = array(
            'status_content' => 'required|min:2'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {

            $previousStatusID = $this->statusUpdate->getCurrentActionID($client);

            $newStatus = new StatusUpdate;
            $newStatus->user_id = $userAuthor->id;
            $newStatus->client_id = $client;
            $newStatus->status_content = Input::get('status_content');

            if (! $previousStatusID) {

                $newStatus->action_id = 21;

            } else {

                $newStatus->action_id = $previousStatusID;

            }

            $userAuthor->statuses()->save($newStatus);

            if ($userAuthor->statuses()->save($newStatus)) { 

                $thisClient = $this->client->where('id', '=', $client)->first();

                $inhouseAgentInfo = $this->assignedAgent->grabInhouse($userAuthor);

                //Email Inhouse
                $statusRoute = 'clientStatusInhouse'; //url
                $inhouseData = array('client' => $thisClient, 'agent' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRoute);
                $this->mailer->newStatusUpdateToInhouse($inhouseAgentInfo, $inhouseData);

                //Email Admin
                $statusRouteAdmin = 'clientStatusAdmin'; //url
                $adminData = array('client' => $thisClient, 'agent' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRouteAdmin);
                $this->mailer->newStatusUpdateToAdmin($inhouseAgentInfo, $adminData);

                Activity::log([
                    'contentId'   => $thisClient->id,
                    'contentType' => 'Status',
                    'action'      => 'Add',
                    'description' => 'Posted a new comment',
                    'details'     => 'posted a comment to '.$thisClient->first_name.'.',
                    'updated'     => $userAuthor->id ? true : false,
                ]);

                return Redirect::to('user/client/'.$client.'/status/')->with('success', 'Your comment was successfully added.');

            }

            return Redirect::to('user/client/'.$client.'/status/')->with('error', 'There was a problem adding your status, please try again.');

        }
            
        return Redirect::to('user/client/'.$client.'/status/')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     * GET /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    	//
    }

    /**
     * Show the form for editing the specified resource.
     * GET /statusupdates/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
    	//
    }

    /**
     * Update the specified resource in storage.
     * PUT /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
    	//
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    	//
    }

}