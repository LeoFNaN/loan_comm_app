<?php

class ContactController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 * GET /contact
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return View::make('/site/help');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /contact/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /contact
	 *
	 * @return Response
	 */
	public function store()
	{

		//Get all data
		$data = Input::all();

		//Validation Rules
		$rules = array(
			'name' => 'required|alpha_spaces',
			'email' => 'required|email',
			'phone' => 'min:7',
			'message' => 'required|min:3'
		);

		//Validate Data
		$validator = Validator::make ($data, $rules);

		//Data passes
		if($validator -> passes())
		{

			//Send email
	            Mail::send('emails.contact', array('data' => $data), function($message) use ($data)
	            {
	                $message->to('jim@multivisionfinancial.com', 'MVF Agent Center')->subject('MVF Agent Contact Us - '.$data['name']);
	            });
		
	            return Redirect::to('help')->with( 'success', 'Your message was sent successfully');
		}
		else
		{
		  	// Get validation errors (see Ardent package)
            	
			return Redirect::to('help')
				->withInput()
				->withErrors($validator);
		}

	}

	/**
	 * Get information Process message and email.
	 * POST /contact
	 *
	 * @return Response
	 */
	public function storeHomeInfo()
	{

		//Get all data
		$data = Input::all();

		//Validation Rules
		$rules = array(
			'name' => 'required|alpha_spaces',
			'email' => 'required|email',
			'phone' => 'min:7',
			'message' => 'required|min:5'
		);

		//Validate Data
		$validator = Validator::make ($data, $rules);

		//Data passes
		if($validator -> passes())
		{
			//Send ADMIN email
	            Mail::send('emails.admin.referralinfo', array('data' => $data), function($message) use ($data)
	            {
	                $message
	                	->to('jim@multivisionfinancial.com', 'MVF Agent Center')
	               	->subject('MVF Referral Info Request by '.$data['name']);
	            });
		
			//Send Info to Receipient
	            Mail::send('emails.referralinfo', array('data' => $data), function($message) use ($data)
	            {
	                $message
	                	->to($data['email'], 'MVF Agent Center')
	               	->subject('MVF Referral Information Request - '.$data['name']);
	            });

	            return Redirect::to('/')->with( 'success', 'Your message was sent successfully');
		}
		else
		{
		  	// Get validation errors (see Ardent package)
            	
			return Redirect::to('/')
				->withInput()
				->withErrors($validator);
		}


	}

	/**
	 * Display the specified resource.
	 * GET /contact/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /contact/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /contact/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /contact/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}