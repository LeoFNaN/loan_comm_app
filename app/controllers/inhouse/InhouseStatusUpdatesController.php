<?php

use MVF\Mailers\InhouseStatusUpdatesMailer as Mailer;

class InhouseStatusUpdatesController extends BaseController 
{

   /**
     * User Model
     * @var User
     */
    protected $user;

    /**
    * AssignedAgent Model
    * @var AssignedAgent
    */
    protected $assignedAgent;

    /**
    * StatusUpdate Model
    * @var StatusUpdate
    */
    protected $statusUpdate;

    /**
    * StatusUpdate Model
    * @var StatusAction
    */
    protected $statusAction;

    /**
    * StatusUpdatesMailer Model
    * @var mailer
    */
    protected $mailer;


    /**
    * LeadReport Model
    * @var leadReport
    */
    protected $leadReport;

    /**
    * Client Model
    * @var client
    */
    protected $client;

    public function __construct(User $user, AssignedAgent $assignedAgent, StatusUpdate $statusUpdate, Mailer $mailer, Client $client, StatusAction $statusAction, LeadReport $leadReport)
    {
            parent::__construct();
            $this->user = $user;
            $this->assignedAgent = $assignedAgent;
            $this->statusUpdate = $statusUpdate;
            $this->statusAction = $statusAction;
            $this->mailer = $mailer;
            $this->client = $client;
            $this->leadReport = $leadReport;
    }

    /**
     * Display a listing of the resource. Client Status/Profile View
     * GET /statusupdates - View individual Client status and info
     *
     * @return Response
     */
    public function index($clientID)
    {

            list($user,$redirect) = $this->user->checkAuthAndRedirect('inhouse/dashboard');

            if ($redirect) { 

                return $redirect; 
            
            }

            $clientObj = Client::with('user')->where('id', '=', $clientID)->first();

            if (!$clientObj) { //Error Catch -client doesn't exist
                
                return Redirect::to('inhouse/dashboard')->with( 'error', ('No such client'));
            
            }

            $statusUpdates = $this->statusUpdate->getStatusUpdates($clientID);
            $currentStatus = $this->statusAction->getCurrentStatus($clientID);
            $allStages = $this->statusAction->getAllStatusActions(); //populate dropdown

            $inhouseLoggedin = $this->user->currentUser();

            //Permission to view client?
            $agentMatchResult = $this->assignedAgent->agentInhouseMatch($clientObj->user_id, $inhouseLoggedin->id);

            if ($agentMatchResult == 'true') {

                    if ($user->count()) {

                        $canComment = false;

                        if (!empty($inhouseLoggedin)) {

                            $canComment = $inhouseLoggedin->can('post_comment');
                        
                        }

                        return View::make('inhouse/client/status')->with(compact('user', 'title', 'clientObj', 'statusUpdates', 'canComment', 'currentStatus', 'allStages'));

                    } else {

                        return Redirect::to('inhouse/dashboard')->with( 'error', ('No such client'));

                    }    

            } else { //Inhouse doesn't have permission

                 return Redirect::to('inhouse/dashboard')->with( 'error', ('No such client'));

            }

    }

    /**
     * Show the form for creating a new resource.
     * GET /statusupdates/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage/Save Status Update and Email
     * Validate, Save Status Update Pass Comment ID to save
     * POST /statusupdates
     *
     * @return Response
     */
    public function store($clientID)
    {

        $userAuthor = $this->user->currentUser();

        $canComment = $userAuthor->can('post_comment');

        $previousStatusID = $this->statusUpdate->getCurrentActionID($clientID);

        $statusContent = '';

        if ( ! $canComment) {

            return Redirect::to('inhouse/client/'.$clientID.'/status')->with('error', 'You need to be logged in to post a status!');

        }

        $rules = array();
        
        if (Input::get('status_action') == '5') { //comment

                $rules = array(
                   'status_content' => 'required|min:2'
                );

        }

        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->passes()) {

                $statusContent = $this->statusUpdate->setStatusContent(Input::get('status_content'), Input::get('status_action'));

                if ( ! $previousStatusID ) { //Blank then no status ever declared keep it Queued, Fail Safe

                        $currentAction = '21'; 

                } elseif (Input::get('status_action') == "5") {//if comment then keep previous/current status

                        $currentAction = $previousStatusID;

                } else {//New status action declared

                        $currentAction = Input::get('status_action');

                }

                $thisClient = $this->client->where('id', '=', $clientID)->first();

                $newStatus = new StatusUpdate;
                $newStatus->user_id = $userAuthor->id;
                $newStatus->client_id = $clientID;
                $newStatus->status_content = $statusContent;
                $newStatus->action_id =  $currentAction;

                $userAuthor->statuses()->save($newStatus);

                if ($userAuthor->statuses()->save($newStatus)) {                      

                       $agentInfo = $this->user->where('id', '=', $thisClient->user_id)->first();

                        if (Input::get('notify_agent') == "Yes") {

                           //Email to Agent
                            $statusRoute = 'clientStatus';
                            $agentData = array('client' => $thisClient, 'agent' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRoute);
                            $this->mailer->newStatusUpdateToAgent($agentInfo, $agentData);

                        }

                        //Email/Notify Admins
                        $statusRouteAdmin = 'clientStatusAdmin';
                        $adminData = array('client' => $thisClient, 'author' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRouteAdmin, 'refAgent' => $agentInfo);
                        $this->mailer->newStatusUpdateToAdmin($agentInfo, $adminData); //(Refactor AgentInfo Object)

                       $actionName = $this->statusAction->getActionByName($currentAction);

                      if (Input::get('status_action') == "5" || Input::get('status_action') == $previousStatusID) {

                            //Generic comment only
                            Activity::log([
                                'contentId'   => $thisClient->id, //client
                                'contentType' => 'Status',
                                'action'      => 'Add',
                                'description' => 'Posted a new status',
                                'details'     => 'updated '.$thisClient->first_name.'.',
                                'updated'     => $userAuthor->id ? true : false,
                            ]);

                        } else {

                            //Log with status change
                            Activity::log([
                                'contentId'   => $thisClient->id, //client
                                'contentType' => 'Status',
                                'action'      => 'Add',
                                'description' => 'Posted a new status',
                                'details'     => 'updated '.$thisClient->first_name.' to '.$actionName.'.',
                                'updated'     => $userAuthor->id ? true : false,
                            ]);
                        
                        }

                    $reportResult = $this->leadReport->qualityCount($thisClient->user_id, $previousStatusID, $currentAction);

                    return Redirect::to('inhouse/client/'.$clientID.'/status/')->with('success', 'Client status successfully updated.');

                } else {

                    return Redirect::to('inhouse/client/'.$clientID.'/status/')->with('error', 'There was a problem adding your status, please try again.');

                }

            }

        return Redirect::to('inhouse/client/'.$clientID.'/status/')->withInput()->withErrors($validator);

    }

     /**
    *  List of Clients View for Inhouse
    *  Quick update action for current status
    *  Datatable called by inhouse/show.blade.php
    */
    public function getQuickStatus($clientID)
    {

        list($user,$redirect) = $this->user->checkAuthAndRedirect('inhouse/dashboard');

        if ($redirect) { 

            return $redirect;

        }

        //Retrieve Client
        $clientObj = $this->client->select('id','first_name','last_name', 'user_id')->where('id', '=', $clientID)->first();

        if (!$clientObj) { //Error Catch -client doesn't exist
            
            return Redirect::to('inhouse/dashboard')->with( 'error', ('No such client'));
        
        }

        $statusUpdates = $this->statusUpdate->getStatusUpdates($clientID);
        $currentStatus = $this->statusAction->getCurrentStatus($clientID);
        $allStages = $this->statusAction->getAllStatusActions(); //populate dropdown

        $inhouseLoggedin = $this->user->currentUser();

        //Does Inhouse have permission to view client
        $agentMatchResult = $this->assignedAgent->agentInhouseMatch($clientObj->user_id, $inhouseLoggedin->id);

        if ($agentMatchResult == 'true') {

                if ($user->count()) {

                    $canComment = false;

                    if (!empty($inhouseLoggedin)) {

                        $canComment = $inhouseLoggedin->can('post_comment');

                    }

                    return View::make('inhouse/client/quickstatus')
                        ->with(compact('user', 'title', 'clientObj', 'statusUpdates', 'canComment', 'currentStatus', 'allStages'));

                } else {

                    return Redirect::to('inhouse/dashboard')->with( 'error', ('No such client'));

                }    

        } else { //Inhouse doesn't have permission

             return Redirect::to('inhouse/dashboard')->with( 'error', ('No such client'));

        }

    }

        /**
     * Store a newly created resource in storage/Save Status Update and Email
     * Validate, Save Status Update Pass Comment ID to save
     * POST /statusupdates
     *
     * @return Response
     */
    public function postQuickStatus($clientID)
    {

        $userAuthor = $this->user->currentUser();

        $canComment = $userAuthor->can('post_comment');

        $previousStatusID = $this->statusUpdate->getCurrentActionID($clientID);

        $statusContent = '';

        if ( ! $canComment) {

            return Redirect::to('inhouse/client/'.$clientID.'/quickstatus')
                ->with('error', 'You need to be logged in to post a status!');

        }

        $rules = array();
        
        if (Input::get('status_action') == '5') { //comment only

                //Add Rule to Validator: Check if comment entered
               $rules = array(
                   'status_content' => 'required|min:2'
               );

        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {

            $statusContent = $this->statusUpdate->setStatusContent(Input::get('status_content'), Input::get('status_action'));

            if ( ! $previousStatusID ) { //Blank then no status ever declared keep it Queued, Fail Safe

                    $currentAction = '21'; 

            } elseif (Input::get('status_action') == "5") {//if comment only then keep previous/current status

                    $currentAction = $previousStatusID;

            } else {//New status action declared

                    $currentAction = Input::get('status_action');

            }

            $agentID = $this->client->select('user_id')->where('id', '=', $clientID)->first();

            $newStatus = new StatusUpdate;
            $newStatus->user_id = $userAuthor->id;
            $newStatus->client_id = $clientID;
            $newStatus->status_content = $statusContent;
            $newStatus->action_id =  $currentAction;
            $newStatus->updated_at = Carbon::now();

            $userAuthor->statuses()->save($newStatus);

            if ($userAuthor->statuses()->save($newStatus)) {

                $thisClient = $this->client->where('id', '=', $clientID)->first(); //client Info
                $agentInfo = $this->user->where('id', '=', $thisClient->user_id)->first();

                if (Input::get('notify_agent') == "Yes") {

                    //Email to Agent
                    $statusRoute = 'clientStatus';
                    $agentData = array('client' => $thisClient, 'agent' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRoute);
                    $this->mailer->newStatusUpdateToAgent($agentInfo, $agentData);

                }

                //Email to Admins
                $statusRouteAdmin = 'clientStatusAdmin';
                $adminData = array('client' => $thisClient, 'author' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRouteAdmin, 'refAgent' => $agentInfo);
                $this->mailer->newStatusUpdateToAdmin($agentInfo, $adminData); //agentInfo just a placeholder (Refactor)

                $actionName = $this->statusAction->getActionByName($currentAction);

                if(Input::get('status_action') == "5"){

                    //Log activity generic comment only
                    Activity::log([
                        'contentId'   => $thisClient->id, //client
                        'contentType' => 'Status',
                        'action'      => 'Add',
                        'description' => 'Posted a new status',
                        'details'     => 'updated '.$thisClient->first_name.'.',
                        'updated'     => $userAuthor->id ? true : false,
                    ]);

                } else {

                    //Log activity with status change
                    Activity::log([
                        'contentId'   => $thisClient->id, //client
                        'contentType' => 'Status',
                        'action'      => 'Add',
                        'description' => 'Posted a new status',
                        'details'     => 'updated '.$thisClient->first_name.' to '.$actionName.'.',
                        'updated'     => $userAuthor->id ? true : false,
                    ]);
                
                }

                $reportResult = $this->leadReport->qualityCount($agentID->user_id, $previousStatusID, $currentAction);
                
                return Redirect::to('inhouse/client/'.$clientID.'/quickstatus')->with('success', 'Your status was successfully added.');

            } else {

                return Redirect::to('inhouse/client/'.$clientID.'/quickstatus')->with('error', 'There was a problem adding your status, please try again.');

            }

        }

        return Redirect::to('inhouse/client/'.$clientID.'/quickstatus')->withInput()->withErrors($validator);

    }

    /**
     * Display the specified resource.
     * GET /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /statusupdates/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}