<?php

class InhouseController extends BaseController {

	/**
	 * User Model Injection
	 * @var User
	 */
	protected $user;

	/**
	* AssignedAgent Model
	* @var $assignedAgent
	*/
	protected $assignedAgent;

	/**
	* ActivityLog Model
	* @var $activityLog
	*/
	protected $activityLog;

	/**
	* Client Model
	* @var $client
	*/
	protected $client;

	/**
	* Activity Class Model
	* @var $activityLog
	*/
	protected $statusUpdates;

	/**
	* StatusUpdate Model
	* @var StatusAction
	*/
	protected $statusAction;

	/**
	* LeadReport Model
	* @var leadReport
	*/
	protected $leadReport;

	/**
	 * Inject the models.
	 * @param User $user
	 */
	public function __construct(User $user, AssignedAgent $assignedAgent, Activity $activityLog, Client $client, StatusUpdate $statusUpdates, StatusAction $statusAction, LeadReport $leadReport)
	{
		parent::__construct();
		$this->user = $user;
		$this->assignedAgent = $assignedAgent;
		$this->activityLog = $activityLog;
		$this->leadReport = $leadReport;
		$this->client = $client;
		$this->statusUpdates = $statusUpdates;
		$this->statusAction = $statusAction;
	}

	/**
	 * Inhouse Main Login Page
	 * 
	 * @return View
	 */
	public function getIndex()
	{

		$userLoggedin = $this->user->currentUser();

		$listofClientsIds = $this->client->getClientsIDByAllAgents($userLoggedin->id);

		$paginator = $this->user->getRecentActivityTimeFrame($listofClientsIds, 15, 4);

		$allStages = $this->statusAction->getAllStatusActions(); //populate dropdown

		if($userLoggedin->hasRole('Agent'))
		{
			return Redirect::to('user/dashboard')->with(compact('userLoggedin'));
		}
		elseif($userLoggedin->hasRole('admin'))
		{
			return Redirect::to('admin/dashboard')->with(compact('userLoggedin'));
		} 

		return View::make('inhouse/dashboard', compact('userLoggedin', 'paginator', 'allStages'));
 
	}

	/**
	* Show list of Agents
	* Display agents underneath Inhouse
	*/
	public function getShowAgents()
	{

		$userLoggedin = $this->user->currentUser();

		$listofClientsIds = $this->client->getClientsIDByAllAgents($userLoggedin->id);

		$paginator = $this->user->getRecentActivityTimeFrame($listofClientsIds, 30, 5);

		return View::make('inhouse/agents', compact('userLoggedin', 'paginator'));
	}

	/**
	*  List of Clients View
	*  Inhouse View List of clients submitted by Specific Agent ($user)
	*/
	public function getShowList($agentID)
	{

		//User doesn't exist then redirect
		$resultsRedirect = $this->user->find($agentID);
		
		if (is_null($resultsRedirect)){
			return Redirect::to('inhouse/dashboard')->with( 'error', ('No such agent exists.'));
		}
		
		$inhouseLoggedin = $this->user->currentUser();

		$agentAssigned = $this->user->find($agentID);

		$agentMatchResult = $this->assignedAgent->agentInhouseMatch($agentAssigned->id, $inhouseLoggedin->id);

		$allStages = $this->statusAction->getAllStatusActions(); //populate dropdown

		if($agentMatchResult == 'true') 
		{

			$listofClientsIds = $this->client->getClientsIDByAgent($agentID);
			$results = $this->user->getRecentActivityTimeFrame($listofClientsIds, 90, 5); //3 months

			return View::make('inhouse/show')->with(compact('agentAssigned', 'results', 'inhouseLoggedin', 'allStages'));
		}

		return Redirect::to('inhouse/dashboard')->with( 'error', ('No such agent exists.'));

	}

	/**
	 * Stores new Client
	 * Inhouse can add a new Client (Not implemented yet 10/24/14)
	 * Do Mailer abstraction (12/26/14)
	 */
	public function postIndex()
	{

		$this->user->office_id = $this->user->mtrandstr(7, '0123456789ABCDEF');
		$this->user->first_name = Input::get('first_name'); 
		$this->user->last_name = Input::get('last_name');
		$this->user->phone_number = Input::get('phone_number');
		$this->user->email = Input::get( 'email' );
		$this->user->referral_source = Input::get( 'referral_source');
		$this->user->agent_reason = Input::get('agent_reason');
		$this->user->username = Input::get( 'username', false);

		$password = Input::get( 'password' );
		$passwordConfirmation = Input::get( 'password_confirmation' );

		if(!empty($password)) {
			if($password === $passwordConfirmation) {
				$this->user->password = $password;
				// The password confirmation will be removed from model
				// before saving. This field will be used in Ardent's
				// auto validation.
				$this->user->password_confirmation = $passwordConfirmation;
			} else {
				// Redirect to the new user page
				return Redirect::to('signup')
					->withInput(Input::except('password','password_confirmation'))
					->with('error', Lang::get('admin/users/messages.password_does_not_match'));
			}
		} else {
			unset($this->user->password);
			unset($this->user->password_confirmation);
		}

		//Save if valid. Password field will be hashed before save
		$this->user->save();

		if ( $this->user->id )
		{
			$newUser = $this->user->where('id', '=', $this->user->id)->first();

			//ADD LEAD REPORT INSTANCE HERE

			Mail::send('emails.registration', array('user' => $newUser), function($message)
			{
				$message->to('admin@multivisionfinancial.com', 'MVF Agent Center')
				->cc('jim@multivisionfinancial.com', 'Referral Agent Director')
				->subject('MVF: New Agent Signup');
			});

			// Redirect with success message, You may replace "Lang::get(..." for your custom message.
			return Redirect::to('user/login')
				->with( 'notice', Lang::get('user/user.user_account_created') );
		}
		else
		{
			// Get validation errors (see Ardent package)
			$error = $this->user->errors()->all();

			return Redirect::to('signup')
				->withInput(Input::except('password'))
				->with( 'error', $error );
		}
	}

	/**
	 * Inhouse profile editing
	 * (Not implemented yet - 10/24/14) >> Follow postEdit from UsersController
	 */
	public function postEdit($user)
	{


	}

	/**
	*   Get Inhouse profile
	*   Update email, phone, password, etc...
	*/
	public function getProfile()
	{
		list($user,$redirect) = $this->user->checkAuthAndRedirect('user/profile');
		if($redirect){return $redirect;}

		if (is_null($user))
		{
			return App::abort(404);
		}
		return View::make('site/user/dash/profile', compact('user')); //Inhouse and User shares same profile blade template
	}

	/**
	*   Get Agent profile
	*   Lead Reports, Updates, Image, Information
	*/
	public function getAgentProfile($agentID)
	{

		$agent = $this->user->select()->where('id', '=', $agentID)->first();

		$leadReport = $this->leadReport->select()->where('user_id', '=', $agentID)->first();

		return View::make('/inhouse/agent-profile', compact('agent', 'leadReport')); //Inhouse and User shares same profile blade template
	}

	/*
	*   Display Create Client Form
	*   Not yet implemented 10/24/14
	*/
	public function getCreateClient()
	{
		/*  For the inhouse, add a dropdown of all the Agents that are assigned to them in the Form  */
		return View::make('site/user/client/create');
	}

	/*
	*   Store New Client
	*   Validate, Create, Save New Client submitted by Inhouse for an agent
	*   Pass Comment ID to save 
	*  Note implemented yet 10/24/14
	*  Do Mailer abstraction 12/26/14 (Check out StatsUpdatesController, Mailer.php)
	*/
	public function postClient()
	{
		//Grab Logged In User
		$userLoggedin = $this->user->currentUser();

		// Declare the rules for the form validation
		$rules = array(
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'phone' => 'required|min:7|unique:clients',
			'email' => 'required|email|unique:clients',
			'business_name' => '',
			'loan_amount' => '',
			'business_years' => '',
			'state' => '',
			'monthly_revenue' => '',
			'callback_time' => '',
			'client_message' => 'min:2'
		);

		// Validate the inputs
		$validator = Validator::make(Input::all(), $rules);

			if ($validator->passes())
			{
				$client = new Client;

					$client->user_id = $userLoggedin->id; //Submitted Agents Id
					$client->first_name = Input::get('first_name');
					$client->last_name = Input::get('last_name');
					$client->phone = Input::get('phone');
					$client->email = Input::get('email');
					$client->business_name = Input::get('business_name');
					$client->loan_amount = Input::get('loan_amount');
					$client->business_years = Input::get('business_years');
					$client->state = Input::get('state');
					$client->monthly_revenue = Input::get('monthly_revenue');
					$client->callback_time = Input::get('callback_time');
					$client->client_message = Input::get('client_message');
					/*
						INHOUSE is REQUIRED to assign to an agent that is under them, if it's their own client then put in velocify
					*/

				/*
						Since this is an inhouse agent then remove all instances of Inhouseagent info and the IF Elses for emailing as well,
						I Will edit this when I get to inhouse agents adding in their own clients (send notification to admin, jim and Agent)
				*/

				//Grab Inhouse Agent
				$inhouseAgentData = $this->assignedAgent->grabInhouse($userLoggedin);
				$userLoggedin->clients()->save($client);

				//Check if assigned to agent
				if(count($inhouseAgentData)){  

						 //Director Dupe Email Fix > If inhouse is Director then just email him and admin
						if($inhouseAgentData->email == 'jim@multivisionfinancial.com')
						{
							Mail::send('emails.newclient', array('client' => $client, 'agent' => $userLoggedin, 'inhouse' => $inhouseAgentData), function($message) use ($inhouseAgentData){
								$message->to('admin@multivisionfinancial.com', 'MVF Agent Center')
								->cc('jim@multivisionfinancial.com', 'MVF Referral Director')
								->subject('MVF: New Client Added');
							});   

						//Director is not the inhouse then email everyone
						}else{

							Mail::send('emails.newclient', array('client' => $client, 'agent' => $userLoggedin, 'inhouse' => $inhouseAgentData), function($message) use ($inhouseAgentData)
							{
								$message->to('admin@multivisionfinancial.com', 'MVF Agent Center')
								->cc('jim@multivisionfinancial.com', 'MVF Referral Director')
								->cc($inhouseAgentData->email, 'MVF Inhouse Agent') //email Agent or Inhouse
								->subject('MVF: New Client Added');
							});

						}

				 }else{//if the Agent doesn't have an Inhouse then just email to Admin

					Mail::send('emails.newclient', array('client' => $client, 'agent' => $userLoggedin), function($message)
					{
						$message->to('admin@multivisionfinancial.com', 'MVF Agent Center')
						->cc('jim@multivisionfinancial.com', 'MVF Referral Director')
						->subject('MVF: New Client Added');
					});

				}

					return Redirect::to('user/dashboard')->with('success', 'Your client was successfully added.');
				
			}
			else
			{
				 // Redirect to edit page w errors
				return Redirect::to('user/client/create')->withInput()->withErrors($validator);
			}
	}

	 /**
	 * DATATABLE
	*  List of Agents managed by Inhouse
	*  View List of Agents/Users who are managed by an Inhouse Agent ($user)
	*/
	public function getListData()
	{

		$userLoggedin = $this->user->currentUser();

		$agents = $this->user->select(array(
			'users.id',
			'users.office_id',
			DB::raw('CONCAT(users.first_name," ",users.last_name) as userName'),
			'users.email',
			'users.phone_number',
			'users.created_at'
		))
		->leftJoin('assigned_agents', 'users.id', '=', 'assigned_agents.user_id')
		->where('assigned_agents.agent_id', '=', $userLoggedin->id);
 
		return Datatables::of($agents)
			->edit_column('created_at', function($agent) use ($userLoggedin){

					$listofClientsIds = $this->client->getClientsIDByAgent($agent->id);

					$results = $this->activityLog->select('updated_at')->whereIn('activity_log.content_id', $listofClientsIds)->orderBy('activity_log.updated_at', 'DESC')->first();

					if($results){
						return Carbon::parse($results->updated_at)->format('m/d/y');
					}

					return '00/00/00';

				}
			)
			->edit_column('email', "{{ HTML::mailto(\$email) }}")
			->edit_column('phone_number', function($agent)
				{
					$number = $agent->phone_number;

					if(strlen($number) == 7){
						$formatted_number = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $number);
					}
					elseif(strlen($number) == 10){
						$formatted_number = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $number);
					}
					else{
						$formatted_number = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $number);
					}

					return $formatted_number;
				}
			)
			->add_column('actions', function ($agent) {
				return "<a title='View Clients' alt='View Clients' style='margin-left: 5px;' href='/inhouse/show/" . $agent->id . "' class='iframe btn btn-xs btn-info'><span class='icon icon-users'></span></a> 

				<a title='View Agent Profile' alt='View Agent Profile' style='' href='/inhouse/agent/".$agent->id."/profile' class='btn btn-xs'><span class='glyphicon glyphicon-user'></span></a>";
				
			})
			->remove_column('id')
			->make();
	}

	 /**
	*  Datatable called by inhouse/show.blade.php
	*  List of Clients View for Inhouse
	*  View List of clients submitted by an Agent ($user)
	*/
	public function getClientDataList($user)
	{

		$clients = $this->client->select(array(
			'clients.id', 
			DB::raw('CONCAT(clients.first_name," ",clients.last_name) as ClientName'),
			'clients.email',
			'clients.created_at'
		))
		->where('clients.user_id', '=', $user);

		return Datatables::of($clients)
		->edit_column('email', "{{ HTML::mailto(\$email) }}")
		->edit_column('created_at', "{{{ Carbon::parse(\$created_at)->format('m/d/y') }}}")
		->add_column('updated', function($client){

				$results = $this->statusUpdates->select('updated_at')->where('status_updates.client_id', '=', $client->id)->orderBy('status_updates.updated_at', 'DESC')->first();

				if($results){
					return Carbon::parse($results->updated_at)->format('m/d/y');
				}

				return '00/00/00';

		})
		 ->add_column('status', function ($client) {

				$currentStatus = $this->statusUpdates->select('action_id')->where('client_id', '=', $client->id)->orderBy('status_updates.updated_at', 'DESC')->first();

				if($currentStatus){

					return $this->statusAction->getActionByName($currentStatus->action_id);

				}
				else{

					return 'Queued In System';

				}

		})
		->add_column('actions', function ($client) {
			return "<a title='View Profile' style='margin-left: 2px;' href='/inhouse/client/" . $client->id . "/status/' class='btn btn-xs btn-info'><span class='glyphicon glyphicon-comment'></span></a> 
				
		  <a title='Quick Update' style='margin-left: 2px;' href='/inhouse/client/" . $client->id . "/quickstatus' class='iframeQuickStatus btn btn-xs btn-info'><span class='glyphicon glyphicon-plus-sign'></span></a> 

			";
		})
		->remove_column('id')
		->make(); 
	}


	 /**
	*  Datatable called by inhouse/show.blade.php
	*  Called by inhouse/dashboard - List of Clients View for Inhouse
	*  View List of clients all clients belonging to Agents underneath current inhouse
	*/
	public function getClientDataListAll($inhouseID)
	{

		$clients = $this->client->select(array('clients.id', 
			DB::raw('CONCAT(clients.first_name," ",clients.last_name) as ClientName'),
			'clients.email',
			DB::raw('CONCAT(users.first_name, " ", users.last_name) as agentName'),
			'clients.created_at'))
             ->join('assigned_agents', function($join) {

                    $join->on('clients.user_id', '=', 'assigned_agents.user_id');

                })
             ->join('users', function($join){

             	$join->on('clients.user_id', '=', 'users.id');

             })
             ->where('assigned_agents.agent_id', '=', $inhouseID);

		return Datatables::of($clients)
		->edit_column('email', "{{ HTML::mailto(\$email) }}")
		->edit_column('created_at', "{{{ Carbon::parse(\$created_at)->format('m/d/y') }}}")
		->add_column('updated', function($client){

					$results = $this->statusUpdates->select('updated_at')->where('status_updates.client_id', '=', $client->id)->orderBy('status_updates.updated_at', 'DESC')->first();

					if($results){

						return Carbon::parse($results->updated_at)->format('m/d/y');

					}

					else{

						return '00/00/00';

					}

				}

			)
		 ->add_column('status', function ($client) {

				$currentStatus = $this->statusUpdates->select('action_id')->where('client_id', '=', $client->id)->orderBy('status_updates.updated_at', 'DESC')->first();

				if($currentStatus){

					return $this->statusAction->getActionByName($currentStatus->action_id);

				}
				else{

					return 'Queued In System';

				}

		})

		->add_column('actions', function ($client) {
			return "<a title='View Profile' style='margin-left: 2px;' href='/inhouse/client/" . $client->id . "/status/' class='btn btn-xs btn-info'><span class='glyphicon glyphicon-comment'></span></a> 
		  		<a title='Quick Update' style='margin-left: 2px;' href='/inhouse/client/" . $client->id . "/quickstatus' class='iframeQuickStatus btn btn-xs btn-info'><span class='glyphicon glyphicon-plus-sign'></span></a> 

			";
		})
		->remove_column('id')
		->make(); 
	}


	/**
	 * Process a dumb redirect.
	 * @param $url1
	 * @param $url2
	 * @param $url3
	 * @return string
	 */
	public function processRedirect($url1,$url2,$url3)
	{
		$redirect = '';
		if( ! empty( $url1 ) )
		{
			$redirect = $url1;
			$redirect .= (empty($url2)? '' : '/' . $url2);
			$redirect .= (empty($url3)? '' : '/' . $url3);
		}
		return $redirect;
	}
}