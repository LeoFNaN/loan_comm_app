<?php

use MVF\Mailers\UserMailer as Mailer;
use MVF\Mailers\ClientMailer as ClientMailer;

class UserController extends BaseController 
{

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
    * AssignedAgent Model
    * @var $assignedAgent
    */
    protected $assignedAgent;

    /**
    * Client Model
    * @var $client
    */
    protected $client;

    /**
    * Mailer Class Model
    * @var $mailer
    */
    protected $mailer;

    /**
    * ClientMailer Class Model
    * @var $mailer
    */
    protected $clientMailer;

    /**
    * Activity Class Model
    * @var $activityLog
    */
    protected $activityLog;

    /**
    * LeadReport Model
    * @var leadReport
    */
    protected $leadReport;

    /**
    * Activity Class Model
    * @var $activityLog
    */
    protected $statusUpdates;

    /**
    * Activity Class Model
    * @var $activityLog
    */
    protected $statusAction;

    /**
     * Inject the models.
     * @param User $user
     */

    public function __construct(User $user, AssignedAgent $assignedAgent, Client $client, Mailer $mailer, ClientMailer $clientMailer, Activity $activityLog, StatusUpdate $statusUpdates, StatusAction $statusAction, LeadReport $leadReport)
    {
        parent::__construct();
        $this->user = $user;
        $this->assignedAgent = $assignedAgent;
        $this->client = $client;
        $this->mailer = $mailer;
        $this->clientMailer = $clientMailer;
        $this->activityLog = $activityLog;
        $this->leadReport = $leadReport;
        $this->statusAction = $statusAction;
        $this->statusUpdates = $statusUpdates;
    }

    /**
     * Users Dashboard
     * 
     * @return View
     */
    public function getIndex()
    {

        list($user,$redirect) = $this->user->checkAuthAndRedirect('user');

        if ($redirect) {
        
            return $redirect;
        
        }

        if ($user->hasRole('Inhouse')) {

            return Redirect::to('inhouse/dashboard')->with(compact('user'));

        } elseif ($user->hasRole('admin')) {

            return Redirect::to('admin/dashboard')->with(compact('user'));

        }

        $assignedAgents = $this->assignedAgent->where('user_id', '=', $user->id)->first();

        if (count($assignedAgents)) {

            $inhouseAgent = $this->user->where('id', '=', $assignedAgents->agent_id)->first();

            $allStages = $this->statusAction->getAllStatusActions(); //populate dropdown

            $listofClientsIds = $this->client->getClientsIDByAgent($user->id);

            $results = $this->user->getRecentActivityTimeFrame($listofClientsIds, 8, 5);

        } else {

            $inhouseAgent = Null;

        }

        return View::make('site/user/dash/dashboard')->with(compact('user', 'inhouseAgent', 'results', 'allStages'));

    }

    /**
     * Stores new user
     * New Signup
     */
    public function postIndex()
    {
 
        $repo = App::make('UserRepository');
        $user = $repo->signup(Input::all());

        if ($user->id) {

            $newUser = $this->user->where('id', '=', $user->id)->first();

            //Email Admin
            $agentData = array('user' => $newUser);
            $this->mailer->newSignUpToAgent($newUser, $agentData);

            //Email Admin
            $adminData = array('user' => $newUser);
            $this->mailer->newSignUpToAdmin($newUser, $adminData);

            $reportResult = $this->leadReport->createLeadReportRecord($newUser->id);

            return Redirect::to('user/login')
                ->with( 'notice', Lang::get('user/user.user_account_created') );

        } else {

            $error = $user->errors()->all();

            return Redirect::to('signup')
                ->withInput(Input::except('password'))
                ->with( 'error', $error );
                
        }

    }

    /**
     * Edits a user
     *
     */
    public function postEdit($user)
    {

        $oldUser = clone $user;

        $user->first_name = Input::get( 'first_name' );
        $user->last_name = Input::get( 'last_name' );
        $user->email = Input::get( 'email' );
        $user->phone_number = Input::get( 'phone_number' );
        $password = Input::get( 'password' );
        $passwordConfirmation = Input::get( 'password_confirmation' );

        if (!empty($password)) {

            if ($password != $passwordConfirmation) {

                // Redirect to the new user page
                $error = Lang::get('admin/users/messages.password_does_not_match');

                return Redirect::to('user/profile')
                    ->with('error', $error);

            } else {

                $user->password = $password;

                $user->password_confirmation = $passwordConfirmation;

            }

        }

        if ($user->save()) {

            return Redirect::to('user/profile/')
                ->with( 'success', Lang::get('user/user.user_account_updated') );

        } else {

            $error = $user->errors()->all(':message');

            return Redirect::to('user/profile/')
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', $error);

        }

    }

    /**
     * Displays the form for user creation
     *
     */
    public function getCreate()
    {

        return View::make('site/signup');

    }

    /**
     * Displays the login form
     *
     */
    public function getLogin()
    {

        $user = Auth::user();

        if (!empty($user->id)) {

            return Redirect::to('/');
        
        }

        return View::make('site/user/login');

    }

    /**
     * Attempt to do login
     *
     */
    public function postLogin()
    {

        $repo = App::make('UserRepository');
        $input = Input::all();

        if ($repo->login($input)) {

            $userLoggedin = $this->user->currentUser();
            
            if ($userLoggedin->hasRole('Inhouse')) {
               
                return Redirect::to('inhouse/dashboard')->with(compact('userLoggedin'));
            
            } elseif ($userLoggedin->hasRole('admin')) {

                return Redirect::to('admin/dashboard')->with(compact('userLoggedin'));
            
            } 

            return Redirect::intended('/user/dashboard');

        } else {

            if ($repo->isThrottled($input)) {

                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');

            } elseif ($repo->existsButNotConfirmed($input)) {

                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');

            } else {

                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');

            }

            return Redirect::to('user/login')
                ->withInput(Input::except('password'))
                ->with( 'error', $err_msg );

        }

    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string  $code
     */
    public function getConfirm($code)
    {

        if (Confide::confirm($code)) {

            return Redirect::to('user/login')
                ->with( 'notice', Lang::get('confide::confide.alerts.confirmation') );

        } else {

            return Redirect::to('user/login')
                ->with( 'error', Lang::get('confide::confide.alerts.wrong_confirmation') );

        }

    }

    /**
     * Displays the forgot password form
     *
     */
    public function getForgot()
    {

        return View::make('site/user/forgot');

    }

    /**
     * Attempt to reset password with given email
     *
     */
    public function postForgot()
    {

        if (Confide::forgotPassword(Input::get( 'email' ))) {

            return Redirect::to('user/login')
                ->with( 'notice', Lang::get('confide::confide.alerts.password_forgot') );

        } else {

            return Redirect::to('user/forgot')
                ->withInput()
                ->with( 'error', Lang::get('confide::confide.alerts.wrong_password_forgot') );

        }

    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function getReset($token)
    {

        return View::make('site/user/reset')
            ->with('token',$token);
            
    }


    /**
     * Attempt change password of the user
     *
     */
    public function postReset()
    {
        
        $repo = App::make('UserRepository');
        $input = array(
            'token'=>Input::get( 'token' ),
            'password'=>Input::get( 'password' ),
            'password_confirmation'=>Input::get( 'password_confirmation' ),
        );

        // By passing an array with the token, password and confirmation
         if ($repo->resetPassword($input)) {

            return Redirect::to('user/login')
            ->with( 'notice', Lang::get('confide::confide.alerts.password_reset') );

        } else {

            return Redirect::to('user/reset/'.$input['token'])
                ->withInput()
                ->with( 'error', Lang::get('confide::confide.alerts.wrong_password_reset') );

        }

    }

    /**
     * Log the user out of the application.
     *
     */
    public function getLogout()
    {

        Confide::logout();
        return Redirect::to('/');
    
    }

    public function getSettings()
    {

        list($user,$redirect) = $this->user->checkAuthAndRedirect('user/settings');
        
        if ($redirect) { 
        
            return $redirect;
        
        }

        if (is_null($user)) {

            return App::abort(404);

        }

        return View::make('site/user/dash/profile', compact('user'));

    }

    /**
     * Process a dumb redirect.
     * @param $url1
     * @param $url2
     * @param $url3
     * @return string
     */
    public function processRedirect($url1, $url2, $url3)
    {

        $redirect = '';

        if (! empty($url1)) {

            $redirect = $url1;
            $redirect .= (empty($url2)? '' : '/' . $url2);
            $redirect .= (empty($url3)? '' : '/' . $url3);

        }

        return $redirect;

    }

    /*
    *   Display Create Client Form
    *   Validate, Save New Client, Send Email Trigger to Admin
    */
    public function getCreateClient()
    {

        return View::make('site/user/client/create');

    }

    /*
    *   Store New Client
    *   Validate, Create, Save New Client submitted by Agent
    *   Pass Comment ID to save
    */
    public function postClient()
    {

        $userLoggedin = $this->user->currentUser();

        $rules = array(
            'first_name' => 'required|min:1',
            'last_name' => 'required|min:1',
            'phone' => 'required|min:7|unique:clients',
            'email' => 'required|email|unique:clients',
            'business_name' => '',
            'loan_amount' => '',
            'business_years' => '',
            'state' => '',
            'monthly_revenue' => '',
            'callback_time' => '',
            'client_message' => 'min:2'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {

            $inhouseAgentData = $this->assignedAgent->grabInhouse($userLoggedin);

            //Verify Assignment to Inhouse
            if (count($inhouseAgentData)) {

                $client = new Client;

                $client->user_id = $userLoggedin->id; //Submitted Agents Id
                $client->first_name = Input::get('first_name');
                $client->last_name = Input::get('last_name');
                $client->phone = Input::get('phone');
                $client->email = Input::get('email');
                $client->business_name = Input::get('business_name');
                $client->loan_amount = Input::get('loan_amount');
                $client->business_years = Input::get('business_years');
                $client->state = Input::get('state');
                $client->monthly_revenue = Input::get('monthly_revenue');
                $client->callback_time = Input::get('callback_time');
                $client->client_message = Input::get('client_message');

                $userLoggedin->clients()->save($client);

                $setStatus = new StatusUpdate;
                
                $setStatus->user_id = $userLoggedin->id;
                $setStatus->client_id = $client->id;
                $setStatus->status_content = 'Status: Queued In System';
                $setStatus->action_id = 21;

                $userLoggedin->statuses()->save($setStatus);

                //Welcome Email to Client
                $clientData = array('client' => $client, 'agent' => $userLoggedin, 'inhouse' => $inhouseAgentData);

                $this->clientMailer->welcomeEmailToClient($client, $clientData);

                //Inhouse Email
                $statusRoute = 'clientStatusInhouse';
                $inhouseData = array('client' => $client, 'agent' => $userLoggedin, 'inhouse' => $inhouseAgentData, 'statusRoute' => $statusRoute);

                $this->mailer->newClientToInhouse($inhouseAgentData, $inhouseData);

                //Admin Email
                $statusRouteAdmin = 'clientStatusAdmin';
                $adminData = array('client' => $client, 'agent' => $userLoggedin, 'inhouse' => $inhouseAgentData, 'statusRoute' => $statusRouteAdmin);

                $this->mailer->newClientToAdmin($inhouseAgentData, $adminData);

                Activity::log([
                    'contentId'   => $client->id,
                    'contentType' => 'User',
                    'action'      => 'Add',
                    'description' => 'Added new client',
                    'details'     => 'added client '.$client->first_name.'.',
                    'updated'     => $userLoggedin->id ? true : false,
                ]);

                //Create or Update Lead Report
                $hasReport = $this->leadReport->newLeadSubmit($userLoggedin->id);

                return Redirect::to('user/dashboard')->with('success', 'Your client was successfully added.');

             }

                return Redirect::to('user/client/create')->withInput()
                    ->withErrors(['You must be assigned to an agent to submit a client. Please <a title="Contact Support" alt="Contact Support" href="/help">contact support</a>.  Error: NOA214']);

        } else {

            return Redirect::to('user/client/create')->withInput()->withErrors($validator);

        }
        
    }
    
     /**
    *  List of Clients View for Agent
    *  View List of clients submitted by an Agent ($user)
    *
    */
    public function getClientDataList($user)
    {

        $clients = $this->client->select(array(
            'clients.id', 
            'clients.business_name',
            DB::raw('CONCAT(clients.first_name," ",clients.last_name) as ClientName'),
        ))->where('clients.user_id', '=', $user);

        return Datatables::of($clients)

        ->add_column('updated', function($client) {

                    $results = $this->statusUpdates->select('updated_at')->where('client_id', '=', $client->id)->orderBy('updated_at', 'DESC')->first();

                    if ($results) {
                       
                        return Carbon::parse($results->updated_at)->format('m/d/y');
                    
                    }

                        return '00/00/00';

        })
        ->add_column('status', function ($client) {

                $currentStatus = $this->statusAction->getCurrentStatus($client->id);

                if ($currentStatus) {

                    return '<p class="tblStatus">'.$currentStatus->statusAction->getActionByName($currentStatus->action_id).'</p>';

                }

                    return '<p class="tblStatus">Queued In System</p>';


        })
        ->add_column('notes', function ($client) {return "<a style='margin-left: 5px;' href='/user/client/" . $client->id . "/status/' class='iframe btn btn-xs btn-info'><span class='glyphicon glyphicon-comment'></span> View Comments</a>";})
        ->remove_column('id')
        ->make();
    }

    /*
        Test functions real quick
    */

    /* TESTING PURPOSES remember to make a ROUTE
        public function getTest()
        {
            $userLoggedin = $this->user->currentUser();
           // echo $userLoggedin;
            echo $this->assignedAgent->grabInhouse($userLoggedin);
        }*/

}