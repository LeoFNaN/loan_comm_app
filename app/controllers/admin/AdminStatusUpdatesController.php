<?php

use MVF\Mailers\AdminMailer as Mailer;

class AdminStatusUpdatesController extends BaseController {

   /**
     * User Model
     * @var User
     */
    protected $user;

    /**
    * AssignedAgent Model
    * @var AssignedAgent
    */
    protected $assignedAgent;

    /**
    * StatusUpdate Model
    * @var StatusUpdate
    */
    protected $statusUpdate;

    /**
    * StatusUpdate Model
    * @var StatusAction
    */
    protected $statusAction;

    /**
    * Client Model
    * @var client
    */
    protected $client;

    /**
    * LeadReport Model
    * @var leadReport
    */
    protected $leadReport;

    /**
    * StatusUpdatesMailer Model
    * @var mailer
    */
    protected $mailer;

    public function __construct(User $user, AssignedAgent $assignedAgent, StatusUpdate $statusUpdate, StatusAction $statusAction, Client $client, Mailer $mailer, LeadReport $leadReport)
    {
            parent::__construct();
            $this->user = $user;
            $this->assignedAgent = $assignedAgent;
            $this->statusUpdate = $statusUpdate;
            $this->statusAction = $statusAction;
            $this->leadReport = $leadReport;
            $this->client = $client;
            $this->mailer = $mailer;
    }

    /**
     * Display a listing of the resource. Client Status/Profile View
     * GET /statusupdates - View individual Client/Lead status and info
     *
     * @return Response
     */
    public function index($clientID)
    {

            $clientObj = $this->client->with('user')->where('id', '=', $clientID)->first();

            $statusUpdates = $this->statusUpdate->getStatusUpdates($clientID);
            $currentStatus = $this->statusAction->getCurrentStatus($clientID);
            $allStages = $this->statusAction->getAllStatusActions(); //populate dropdown

            $adminLoggedin = $this->user->currentUser();

            if (isset($clientObj)) {

                    // Get current user and check permission
                    $userLoggedin = $this->user->currentUser();

                    $canComment = false;
                    if(!empty($userLoggedin)) {

                        $canComment = $userLoggedin->can('post_comment');

                    }

                return View::make('admin/client/status')
                ->with(compact('clientObj', 'statusUpdates', 'canComment', 'allStages', 'currentStatus'));

            }

            return App::abort(404);

    }

    /**
     * Show the form for creating a new resource.
     * GET /statusupdates/create
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage/Save Status Update and Email
     * Validate, Save Status Update Pass Comment ID to save
     * POST /statusupdates
     *
     * @return Response
     */
    public function store($client)
    {

        //Grab Logged In User
        $userAuthor = $this->user->currentUser();

        $canComment = $userAuthor->can('post_comment');

        $previousStatusID = $this->statusUpdate->getCurrentActionID($client);

         $statusContent = '';

            if ( ! $canComment) {

                return Redirect::to('admin/client/'.$client.'/status/')->with('error', 'You need to be logged in to post a status!');

            }

            $rules = array();
            
            if (Input::get('status_action') == '5') { //comment only

               $rules = array(
                   'status_content' => 'required|min:2'
               );

            }

           $validator = Validator::make(Input::all(), $rules);

            if ($validator->passes()) {

                $statusContent = $this->statusUpdate->setStatusContent(Input::get('status_content'), Input::get('status_action'));

                if ( ! $previousStatusID ) { //Blank then no status ever declared keep it Queued, Fail Safe

                        $currentAction = '21'; 

                } elseif (Input::get('status_action') == "5") {//if comment only then keep previous/current status

                        $currentAction = $previousStatusID;

                } else {//New status action declared

                        $currentAction = Input::get('status_action');

                }

                $agentID = $this->client->select('user_id')->where('id', '=', $client)->first();

                $newStatus = new StatusUpdate;
                $newStatus->user_id = $userAuthor->id;
                $newStatus->client_id = $client;
                $newStatus->status_content = $statusContent;
                $newStatus->action_id = $currentAction;

                $userAuthor->statuses()->save($newStatus);

                // Check Save
                if ($userAuthor->statuses()->save($newStatus)) {

                     $thisClient = $this->client->where('id', '=', $client)->first(); //client Information
                     $agentInfo = $this->user->select('id', 'email', 'first_name', 'last_name')->where('id', '=', $thisClient->user_id)->first(); //clients Agent
                     $inhouseEmail = $this->assignedAgent->grabInhouse($agentInfo);

                     if (Input::get('notify_agent') == "Yes") {

                         //Email Agent
                         $statusRoute = 'clientStatus';
                         $data = array('client' => $thisClient, 'agent' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRoute);
                         $this->mailer->newStatusUpdateToAgent($agentInfo, $data);

                    }

                     //Email Inhouse
                    $statusRoute = 'clientStatusInhouse';
                    $inhouseData = array('client' => $thisClient, 'admin' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRoute, 'refAgent' => $agentInfo);
                    $this->mailer->newStatusUpdateToInhouse($inhouseEmail, $inhouseData);

                    //Email/Notify Admins
                    $statusRouteAdmin = 'clientStatusAdmin';
                    $adminData = array('client' => $thisClient, 'admin' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRouteAdmin, 'refAgent' => $agentInfo); //Inhouse = Admin
                    $this->mailer->newStatusUpdateToAdmin($agentInfo, $adminData); //refactor agentInfo

                    $actionName = $this->statusAction->getActionByName($currentAction);

                    if (Input::get('status_action') == "5") {
                    
                        Activity::log([
                            'contentId'   => $thisClient->id,
                            'contentType' => 'Status',
                            'action'      => 'Add',
                            'description' => 'Posted a new status',
                            'details'     => 'posted update to '.$thisClient->first_name.'.',
                            'updated'     => $userAuthor->id ? true : false,
                        ]);

                     } else {
                          
                        //Log activity with status change
                        Activity::log([
                            'contentId'   => $thisClient->id, //client
                            'contentType' => 'Status',
                            'action'      => 'Add',
                            'description' => 'Posted a new status',
                            'details'     => 'updated '.$thisClient->first_name.' to '.$actionName.'.',
                            'updated'     => $userAuthor->id ? true : false,
                        ]);
                            
                    }

                    $reportResult = $this->leadReport->qualityCount($agentID->user_id, $previousStatusID, $currentAction);

                    return Redirect::to('admin/client/'.$client.'/status/')->with('success', 'Client status successfully update.');

                }

                return Redirect::to('admin/client/'.$client.'/status/')->with('error', 'There was a problem adding your status, please try again.');

        }

        return Redirect::to('admin/client/'.$client.'/status/')->withInput()->withErrors($validator);

    }

 /**
    *  List of Clients View for Admin
    *  Quick update action for current status
    *  Datatable called by admin/show.blade.php
    */
    public function getQuickStatus($clientID)
    {

        list($user,$redirect) = $this->user->checkAuthAndRedirect('admin/dashboard');
        
        if ($redirect) { 
        
            return $redirect;
        
        }

        $clientObj = $this->client->where('id', '=', $clientID)->first();

        if (!$clientObj) {
           
            return Redirect::to('admin/dashboard')->with( 'error', ('No such client'));
        
        }

        $statusUpdates = $this->statusUpdate->getStatusUpdates($clientID);
        $currentStatus = $this->statusAction->getCurrentStatus($clientID);
        $allStages = $this->statusAction->getAllStatusActions(); //populate dropdown

        $adminLoggedin = $this->user->currentUser();

        if ($user->count()) {

            $canComment = false;

            if (!empty($adminLoggedin)) {

                $canComment = $adminLoggedin->can('post_comment');
            
            }

            return View::make('admin/client/quickstatus')
            ->with(compact('clientObj', 'statusUpdates', 'canComment', 'currentStatus', 'allStages'));

        } else {

            return Redirect::to('admin/dashboard')->with( 'error', ('No such client'));

        } 

    }

    /**
     * Store: Quick Status Update
     * Validate, Save Status Update Pass Comment ID to save
     * POST /statusupdates
     *
     * @return Response
     */
    public function postQuickStatus($clientID)
    {

        $userAuthor = $this->user->currentUser();

        $canComment = $userAuthor->can('post_comment');

        $previousStatusID = $this->statusUpdate->getCurrentActionID($clientID);

        $statusContent = '';

        if ( ! $canComment) {

            return Redirect::to('admin/client/'.$clientID.'/quickstatus')->with('error', 'You need to be logged in to post a status!');

        }

        $rules = array();
        
        if (Input::get('status_action') == '5') { //comment only

               $rules = array(
                   'status_content' => 'required|min:2'
               );

        }

        $validator = Validator::make(Input::all(), $rules);

            if ($validator->passes()) {

                $statusContent = $this->statusUpdate->setStatusContent(Input::get('status_content'), Input::get('status_action'));

                if ( ! $previousStatusID ) { //Blank then no status ever declared keep it Queued, Fail Safe

                        $currentAction = '21'; 

                } elseif (Input::get('status_action') == "5") {//if comment only then keep previous/current status

                        $currentAction = $previousStatusID;

                } else {//New status action declared

                        $currentAction = Input::get('status_action');

                }

                $newStatus = new StatusUpdate;
                $newStatus->user_id = $userAuthor->id;
                $newStatus->client_id = $clientID;
                $newStatus->status_content = $statusContent;
                $newStatus->action_id =  $currentAction;

                $userAuthor->statuses()->save($newStatus);

                $agentID = $this->client->select('user_id')->where('id', '=', $clientID)->first();

                $reportResult = $this->leadReport->qualityCount($agentID->user_id, $previousStatusID, $currentAction);

                //Comment saved with success?
                if ($userAuthor->statuses()->save($newStatus)) {

                    $thisClient = $this->client->where('id', '=', $clientID)->first(); //client Info

                    $agentInfo = $this->user->select('id', 'email', 'first_name', 'last_name')->where('id', '=', $thisClient->user_id)->first(); //clients Agent

                    $inhouseEmail = $this->assignedAgent->grabInhouse($agentInfo);

                    if (Input::get('notify_agent') == "Yes") {

                        //Email to Agent
                        $statusRoute = 'clientStatus';
                        $agentData = array('client' => $thisClient, 'agent' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRoute);
                        $this->mailer->newStatusUpdateToAgent($agentInfo, $agentData);
                            
                    }

                    //Email to Inhouse
                    $statusRoute = 'clientStatusInhouse';
                    $inhouseData = array('client' => $thisClient, 'agent' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRoute, 'refAgent' => $agentInfo);
                    $this->mailer->newStatusUpdateToInhouse($inhouseEmail, $inhouseData);

                    //Email/Notify Admins (Record keeping)
                    $statusRouteAdmin = 'clientStatusAdmin';
                    $adminData = array('client' => $thisClient, 'inhouse' => $userAuthor, 'status' => $newStatus, 'statusRoute' => $statusRouteAdmin, 'agent' => $agentInfo);
                    $this->mailer->newStatusUpdateToAdmin($agentInfo, $adminData); //refactor agentInfo

                     $actionName = $this->statusAction->getActionByName($currentAction);

                    if (Input::get('status_action') == "5") {

                        //Log activity generic comment only
                        Activity::log([
                            'contentId'   => $thisClient->id, //client
                            'contentType' => 'Status',
                            'action'      => 'Add',
                            'description' => 'Posted a new status',
                            'details'     => 'updated '.$thisClient->first_name.'.',
                            'updated'     => $userAuthor->id ? true : false,
                        ]);

                    } else {

                          //Log activity with status change
                        Activity::log([
                            'contentId'   => $thisClient->id, //client
                            'contentType' => 'Status',
                            'action'      => 'Add',
                            'description' => 'Posted a new status',
                            'details'     => 'updated '.$thisClient->first_name.' to '.$actionName.'.',
                            'updated'     => $userAuthor->id ? true : false,
                        ]);
                    
                    }

                    return Redirect::to('admin/client/'.$clientID.'/quickstatus')->with('success', 'Your status was successfully added.');

                } else {

                    return Redirect::to('admin/client/'.$clientID.'/quickstatus')->with('error', 'There was a problem adding your status, please try again.');
                }

            }
            
        return Redirect::to('admin/client/'.$clientID.'/quickstatus')->withInput()->withErrors($validator);

    }

    /**
     * Display the specified resource.
     * GET /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /statusupdates/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * PUT /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /statusupdates/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}