<?php

use MVF\Mailers\AdminMailer as Mailer;

class AdminUsersController extends AdminController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    /**
    * AssignedAgent Model
    * @var $assignedAgent
    */
    protected $assignedAgent;

    /**
    * StatusUpdatesMailer Model
    * @var mailer
    */
    protected $mailer;

    /**
    * Activity Class Model
    * @var $activityLog
    */
    protected $activityLog;

    /**
    * Lead Report DI
    * @var $leadReport
    */
    protected $leadReport;

/**
    * StatusAction Class Model
    * @var $statusAction
    */
    protected $statusAction;

    /**
    * Client Class Model
    * @var $client
    */
    protected $client;

    /**
    * StatusUpdate Model
    * @var $statusUpdates
    */
    protected $statusUpdates;

    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission, AssignedAgent $assignedAgent, Mailer $mailer, Activity $activityLog, StatusAction $statusAction, Client $client, StatusUpdate $statusUpdates, LeadReport $leadReport)
    {

        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
        $this->assignedAgent = $assignedAgent;
        $this->mailer = $mailer;
        $this->statusAction = $statusAction;
        $this->statusUpdates = $statusUpdates;
        $this->activityLog = $activityLog;
        $this->leadReport = $leadReport;
        $this->client = $client;

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {

        // Grab all the users
        $users = $this->user;

        $userLoggedin = $this->user->currentUser();

        $paginator = $this->user->getRecentActivityTimeFrameAdmin(21, 5); //All Clients Recent Activity

        // Show the page
        return View::make('admin/dashboard', compact('userLoggedin', 'users', 'paginator'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {

        $roles = $this->role->all();

        // Get all the available permissions
        $permissions = $this->permission->all();

        $selectedRoles = Input::old('roles', array());

        $selectedPermissions = Input::old('permissions', array());

        $inhouseAgents = DB::table('users')->join('assigned_roles', function($assigned)
        {
            $assigned->on('users.id', '=', 'assigned_roles.user_id')->where('assigned_roles.role_id', '=', 3);
        })->get(); 

    	$title = Lang::get('admin/users/title.create_a_new_user');

    	$mode = 'create';

    	return View::make('admin/users/create', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'inhouseAgents', 'mode'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    { 

        $this->user->office_id = $this->user->mtrandstr(7, '0123456789ABCDEF');
        $this->user->first_name = Input::get( 'first_name' );
        $this->user->last_name = Input::get( 'last_name' );
        $this->user->phone_number = Input::get( 'phone_number' );
        $this->user->email = Input::get( 'email' );
        $this->user->password = Input::get( 'password' );
        $this->user->password_confirmation = Input::get( 'password_confirmation' );
        $this->user->confirmed = Input::get( 'confirm' ); //Agent activation

        if ($this->user->save()) {

            $this->user->saveRoles(Input::get( 'roles' ));

            $newUser = $this->user->where('id', '=', $this->user->id)->first();

            $reportResult = $this->leadReport->createLeadReportRecord($newUser->id);

            return Redirect::to('admin/dashboard/')->with('success', Lang::get('admin/users/messages.create.success'));

        } else {

            $error = $this->user->errors()->all();

            return Redirect::to('admin/users/create')
                ->withInput(Input::except('password'))
                ->with( 'error', $error );

        }

    }

    /**
     * Display the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getShow($user)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    protected function getEdit($user)
    {

        if ( $user->id ) {

            $roles = $this->role->all();

            $permissions = $this->permission->all();

            //Grab users with inhouse role (id = 3)
            $inhouseAgents = DB::table('users')
                ->select('users.id', DB::raw('CONCAT(users.first_name, " ", users.last_name) as fullName'))
                ->join('assigned_roles', function($assigned){
                    $assigned->on('users.id', '=', 'assigned_roles.user_id')->where('assigned_roles.role_id', '=', 3);
                })
                ->get(); 

            //If inhouse hide Agent Assignment Tab
            if($user->hasRole('Inhouse') || $user->hasRole('admin')) {

                 $assignedTo = "inhouse";

            } else {

                //Check if user belongs to inhouse already.
                $assignedAgents = $this->assignedAgent->select('agent_id')->where('user_id', '=', $user->id)->first();

                if(count($assignedAgents) == "1") {

                    $assignedTo = $this->user->select(DB::raw('CONCAT(first_name, " ", last_name) as fullName'))->where('id', '=', $assignedAgents->agent_id)->first(); //grab Agents Name

                } else {

                    $assignedTo = null;

                }
                
            }

            $title = Lang::get('admin/users/title.user_update');

            $mode = 'edit';

            $leadReport = $this->leadReport->getLeadReport($user->id);

        	return View::make('admin/users/create_edit', compact('user', 'roles', 'permissions', 'title', 'mode', 'inhouseAgents', 'assignedTo', 'leadReport'));
        
        } else {

            return Redirect::to('admin/dashboard')->with('error', Lang::get('admin/users/messages.does_not_exist'));
        
        }

    }

    /**
     * Update the specified resource in storage.
     * @param $user
     * @return Response
     */
    protected function postEdit($user)
    {

         //Check and grab Assigned inhouse Id
        $assignedUpdate = $this->assignedAgent->where('user_id', '=', $user->id)->first();

        $oldUser = clone $user;

        $user->first_name = Input::get( 'first_name' );
        $user->last_name = Input::get( 'last_name' );
        $user->email = Input::get( 'email' );
        $user->phone_number = Input::get( 'phone_number' );
        $user->confirmed = Input::get( 'confirm' );
        $password = Input::get( 'password' );
        $passwordConfirmation = Input::get( 'password_confirmation' );

        if (!empty($password)) {
        
            if ($password != $passwordConfirmation) {
        
                return Redirect::to('admin/users/' . $user->id . '/edit')
                    ->with('error', Lang::get('admin/users/messages.password_does_not_match'));
        
            } else {
        
                $user->password = $password;
                $user->password_confirmation = $passwordConfirmation;
        
            }
        
        }

        if($user->hasRole('Inhouse') || Input::get('assignment') == "Select" || $user->hasRole('admin')) {

                //Skip if Agent being saved is an Inhouse, or no inhouse agent is selected

        } else {

                if (count($assignedUpdate) == "1") { //Update records. 1 = true
                    
                    $assignedUpdate->user_id = $oldUser->id;
                    $assignedUpdate->agent_id = Input::get('assignment');
                
                } else { //Create a new record in assigned_agents
                    
                    if (Input::get('confirm') == "0") { //Check if activated first
                    
                        return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', 'User must be activated first.');
                    
                    } else { //create assignment
                    
                        $this->assignedAgent->user_id = $oldUser->id;
                        $this->assignedAgent->agent_id = Input::get('assignment');
                    
                    }
                }
        }
        
        if ($user->confirmed == null) {

            $user->confirmed = $oldUser->confirmed;

        }

       //Assignment Save/Emails
        if ($user->hasRole('Inhouse')  || Input::get('assignment') == "Select"  || $user->hasRole('admin')) {

            //Skip if Agent being saved is an Inhouse or nothing was selected from dropdown.

        } else {

            if (count($assignedUpdate) == "1") { //if Assignedalready, then update records only. 1 = true

                $assignedUpdate->save();

                $assignedAgentInfo = $user->where('id', '=', $assignedUpdate->agent_id)->first();

                //Email to Agent Notifying them of new assignment
                $assignedAgentData = array('inhouse' => $assignedAgentInfo, 'agent' => $user);
                $this->mailer->agentAssignmentTo($user, $assignedAgentData);

                 //Email to Inhouse notifying them of new assignment
                $inhouseData = array('inhouse' => $assignedAgentInfo, 'agent' => $user);
                $this->mailer->inhouseAssignmentTo($assignedAgentInfo, $inhouseData);

            } else { //If not previously assigned, then insert a new row.

                $this->assignedAgent->save();

                $assignedAgentInfo = $user->where('id', '=', $this->assignedAgent->agent_id)->first();

                //Notify agent of new assignenment
                $assignedAgentData = array('inhouse' => $assignedAgentInfo, 'agent' => $user);
                $this->mailer->agentAssignmentTo($user, $assignedAgentData);

                 //Notify Inhouse and Admin of new assignment
                $inhouseData = array('inhouse' => $assignedAgentInfo, 'agent' => $user);
                $this->mailer->inhouseAssignmentTo($assignedAgentInfo, $inhouseData);

            }

        }

        //Activation Email
        //If the new Confirmed is Yes and previous was No
        if ($user->confirmed == '1' && $oldUser->confirmed == '0') {
                $data = array('agent' => $user);
                $this->mailer->agentActivation($user, $data);
        }

        // Save roles. Handles updating.
        $user->saveRoles(Input::get( 'roles' ));

        if ($user->save()) {
            return Redirect::to('admin/users/' . $user->id . '/edit')
                ->with('success', Lang::get('admin/users/messages.edit.success'));
        } else {
            $error = $user->errors()->all(':message');
            return Redirect::to('admin/users/' . $user->id . '/edit')
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', $error);
        }

}

    /**
     * Remove user page.
     *
     * @param $user
     * @return Response
     */
    protected function getDelete($user)
    {
        // Title
        $title = Lang::get('admin/users/title.user_delete');

        // Show the page
        return View::make('admin/users/delete', compact('user', 'title'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return Response
     */
    protected function postDelete($user)
    {
        // Check if we are not trying to delete ourselves
        if ($user->id === Confide::user()->id) {

            // Redirect to the user management page
            return Redirect::to('admin/dashboard')->with('error', Lang::get('admin/users/messages.delete.impossible'));

        }

        AssignedRoles::where('user_id', $user->id)->delete();

        $id = $user->id;
        $user->delete();

        // Was the comment post deleted?
        $user = User::find($id);

        if ( empty($user) ) {

            // TODO needs to delete all of that user's content
            return Redirect::to('admin/dashboard')->with('success', Lang::get('admin/users/messages.delete.success'));

        } else {
    
            // There was a problem deleting the user
            return Redirect::to('admin/dashboard')->with('error', Lang::get('admin/users/messages.delete.error'));
    
        }
    
    }

    /**
     * Show a list of all the users formatted for Datatables.
     * {{{ Carbon::parse(\$created_at)->format('m-d-y') }}}
     * @return Datatables JSON
     */
    public function getData()
    {

        //$clientCount = Client::where('user_id', '=', '2')->get();
        $users = User::leftjoin('assigned_roles', 'assigned_roles.user_id', '=', 'users.id')
                    ->leftjoin('roles', 'roles.id', '=', 'assigned_roles.role_id')
                    ->select(array(
                            'users.id', 
                            'users.office_id',
                            DB::raw('CONCAT(users.first_name," ",users.last_name) as userName'),
                            'users.email', 
                            'roles.name as rolename', 
                            'users.confirmed', 
                            'users.created_at'
                        ));

        return Datatables::of($users)
        ->edit_column('confirmed',
                    '@if($confirmed)
                            Yes
                    @else
                            No
                    @endif'
            )

        ->edit_column('created_at', "{{{ Carbon::parse(\$created_at)->format('m/d/y') }}}")

        ->add_column('actions', 
                '<a title="Edit Agent" alt="Edit Agent" href="{{{ URL::to(\'admin/users/\' . $id . \'/edit\' ) }}}" ><span class="icon icon-pencil"></span></a>
                            @if($rolename == \'admin\' || $rolename == \'Inhouse\')

                            @else 
                                <a title="View Clients" alt="View Clients" style="clear: both;" href="{{{ URL::to(\'admin/client/\' . $id . \'/show/\') }}}" >
                                    <span class="icon icon-users grayIcon"></span>
                                </a>    

                                <a title="Delete Agent" alt="Delete Agent" style="margin-top: 10px;" href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" ><span class="icon icon-remove redIcon"></span>
                                </a>

                            @endif
            ')
        ->remove_column('id')
        ->make();
    }


  /**
    *  List of Clients View for Admin
    *  View List of clients submitted by an Agent ($userAgent)
    *
    */
    public function getShowList($userAgent)
    {

        $userLoggedin = $this->user->currentUser();

        $title = 'Clients Submitted by '.$userAgent->first_name.' '.$userAgent->last_name;

        $assignedAgents = $this->assignedAgent->where('user_id', '=', $userAgent->id)->first();

        $listofClientsIds = $this->client->getClientsIDByAgent($userAgent->id);
        $paginator = $this->user->getRecentActivityTimeFrame($listofClientsIds, 90, 5);
        $allStages = $this->statusAction->getAllStatusActions(); //populate dropdown

        if (count($assignedAgents)) { //Agent is assigned

            $agent = $this->user->where('id', '=', $assignedAgents->agent_id)->first();

        } else { //no agent found

            $agent = '';

        }

        if ($userAgent->clients->count()) {

            $noClients = 'False';

        } else {

            $noClients = 'True';

        }

        return View::make('admin/client/show')->with(compact('noClients', 'agent', 'userAgent', 'title', 'paginator', 'userLoggedin', 'allStages'));
    
    }

  /**
    *  List of Clients View for Admin
    *  View List of clients submitted by an Agent ($user)
    *  Datatables - admin.clients.show
    */
    public function getListData($user)
    {

        $clients = Client::select(array(
            'clients.id', 
            DB::raw('CONCAT(clients.first_name," ",clients.last_name) as ClientName'),
            'clients.email',
            'clients.created_at'
        ))
        ->where('clients.user_id', '=', $user);

        return Datatables::of($clients)
        ->edit_column('created_at', "{{{ Carbon::parse(\$created_at)->format('m/d/y') }}}")
        ->add_column('updated', function($client){

                    $results = $this->statusUpdates->select('updated_at')->where('status_updates.client_id', '=', $client->id)->orderBy('status_updates.updated_at', 'DESC')->first();

                    if ($results) {
                        return Carbon::parse($results->updated_at)->format('m/d/y');
                    } else {
                        return '00/00/00';
                    }
            
                })
        ->add_column('status', function ($client) {

                $currentStatus = $this->statusAction->getCurrentStatus($client->id);

                if ($currentStatus) {

                    return $currentStatus->statusAction->getActionByName($currentStatus->action_id);

                } else {

                    return 'Queued In System';

                }

        })
        ->add_column('actions', function ($client) { return 

            "<a title='View Profile' alt='View Profile' style='margin-left: 2px;' href='/admin/client/" . $client->id . "/status/'  class='btn btn-xs btn-info'><span class='glyphicon glyphicon-comment'></span></a>

            <a title='Quick Update' style='margin-left: 3px;' href='/admin/client/" . $client->id . "/quickstatus' class='iframeQuickStatus btn btn-xs btn-info'><span class='glyphicon glyphicon-plus-sign'></span> </a> 

            ";

            ;})
        ->remove_column('id')
        ->make(); 

    }

/**
     * Show a list of all the users formatted for Datatables.
     * {{{ Carbon::parse(\$created_at)->format('m-d-y') }}}
     * @return Datatables JSON
     */
    public function getTest()
    {
        //Route::get('/test', 'AdminUsersController@getTest'); ADD THIS TO ROUTES.PHP
    }

}
