@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
    Agent Profile - MVF Agent Center
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
    <h3><span class="glyphicon glyphicon-user"></span> Agent Profile</h3>
</div>

<div class="col-xs-12 col-sm-4 col-md-4">
    
    <div class="profileImg">
        <img src="" width="200px" height="200px" /><span class="glyphicon glyphicon-download-alt"></span>
    </div>

</div>

<div class="col-xs-12 col-sm-8 col-md-8">

        <div class="col-md-12 bottomMargin10">
            <p class="profileLabel">Name:  </p>
            <p class="profileText">{{{ $agent->first_name.' '.$agent->last_name }}}</p>
        </div>


        <div class="col-md-12 bottomMargin10">
            <p class="profileLabel">Email: </p>
            <p class="profileText">{{ HTML::mailto($agent->email, $agent->email) }}</p>
        </div>


        <div class="col-md-12 bottomMargin10">
           <p class="profileLabel">Phone: </p>
           <p class="profileText"> {{{ $agent->phone_number }}}</p>
        </div>

        <div class="col-md-6" style="margin-top: 10px;">
            {{ HTML::linkRoute('showListOfClients', 'Manage Clients', array($agent->id), array('class' => 'btn btn-default')) }}
        </div>

</div>

@stop

@section('sidebar')

    <div class="innerSidebar">
        
        <h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Lead Report</h4>

        <div class="col-sm-6" style="text-align: center;">
            <p class="leadCount">{{{ $leadReport->gross_count }}}</p>
            <p class="leadLabel">Total Leads</p>
        </div>

        <div class="col-sm-6" style="text-align: center; ">
            <p class="leadCount">{{{ $leadReport->active_count }}}</p>
            <p class="leadLabel">Active Leads</p>
        </div>

        <div style="clear: both; "></div>

        <div class="" style="text-align: center; margin-top: 30px;">
            <p class="leadCount">{{{ $leadReport->process_rate }}}%</p>
            <p class="leadLabel">Process Rate</p>
        </div>

        <div style="text-align: center; margin-top: 30px;">
            <p class="leadCount">{{{ $leadReport->funded_rate }}}%</p>
            <p class="leadLabel">Funded Rate</p>
        </div>

        <div style="text-align: center; margin-top: 30px;">
            <p class="leadCount">{{{ $leadReport->failure_rate }}}%</p>
            <p class="leadLabel">Failure Rate</p>
        </div>

    </div>
@stop

@section('beforeFooter')
          <div class="col-md-12">
               <a class="btn btn-default" href="{{{ URL::to('inhouse/agents') }}}">Back</a>
        </div>
@stop
