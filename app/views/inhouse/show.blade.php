@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
    Clients submitted by {{ $agentAssigned->office_id }}
@stop

@section('pageTitle')
  <span class="glyphicon glyphicon-list"></span> Clients from {{ $agentAssigned->first_name.' '.$agentAssigned->last_name }}
@stop

{{-- Content --}}
@section('content')

    <div class="filterLabel">Filter</div>

      <div class="filtersBox">

        {{ Form::select('status_action', array('All' => 'All Statuses', 'Queued In System' => 'Queued In System') + $allStages, Input::old('status_action', (isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->id : '21')), ['id' => 'statusSelect']) }}

        <button id="clearFilter" class="pull-right btn btn-default btn-sm">Clear Filter</button>

  </div>

   <table id="clients" class="table table-striped table-hover">
    <thead>
      <tr>
          <th class="col-md-2">Name</th>
          <th class="col-md-2">Email</th>
          <th class="col-md-2">Added</th>
          <th class="col-md-2">Updated</th>
          <th class="col-md-2">Current Status</th>
          <th class="col-md-2">Actions</th>
      </tr>
    </thead>
      <tbody>
      </tbody>
  </table>
   
@stop

@section('sidebar')

  <div class="innerSidebar">

      <h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Recent Activity <span class="statusUpdateText"> Last 3 Months</span></h4>
      <ul class="recentActivityUl">

        @forelse($results as $recent)
          <li class="animated fadeIn">

             <a title="" href={{ URL::to('inhouse/client/'.$recent->content_id.'/status') }}>

              <p class="activityDetails"> 

                  @if ($recent->user_id == $inhouseLoggedin->id)

                    You {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

                  @else

                    Agent {{ $recent->first_name }} {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

                  @endif

              </p>

             </a>

             <div style="clear: both;"></div>

          </li>

        @empty

          <li>No activity</li>

        @endforelse

        {{ $results->links('pagination::slider-3') }}

      </ul>
    
    </div>

      <div style="clear: both;"></div>

	<div class="innerSidebar">

		<h4 class="sidebarH4"> <span class="icon icon-user"></span> Agent Information</h4>

		<div class="dashSideAgent" >

			<p><span>Name: </span>{{ $agentAssigned->first_name.' '.$agentAssigned->last_name}}</p>

			<p><span>ID: </span>{{ $agentAssigned->office_id  }}

			<p><span>Phone: </span> {{ $agentAssigned->phone_number }} </p>

			<p><span>Email: </span> {{ HTML::mailto($agentAssigned->email, $agentAssigned->email) }} </p> 

		</div>

	</div>

	<div class="innerSidebar">
		
		<h4 class="sidebarH4"><span class="glyphicon glyphicon-import"></span> Account Actions</h4>

		<p class="text-center"><a href="{{{ route('showAgents') }}}" alt="View Agents" class="btn btn-sm btn-success btnBig">View Agents</a></p>
	
	</div>

@stop

{{-- Scripts --}}
@section('scripts')

	<script>
        	var action = "{{ URL::action('InhouseController@getClientDataList', $agentAssigned->id) }}";
	</script>

	{{ HTML::script('assets/js/dtables/dtable.listclients.js') }}
  
@stop