@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
	Agent Management - Inhouse Dashboard
@stop

@section('pageTitle')
	<span class="glyphicon glyphicon-user"></span> Agent Management
@stop

{{-- Content --}}
@section('content')

	<p>Below are the agents assigned to you.  To view clients submitted by an agent click on the <span class='icon icon-users'></span> clients icon in the Actions column.  If you need to delete or edit an agent email {{ HTML::mailto('support@mvfloans.com?subject=MVF Delete/Edit Agent&body=Client%20Name:%0D%0AAgent%20Name:', 'support@mvfloans.com') }}.</p>
	
	<table id="users" class="table table-striped table-hover order-column compact">
		<thead>
			<tr>
				<th class="col-md-2">Agent ID</th>
				<th class="col-md-2">Agent Name</th>
				<th class="col-md-2">{{{ Lang::get('admin/users/table.email') }}}</th>
				<th class="col-md-2">Phone Number</th>
				<th class="col-md-2">Updated</th>
				<th class="col-md-2">Actions</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>


@stop

@section('sidebar')

	<div class="innerSidebar">

		<h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Recent Activity <span class="statusUpdateText"> Last 14 Days</span></h4>

		<ul class="recentActivityUl">

			@forelse($paginator as $recent)
			
				<li class="animated fadeIn">

					 <a title="" href={{ URL::to('inhouse/client/'.$recent->content_id.'/status') }}>

					 	<p class="activityDetails"> 

					 			@if ($recent->user_id == $userLoggedin->id)

						 			You {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@else

						 			{{ $recent->first_name }} {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@endif

					 	</p>

					 </a>
					 <div style="clear: both;"></div>
				</li>

			@empty

				<li>No activity</li>

			@endforelse

			{{ $paginator->links('pagination::slider-3') }}

		</ul> 

	</div>

	<div style="clear: both;"></div>

	<div class="innerSidebar">
		<h4 class="sidebarH4"><span class="glyphicon glyphicon-import"></span> Account Actions</h4>
		<p class="text-center"><a href="{{{ route('inhouseDashboard') }}}" alt="View Agents" class="btn btn-sm btn-success btnBig">View Clients</a></p>
	</div>
@stop

{{-- Scripts --}}
@section('scripts')

	  <script type="text/javascript">

		    var oTable;
		    $(document).ready(function() {
		    	oTable = $('#users').dataTable( {
		        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
		        "sPaginationType": "bootstrap",
		        "oLanguage": {
		          "sLengthMenu": "_MENU_ records per page",
						"sProcessing": "<img src='/assets/img/loader.gif'> processing..."
		        },
		        "aaSorting": [[4, 'desc']],
		        "bProcessing": true,
		        "bServerSide": false,
		        "sAjaxSource": "{{ URL::action('InhouseController@getListData') }}", 
		      });
		    });
		    
	  </script>


@stop