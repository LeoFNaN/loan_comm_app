@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
	Agent Management - Inhouse Dashboard
@stop

@section('pageTitle')
	<span class="icon icon-dashboard"></span> Dashboard
@stop

{{-- Content --}}
@section('content')

	<p>Below are the clients submitted by your agents.  To view view your agents click <a href="{{{ route('showAgents') }}}" alt="View Agents">View Your Agents</a>.  If you need to delete or edit a client please email {{ HTML::mailto('support@mvfloans.com?subject=MVF Delete/Edit Agent&body=Client%20Name:%0D%0AAgent%20Name:', 'support@mvfloans.com') }}.</p>

	<h3 class="mainTitle"><span class="glyphicon glyphicon-user"></span> Client Management</h3>

	    <div class="filterLabel">Filter</div>

	      <div class="filtersBox">

	        {{ Form::select('status_action', array('All' => 'All Statuses', 'Queued In System' => 'Queued In System') + $allStages, Input::old('status_action', (isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->id : '21')), ['id' => 'statusSelect']) }}

	        <button id="clearFilter" class="pull-right btn btn-default btn-sm">Clear Filter</button>

	  </div>

	 <table id="clients" class="table table-striped table-hover order-column compact">
	  <thead>
	    <tr>
	      <th class="col-md-2">Client Name</th>
	      <th class="col-md-2">Email</th>
	      	<th class="col-md-2">Agent Name</th>
	      <th class="col-md-2">Added</th>
	      <th class="col-md-2">Updated</th>
	      <th class="col-md-2">Current Status</th>
	      <th class="col-md-2">Actions</th>
	    </tr>
	  </thead>
	    <tbody>
	    </tbody>
	</table>

@stop

@section('sidebar')

	<div class="innerSidebar">
		
		<h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Recent Activity <span class="statusUpdateText"> Last 14 Days</span></h4>

		<ul class="recentActivityUl">

			@forelse($paginator as $recent)
			
				<li class="animated fadeIn">

					 <a title="" href={{ URL::to('inhouse/client/'.$recent->content_id.'/status') }}>

					 	<p class="activityDetails"> 

					 			@if ($recent->user_id == $userLoggedin->id)

						 			You {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@else

						 			{{ $recent->first_name }} {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@endif

					 	</p>

					 </a>
					 <div style="clear: both;"></div>
				</li>

			@empty

				<li>No activity</li>

			@endforelse

			{{ $paginator->links('pagination::slider-3') }}

		</ul> 

	</div>

	<div style="clear: both;"></div>

	<div class="innerSidebar">
		<h4 class="sidebarH4"><span class="glyphicon glyphicon-import"></span> Account Actions</h4>
		<p class="text-center"><a href="{{{ route('showAgents') }}}" alt="View Agents" class="btn btn-sm btn-success btnBig">View Agents</a></p>
	</div>

@stop

{{-- Scripts --}}
@section('scripts')

	<script>
        	var action = "{{ URL::action('InhouseController@getClientDataListAll', $userLoggedin->id) }}";
	</script>

	{{ HTML::script('assets/js/dtables/dtable.listclientsdash.js') }}

@stop