@extends('site.layouts.dashfull')

{{-- Web site Title --}}
@section('title')
	Client Tracking - Status Updates
@stop

@section('pageTitle')
	<span class="glyphicon glyphicon-user"></span> Client Profile <span class="topCurrentStatus pull-right label">Status: {{{ isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->name : 'Queued In System' }}} </span>
@stop

{{-- Content --}}
@section('content')


<div class="col-sm-12 clientProfile">		
	<h5><span class="icon icon-user"></span> Agent Information</h5>

		<div class="text-center">
			<p><a href="/inhouse/agent/{{$clientObj->user->id}}/profile">{{ $clientObj->user->first_name.' '.$clientObj->user->last_name }}</a> ({{ $clientObj->user->office_id }}) | {{ $clientObj->user->phone_number }} | {{  HTML::mailto($clientObj->user->email,  $clientObj->user->email) }} </p>
		</div>
</div>

	<div class="col-sm-4 clientProfile">

		<h5><span class="glyphicon glyphicon-th-list"></span> Client Details</h5>

			<ul class="clientDetailList">

				<li>
					<p>Name:</p><p> {{ $clientObj->first_name.' '.$clientObj->last_name }}</p>
				</li>

				<li>
					<p>Phone:</p><p> {{ $clientObj->phone }} </p>
				</li>

				<li>
					<p>Email:</p><p>
					@if($clientObj->email == "") Not Available @else {{ HTML::mailto($clientObj->email . '?subject=Hello ' . $clientObj->first_name, $clientObj->email) }} @endif</p>
				</li>

				<li>
					<p>Business Name: </p><p>
					@if($clientObj->business_name == "") N/A @else {{ $clientObj->business_name }} @endif</p>
				</li>

				<li>
					<p>Loan Amount: </p><p>
					@if($clientObj->loan_amount == "") N/A @else {{ $clientObj->loan_amount }} @endif</p>
				</li>

				<li>
					<p>State: </p><p>
					<p>@if($clientObj->state == "" || $clientObj->state == "Select One") N/A @else {{ $clientObj->state }} @endif</p>
				</li>

				<li>
					<p>Years In Business: </p><p>
					<p>@if($clientObj->business_years == "") N/A @else {{ $clientObj->business_years }} @endif</p>
				</li>

				<li>
					<p>Monthly Revenue: </p><p>
					<p>@if($clientObj->monthly_revenue == "") N/A @else {{ $clientObj->monthly_revenue }} @endif</p>
				</li>

				<li>
					<p>Contact Time: </p><p>
					<p>@if($clientObj->callback_time == "" || $clientObj->callback_time == "Select One") N/A @else {{ $clientObj->callback_time }} @endif</p>
				</li>

				<li>
					<p>Notes/Instructions:</p> 
					<p>@if($clientObj->client_message == "") N/A @else {{ $clientObj->client_message }} @endif</p>
				</li> 

			</ul>
			
	</div>

	<div class="col-sm-8 statusUpdates">

		<h5><span class="glyphicon glyphicon-comment"></span> 
			@if($statusUpdates->count()) 
				Status Comments <span class="statusCount">{{ $statusUpdates->getTotal() }}</span> 
			@else 
				Status Comments <span class="statusCount">0</span>  
			@endif 
			<span class="statusUpdateText"> Recent Comment First</span>
		</h5>

		<ol class="statusList">

			@if($statusUpdates->count())

				<?php $evenOdd = 0; ?>
				@foreach($statusUpdates as $status)

					<li>
						
						<p class="status @if($evenOdd == 0) animated flash firstStatus @endif @if($evenOdd % 2 == 0) evenRow @else oddRow @endif">
							{{ $status->status_content }}
						</p>

						<p class="statusMeta @if($evenOdd == 0) animated flash firstStatus @endif  @if($evenOdd % 2 == 0) evenMeta @else oddMeta @endif">
							<span class="glyphicon glyphicon-comment"></span> <span class="statusMetaSpan">By:</span> {{ $status->first_name.' '.$status->last_name }} | <span class="statusMetaSpan">Date:</span> {{{ Carbon::parse($status->created_at)->format('m-d-y') }}}<span class="statusMetaSpan"> @ </span> {{{ Carbon::parse($status->created_at)->format('h:i a') }}} | <span class="commentsCurrentStatus">Status: {{{ isset($status->name) ? $status->name : 'Queued In System' }}} </span>
						</p>
						
					</li>

					<?php $evenOdd++; ?>

				@endforeach

			@else 

				No Comments

			@endif

			{{ $statusUpdates->links() }}
			<div class="clearfix "></div>

		</ol>

	@if ( ! Auth::check())

		Login <a href="{{{ URL::to('user/login') }}}">here</a to add a comment.<br /><br />
			
	@elseif ( ! $canComment )

		You don't have the correct permissions to add comments.

	@else

		<div class="statusActions" style="margin: 10px 0;">

			<h5><span class="glyphicon glyphicon-time"></span> Set Status</h5>
			
			{{ Form::open(array('url' => array('inhouse/client/'.$clientObj->id.'/status'))) }}

			<div class="form-group">

					<div class="actionList">
						{{ Form::select('status_action', array('select' => 'Select One', '21' => 'Queued In System', '5' => 'Comment Only') + $allStages, Input::old('status_action', (isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->id : '21')), ['class' => 'form-control', 'id' => 'sel1']) }}
					</div>

					<div class="pull-right noMargin">
						{{ Form::checkbox('notify_agent', 'Yes', true) }} Notify agent of status update/comment
					</div>

			</div>

		<div class="clearfix"></div>

		<h5 style="padding-top: 10px;"><span class="icon icon-pencil"></span> Add Comment</h5>

			@if($errors->has())
				<div class="alert alert-danger alert-block">
					<ul>
			@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
			@endforeach
					</ul>
				</div>
			@endif

		<textarea class="input-block-level" rows="4" style="width: 100%; padding: 5px;" name="status_content" id="status_content">{{{Request::old('status_content') }}}</textarea>

			<div class="form-group">

				<div class="col-md-12" style="margin: 10px 0 0 0; padding: 0;">

					<input type="submit" class="btn btn-success" id="submit" value="Submit Update" />

					<div class="pull-right">

			               	<a class="btn btn-default" href="{{{ URL::route('inhouseDashboard' ) }}}">Back To Dashboard</a>

			          	</div>

				</div>

			</div>

		</form>
		
	@endif

	</div>

@stop