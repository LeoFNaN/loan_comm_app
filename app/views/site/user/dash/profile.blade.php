@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
    Update Your Profile - MVF Agent Center
@stop

{{-- Content --}}
@section('content')
<div class="page-header">
    <h3><span class="glyphicon glyphicon-cog"></span> Update Your Profile</h3>
</div>

<form class="form-horizontal" method="post" action="{{ URL::to('user/' . $user->id . '/edit') }}"  autocomplete="off">

   {{ Form::token(); }}

    <!-- General tab -->
    <div class="tab-pane active" id="tab-general">

        <div class="form-group {{{ $errors->has('first_name') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="first_name">First Name</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="first_name" id="first_name" value="{{{ Input::old('first_name', $user->first_name) }}}" />
                {{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('last_name') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="last_name">Last Name</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="last_name" id="last_name" value="{{{ Input::old('last_name', $user->last_name) }}}" />
                {{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="email">Email</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="email" id="email" value="{{{ Input::old('email', $user->email) }}}" />
                {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('phone_number') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="phone_number">Phone </label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="phone_number" id="phone_number" value="{{{ Input::old('phone_number', $user->phone_number) }}}" />
                {{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="password">New Password</label>
            <div class="col-md-10">
                <input class="form-control" type="password" name="password" id="password" value="" />
                {{ $errors->first('password', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="password_confirmation">Password Confirm</label>
            <div class="col-md-10">
                <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="" />
                {{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

    </div>


    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn btn-success">Update</button>
            @if ($user->hasRole('admin'))
                <a class="btn btn-default" href="{{{ URL::to('admin/dashboard') }}}">Back</a>
            @elseif ($user->hasRole('Inhouse')) 
               <a class="btn btn-default" href="{{{ URL::to('inhouse/dashboard') }}}">Back</a>
            @else
                <a class="btn btn-default" href="{{{ URL::to('user/dashboard') }}}">Back</a>
            @endif    

        </div>
    </div>

</form>
@stop
