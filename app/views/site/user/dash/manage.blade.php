@extends('site.layouts.dashfull')

{{-- Web site Title --}}
@section('title')
	Agent Management - Inhouse Dashboard
@stop

@section('pageTitle')
	<span class="glyphicon glyphicon-user"></span> Agent Management
@stop

{{-- Content --}}
@section('content')

	<p>Below are the agents that are assigned to you.  To view clients under a specific agent click on View Clients button.</p>
	<p>If you need to delete or edit a client please email {{ HTML::mailto('support@multivisionfinancial.com?subject=MVF Delete/Edit Agent', 'support@multivisionfinancial.com') }}.</p>

	<h3 class="mainTitle">List of Agents</h3>
	<table id="users" class="table table-striped table-hover order-column compact">
		<thead>
			<tr>
				<th class="col-md-2">Agent ID</th>
				<th class="col-md-2">Name</th>
				<th class="col-md-2">{{{ Lang::get('admin/users/table.email') }}}</th>
				<th class="col-md-2">Phone Number</th>
				<th class="col-md-2">{{{ Lang::get('admin/users/table.created_at') }}}</th>
				<th class="col-md-2">Actions</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

{{-- Scripts --}}
@section('scripts')
  <script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable = $('#users').dataTable( {
        "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
          "sLengthMenu": "_MENU_ records per page",
	   "sProcessing": "<img src='/assets/img/loader.gif'> processing..."
        },
        "aaSorting": [[4, 'desc']],
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "{{ URL::action('UserController@getListData', $userLoggedin->id) }}", 
      });
    });
  </script>
@stop