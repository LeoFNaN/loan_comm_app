@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
	Marketing Resources
@stop

{{-- Content --}}
@section('content')
<p>Click on button to download file in PDF format.</p>

<ul class="resourceUL">
	<li>
		<a href="{{ URL::asset('assets/pdf/ref_agent_training_manual.pdf') }}">
			<p class="btn btn-info" style="width: 180px; text-align: left;"><span class="glyphicon glyphicon-download"></span> Training Manual </p>
		</a>
	</li>

</ul>

<h3 class="sidebarH4" style="clear: both;">Referral Program Files</h3>

<ul class="resourceUL">

	<li><a href="{{ URL::asset('assets/pdf/ref_agent_role.pdf') }}"><p class="btn btn-info" style="width: 180px; text-align: left;"><span class="glyphicon glyphicon-download"></span> Agent Role </p></a></li>

	<li><a href="{{ URL::asset('assets/pdf/ref_agent_opportunity.pdf') }}"><p class="btn btn-info" style="width: 180px; text-align: left;"><span class="glyphicon glyphicon-download"></span> Opportunity </p></a></li>
	
	<li><a href="{{ URL::asset('assets/pdf/ref_w9_form.pdf') }}" ><p class="btn btn-info" style="width: 180px; text-align: left;"><span class="glyphicon glyphicon-download"></span> W9 Form </p> </a></li>

</ul>

@stop

@section('sidebar')
	<h4 class="sidebarH4">Quick Links: </h4>
	<p class="text-center"><a title="Submit New Client" href="{{{ URL::to('user/client/create/') }}}" class="btn btn-sm btn-success btnBig">Click to Submit New Client</a></p>
@stop