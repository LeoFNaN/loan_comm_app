@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
	Your Dashboard - MVF Agent Center
@stop

@section('pageTitle')
	<span class="icon icon-dashboard"></span> DASHBOARD
@stop

{{-- Content --}}
@section('content')

	@if ($user->confirmed == '1' && Auth::user()->hasRole('Agent'))

		@if ($user->clients == "[]" || $user->clients == "") 

			<p class="text-center">Submit your first client to get started.</p>
                    <p class="text-center"><a href="{{{ route('createClient') }}}" class="btn btn-sm btn-success btnBig">Add First Client</a></p>

		@else

			<p style="margin-bottom: 20px;">Welcome to your dashboard.  Your list of clients are below, you may view available <a title="Marketing Resources" href="{{{ route('marketing') }}}">  Marketing Resources</a> or <a title="Submit New Client" href="{{{ route('createClient') }}}">Submit A New Client</a>.  If you need to update your profile click Profile above.</p>

			<h3 class="mainTitle"><span class="glyphicon glyphicon-user"></span> Client Tracking</h3>

		      <div class="filterLabel">Filter</div>

		        <div class="filtersBox">

		          {{ Form::select('status_action', array('All' => 'All Statuses', 'Queued In System' => 'Queued In System') + $allStages, Input::old('status_action', (isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->id : '21')), ['id' => 'statusSelect']) }}

		          <button id="clearFilter" class="pull-right btn btn-default btn-sm">Clear Filter</button>

		    </div>

		<table id="clients" class="table table-striped table-hover order-column compact">
				<thead>
					<tr>
                                      <th class="col-md-2">Business Name</th>
						<th class="col-md-2">Client Name</th>
						<th class="col-md-2">Last Updated</th>
						<th class="col-md-2">Current Status</th>
						<th class="col-md-2">Comments</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>

		@endif

	@else

		<div class="alert alert-danger">Your dashboard is currently disabled.</div>
		<p>To submit clients and receive your Agent ID you must make sure you have reviewed and completed all forms and faxed to 562-590-9393 or email  {{ HTML::mailto('info@multivisionfinancial.com') }}.</p>

		<p>Please look in your email inbox (or Spam Box) with the subject <em><strong>"MVF Registration: Agent Contract Forms"</strong></em> for more information. </p>

		<p class="disabledError">If you believe you are recieving this message out of error, please make sure to <a href="{{{ URL::to('help') }}}">contact support</a>.  <br/>Email not received? Please check your spam box and add info@multivisionfinancial.com to your email client address book.</p>

	@endif

@stop

@section('sidebar')

	@if ($user->confirmed == '1' && Auth::user()->hasRole('Agent'))

		<div class="innerSidebar">

			<h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Recent Activity <span class="statusUpdateText"> Last 7 Days</span></h4>

			<ul class="recentActivityUl">

				@forelse($results as $recent) 

					<li class="animated fadeIn">
						 <a title="" href={{ URL::to('user/client/'.$recent->content_id.'/status') }}>
						 	<p class="activityDetails"> 

						 		@if ($recent->user_id == $user->id)

						 			You {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@else

						 			{{ $recent->first_name }} {{$recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@endif

						 	</p>
						 </a>
						 <div style="clear: both;"></div>
					</li>

				@empty

					<li>No activity</li>

				@endforelse

				{{ $results->links() }}
				<div style="clear: both;"></div>
			</ul> 
		</div>

		<div class="innerSidebar">
			<h4 class="sidebarH4"><span class="glyphicon glyphicon-import"></span> Account Actions</h4>
			<p class="text-center"><a href="{{{ route('createClient') }}}" class="btn btn-sm btn-success btnBig">Click to Submit New Client</a></p>
			<p class="text-center"><a href="{{{ route('marketing') }}}" class="btn btn-sm btn-primary btnBig">View Marketing Resources</a></p>
		</div>

		<div class="innerSidebar">
			<h4 class="sidebarH4"><span class="glyphicon glyphicon-star"></span> Your Agent ID</h4>
				@if($user->office_id == '')
					<p><strong>Unverified</strong></p>
				@else
					<p style="text-align: center; font-size: 1.3em;"><strong>{{ $user->office_id }}</strong></p>
				@endif
		</div>

		<div class="innerSidebar dashSideAgent">

			<h4 class="sidebarH4"><span class="icon icon-user"></span> Your Inhouse Agent </h4>

				@if(count($inhouseAgent))

					<p><span>Name: </span>{{ $inhouseAgent->first_name.' '.$inhouseAgent->last_name}}</p>

					<p><span>Phone: </span> {{ $inhouseAgent->phone_number }} </p>

					<p><span>Email: </span> {{ HTML::mailto($inhouseAgent->email, $inhouseAgent->email) }} </p>

				@else 

					<p style="font-weight: bold; text-align: center;"> No Agent Assigned </p>

				@endif

		</div> 


	@else

		<h4 class="sidebarH4">Agent ID: </h4>
		<p>Agent ID is issued after you are confirmed.</p>

		<h4 class="sidebarH4">Inhouse Agent </h4>
		<p> No Agent Assigned </p>
		
		<h4 class="sidebarH4">Contract Forms</h4>
		<ol>
			<li><a href="https://www.dropbox.com/sh/resg642brm4iixg/AAAlH4yg1YTUN4i1zp4U954Xa">W9 Form</a></li>
			<li><a href="https://www.dropbox.com/sh/resg642brm4iixg/AAAlH4yg1YTUN4i1zp4U954Xa">Agent Contract &amp; Comp Plan</a></li>
			<li><a href="https://www.dropbox.com/sh/resg642brm4iixg/AAAlH4yg1YTUN4i1zp4U954Xa">Agent Duties</a></li>
		</ol>

		<p>Fax to <em>562-590-9393</em> or Email  {{ HTML::mailto('jim@multivisionfinancial.com') }}</p>

	@endif

@stop


{{-- Scripts --}}
@section('scripts')
  <script type="text/javascript">
   var oTable;
    $(document).ready(function() {
        oTable = $('#clients').dataTable( {
       "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
          "sLengthMenu": "_MENU_ records per page",
          "sProcessing": "<img src='/assets/img/loader.gif'> processing..."
        },
        "aaSorting": [[1, 'desc']],
        "bProcessing": true,
        "bServerSide": false,
        "bStateSave" : true,
        "sAjaxSource": "{{ URL::action('UserController@getClientDataList', $user->id) }}", 
        "cache" : true,
        "fnStateSaveParams": function (settings, data) {    
            if( data.selectedValue == 'All Statuses' || data.selectedValue == '' || data.selectedValue == 'All'){  
              data.selectedValue = 'All';       
            }
            else{
              data.selectedValue = $( "#statusSelect option:selected" ).val();       
            }
          },
          "fnStateLoadParams": function (settings, data) {  
              if( data.selectedValue == 'All Statuses' || data.selectedValue == '' || data.selectedValue == 'All'){  
                $("#statusSelect").val('All');
              }
              else{
                $("#statusSelect").val(data.selectedValue);
              }
          }
      })

          // Clear the filter. Unlike normal filters in Datatables,
          // custom filters need to be removed from the afnFiltering array.
          $('#clearFilter').on('click', function(e){
	            e.preventDefault();
	            oTable.fnFilterClear();
	            $("#statusSelect").val("All");
	            oTable.fnDraw();

          });

         $('select#statusSelect').on('change',function(){
            //var selectedValue = $(this).val();
            var selectedValue = $( "#statusSelect option:selected" ).text();
            if( selectedValue == 'All Statuses' || selectedValue == '')
            {
              oTable.fnFilterClear();
            }else{
              oTable.fnFilter("^"+selectedValue+"$",3, true); //Exact value, column, reg  
            }
        }
      );

    });


  </script>
@stop