@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
Login
@stop

{{-- Content --}}
@section('content')
    @if ( Session::get('notice') )
            <div class="alert alert-success">{{ Session::get('notice') }}</div>
    @endif

    @if ( Session::get('error') )
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
    @endif
    
<form class="form-horizontal" method="POST" action="{{ URL::to('user/login') }}" accept-charset="UTF-8">

    {{ Form::token(); }}
    
    <fieldset>
        <div class="form-group">
            <label class="col-md-2 control-label" for="email">Email</label>
            <div class="col-md-6">
                <input class="form-control" tabindex="1" placeholder="Email" type="text" name="email" id="email" value="{{ Input::old('email') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="password">
                {{ Lang::get('confide::confide.password') }}
            </label>
            <div class="col-md-6">
                <input class="form-control" tabindex="2" placeholder="{{ Lang::get('confide::confide.password') }}" type="password" name="password" id="password">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-6">
                <div class="checkbox loginCheckbox">
                    <label for="remember">{{ Lang::get('confide::confide.login.remember') }}
                        <input type="hidden" name="remember" value="0">
                        <input tabindex="4" type="checkbox" name="remember" id="remember" value="1">
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-6">
                <button tabindex="3" type="submit" class="btn btn-primary">{{ Lang::get('confide::confide.login.submit') }}</button>
                <a class="btn btn-default" href="forgot">{{ Lang::get('confide::confide.login.forgot_password') }}</a>
            </div>
        </div>
    </fieldset>
</form>

@stop


@section('sidebar')
    <h4 class="sidebarH4">Need Help?</h4>
    <p>If you need help with your account or have any questions visit <a href="{{ URL::to('help') }}" title="Customer Support" alt="Customer Support Help Center">help center.</a></p>
@stop