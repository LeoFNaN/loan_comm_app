@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
Submit Client Form - MVF Agent Center
@stop

@section('pageTitle')
Submit Client
@stop
 
{{-- Content --}}
@section('content')

<p style="color: #ff0000;">* denotes mandatory field</p>

<div class="homeForm col-sm-8 col-md-offset-2">

    <div class="contactTop">
        <h4>Client Form </h4><img src="{{ URL::asset('/assets/img/form-arrow.png') }}" />
    </div>

    <div id="form-wrap">

        @foreach($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible alert-block">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                    {{ $error }}
                </div>
        @endforeach

            {{ Form::open(array('url' => array('user/client/create'))) }}

                @if ( Session::get('error') )
                    <div class="alert alert-error alert-danger">
                        @if ( is_array(Session::get('error')) )
                            {{ head(Session::get('error')) }}
                        @endif
                    </div>
                @endif

                @if ( Session::get('notice') )
                    <div class="alert">{{ Session::get('notice') }}</div>
                @endif

            <fieldset>
                <div class="form-group">
                    <label for="first_name">First Name*</label>
                    <input class="form-control" placeholder="First Name" type="text" name="first_name" id="first_name" value="{{{ Input::old('first_name') }}}">
                </div>

                <div class="form-group">
                    <label for="first_name">Last Name*</label>
                    <input class="form-control" placeholder="Last Name" type="text" name="last_name" id="last_name" value="{{{ Input::old('last_name') }}}">
                </div>

                 <div class="form-group">
                    <label for="phone">Phone*</label>
                    <input class="form-control" placeholder="Phone" type="text" name="phone" id="phone" value="{{{ Input::old('phone') }}}">
                </div>

                <div class="form-group">
                    <label for="email">Email*</label>
                    <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
                </div>

                <div class="form-group">
                    <label for="business_name">Business Name</label>
                    <input class="form-control" placeholder="Business Name" type="text" name="business_name" id="business_name" value="{{{ Input::old('business_name') }}}">
                </div>

                <div class="form-group">
                    <label for="loan_amount">Desired Loan Amount</label>
                    <input class="form-control" placeholder="Loan Amount" type="text" name="loan_amount" id="loan_amount" value="{{{ Input::old('loan_amount') }}}">
                </div>

                  <div class="form-group">
                    <label for="business_years">Years In Business</label>
                    <input class="form-control" placeholder="Years In Business" type="text" name="business_years" id="business_years" value="{{{ Input::old('business_years') }}}">
                </div>

                <div class="form-group">
                    <label for="state">State</label>
                    {{ Form::select('state', ['Select One' => 'Select One','AL' => 'AL','AK' => 'AK','AZ' => 'AZ','AR' => 'AR','CA' => 'CA','CO' => 'CO','CT' => 'CT','DE' => 'DE','DC' => 'DC','FL' => 'FL','GA' => 'GA','HI' => 'HI','ID' => 'ID','IL' => 'IL','IN' => 'IN','IA' => 'IA','KS' => 'KS','KY' => 'KY','LA' => 'LA','ME' => 'ME','MD' => 'MD','MA' => 'MA','MI' => 'MI','MN' => 'MN','MS' => 'MS','MO' => 'MO','MT' => 'MT','NE' => 'NE','NV' => 'NV','NH' => 'NH','NJ' => 'NJ','NM' => 'NM','NY' => 'NY','NC' => 'NC','ND' => 'ND','OH' => 'OH','OK' => 'OK','OR' => 'OR','PA' => 'PA','RI' => 'RI','SC' => 'SC','SD' => 'SD','TN' => 'TN','TX' => 'TX','VT' => 'VT','VA' => 'VA', 'WA' => 'WA', 'WV' => 'WV','WI' => 'WI','WY' => 'WY','Canada' => 'Canada'], null, ['class' => 'form-control']) }}
                </div>

                 <div class="form-group">
                    <label for="monthly_revenue">Monthly Revenue</label>
                    <input class="form-control" placeholder="Monthly Revenue" type="text" name="monthly_revenue" id="monthly_revenue" value="{{{ Input::old('monthly_revenue') }}}">
                </div>

                 <div class="form-group">
                    <label for="callback_time">Best Time To Call Client</label>
                    {{ Form::select('callback_time', ['Select One' => 'Select One','Immediately' => 'Immediately','Morning' => 'Morning','Afternoon' => 'Afternoon','Evening' => 'Evening'], null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    <label for="client_message">Message/Special Instructions:</label>
                    <textarea class="form-control" placeholder="Enter details or notes about client" type="text" name="client_message" id="client_message">{{{ Input::old('client_message') }}}</textarea>
                </div>

                <div class="form-actions form-group">
                  <button type="submit" class="btn btn-primary">Click to Submit Client</button>
                </div>

            </fieldset>

        </form>
    </div>
</div>
@stop

@section('sidebar')
	<h4 class="sidebarH4">Information</h4>
	<p>Please fill out all information completely, your submission will be posted on your <a href="{{ URL::to('user/dashboard') }}">Dashboard</a>.</p>
           <p>Your Inhouse Agent will be automatically notified of your submission and will contact the client.</p>
           <p style="color: red;">Not every client submitted will be approved, closed or funded.</p>

@stop