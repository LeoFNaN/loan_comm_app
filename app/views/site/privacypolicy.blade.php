@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
MVFLoans - Privacy Policy
@stop

@section('page-title')
Privacy Policy
@stop

@section('content')
<p>Multi Vision Financial, (MVF)  collects certain confidential information from its customers (and potential customers) through the application process. MVF maintains and uses such customer information as set forth in this privacy policy:</p>
<ul>
	<li>MVF is committed to protecting customer information in each of its merchant transactions.</li>
	<li>MVF does not sell customer information to third parties.</li>
	<li>MVF does not share customer information with advertisers or marketers who wish to market their products to you.</li>
	<li><span style="line-height: 1.5em;">You do not have to take any action or instruct MVF to keep your information confidential.</span></li>
	<li><span style="line-height: 1.5em;">MVF uses commercially reasonable efforts to safeguard customer information.</span></li>
	<li><span style="line-height: 1.5em;">MVF maintains the procedure and technology described in this privacy policy.</span></li>
</ul>
<p>MVF’s corporate mission is to be a trusted source of service and reliability for its customers. MVF is dedicated to these principles.</p>
<h4>HOW MULTI VISION FINANCIAL PROTECTS AND USES CUSTOMER INFORMATION</h4>
<ul>
	<li>MVF safeguards the security and integrity of customer information.</li>
	<li>MVF limits access to customer files to those officers who have a business need to know and access such information.</li>
	<li>MVF maintains policies and procedures to secure physical access to its confidential customer information including:</li>
	<li>Locking down its physical files</li>
	<li>Shredding all discarded customer information</li>
	<li>MVF requires its independent contractors and third party vendors who provide services to MVF to adhere to strict privacy standards</li>
	<li>MVF uses technological means (including back up files, offsite secured storage of electronic files, firewalls, and encryption schemes) to protect against unauthorized access or alterations of confidential customer data.</li>
	<li>MVF collects and maintains customer information as part of the MVF application process. MVF collects information about merchant business and its owners/operators from a variety of sources, including:</li>
</ul>
<ul>
	<li>Information provided to us via the MVF application and other required forms</li>
</ul>
<ul>
	<li>Information MVF receives from third parties that the merchant business authorizes it to contact, such as the merchant’s trade, banking, and landlord references regarding credit history</li>
</ul>
<ul>
	<li>Information MVF receives from third party credit and background reporting agencies whom it contacts to verify merchants’ credit history and general background information.</li>
</ul>
<ul>
	<li>MVF does not share customer confidential information with third parties.</li>
	<li>MVF does not provide confidential customer information to third party advertisers or marketers.</li>
	<li>MVF does not provide confidential customer information to credit bureaus and/or credit reporting agencies.</li>
	<li>MVF will promptly notify a merchant business in writing or by telephone if MVF suspects that there has been a breach of confidentiality regarding confidential customer information.</li>
</ul>
<p>The policies and practices described in this disclosure are subject to change, and any such changes will be communicated to MVF merchants and published on the MVF website. Please contact us at 888-598-9951 if you have questions regarding this policy. (updated June 28, 2014)</p>
@stop