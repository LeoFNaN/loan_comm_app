@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
	Help Center - FAQs and Contact Information
@stop

@section('page-title')
	FAQs
@stop

{{-- Content --}}
@section('content')

<p>Please view the frequently asked questions first.  If you need further assistance fill out the form below and Jim Hewitt Referral Agent Director will get back to you as soon as possible.  Click on the question to view the answer.</p>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
    	<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
      <h4 class="panel-title">Q. How do I sign up as an agent? <span class="glyphicon glyphicon-chevron-down pull-right" aria-hidden="true"></span>
      </h4></a>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
	      	<ol>
			<li>Click on the green “sign up now” button in the menu above. Be sure to list how you were referred to our program.  Click on the appropriate in-house agent name so they can be assigned to you.  Write down your password for future reference. </li>
			<li>Once you submit your information you will receive an email with your Referral Agent Agreement.  Fill it out and send it back.  </li>
			<li>Your dashboard will be activated when we have your agreement.  Now you can start submitting leads!</li>
		</ol>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><h4 class="panel-title">
        
          Q. I can’t log into my dashboard.<span class="glyphicon glyphicon-chevron-up pull-right" aria-hidden="true">
        
      </h4></a>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        	 <ol>
			<li>	Have you sent in your agreement/paperwork? 
				<ul>
					<li>If yes, {{ HTML::link('user/login', 'click here')}}.  Use your email your paperwork was sent to and the password you created.</li>
					<li>If no, your dashboard won’t be activated until we receive your paperwork. </li>
				</ul>
			</li>

			<li>Did you forget your password? 
				<ul>
					<li>Reset your password {{ HTML::link('user/forgot', 'here')}}. Enter your email address and click "continue".  You will be immediately emailed a link to change your password. </li>
				</ul>
			</li>

			<li>Still need help? 
				<ul>
					<li>Email  {{ HTML::mailto('customerservice@multivisionfinancial.com', 'Customer Support') }} or contact your in-house agent.</li>
				</ul>
			</li>
		</ol>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
       <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><h4 class="panel-title">
       
           Q. What paperwork is needed? <span class="glyphicon glyphicon-chevron-up pull-right" aria-hidden="true">
       
      </h4> </a>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
      	        <ul>
			<li>Just your agreement is needed to get started.  If you can’t find the email with it, just call us - 888-598-9951 </li>
		</ul>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFour">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><h4 class="panel-title">
        
           Q. Is there a fee to be a referral agent?<span class="glyphicon glyphicon-chevron-up pull-right" aria-hidden="true">
        
      </h4></a>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body">
        	<ul>
			<li>Nope, not at all.</li>
		</ul>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFive">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"><h4 class="panel-title">
        
           Q. How do I submit a lead? <span class="glyphicon glyphicon-chevron-up pull-right" aria-hidden="true">
        
      </h4></a>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body">
	      	<ol>
			<li>Log into your dashboard. Now click on the Green Button “Click to submit new client”. </li>
			<li>	The basic info required for the lead is first name, last name, email, phone number.  </li>
			<li>Your in-house agent will receive instant notification of the lead submitted. </li>
		</ol>
      </div>
    </div>
  </div>

    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingSix">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><h4 class="panel-title">
        
           Q. How and when do I get paid?  <span class="glyphicon glyphicon-chevron-up pull-right" aria-hidden="true">
        
      </h4></a>
    </div>
    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
      <div class="panel-body">
	      	<ol>
			<li>When a lead you submitted signs and funds, you will be issued a check within days of when the bank funds us.  (if your w-9 and copy of ID are on file)</li>
			<li>	In your dashboard “Marketing Resources” tab is a blank w-9 that you can print and send in, along with a copy of your ID, when you first deal funds.  </li>
			<li>Normally we will mail you a check, please call if you have questions or need other options. </li>
		</ol>
      </div>
    </div>
  </div>

    <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingSeven">
	      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven"><h4 class="panel-title">
	        
	           Q. Do I have to submit a certain number of leads or close deals to earn the 3-5 points? <span class="glyphicon glyphicon-chevron-up pull-right" aria-hidden="true">
	        
	      </h4></a>
	    </div>
	    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
	      <div class="panel-body">
		      	<ul>
				<li>No. There is no tiered compensation plan.</li>
			</ul>
	      </div>
	    </div>
  </div>

    <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingEight">
	      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"><h4 class="panel-title">
	        
	           Q. What is the best way to market or find leads? <span class="glyphicon glyphicon-chevron-up pull-right" aria-hidden="true">
	        
	      </h4></a>
	    </div>
	    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
	      <div class="panel-body">
		      	<ul>
				<li>Using your current book of business is the quickest way to find clients.  Or maybe you are successful at lead generation or just know how to market yourself in this arena. </li>
			</ul>
	      </div>
	    </div>
  </div>

</div>

<h2 class="mainTitle">	Contact Form </h2>

@if($errors->has())
		<div class="alert alert-danger alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

<div id="form-wrap" class="col-md-6">

	{{ Form::open(array('url' => 'help')) }}

		<fieldset>

			 <div class="form-group">
				{{ Form::label('name', 'Name *') }}
				{{ Form::text('name', null, array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('email', 'Email *') }} 
				{{ Form::email('email', null, array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('phone', 'Phone') }} 
				{{ Form::text('phone', null, array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('message', 'Questions/Message *') }} 
				{{ Form::textarea('message', null, array('class' => 'form-control contactTextArea')) }}
			</div>

			{{ Form::submit('Send Message', array('class' => 'btn btn-primary')) }}

		</fieldset>

	{{ Form::close() }}
</div>
	
@stop

@section('sidebar')

	<h4 class="sidebarH4 noTop">Company Information</h4>
	<div class="helpInfo">
		<p><strong>Phone:</strong> 888-598-9951</p>
		<p><strong>Address</strong>: 235 E. Broadway St #940<br/>Long Beach, CA 90802</p>
		<p><strong>Email:</strong> {{ HTML::mailto('customerservice@multivisionfinancial.com', 'Customer Support') }}</p> 
		<p><strong>Business Loan Site:</strong> {{ HTML::link('https://MultiVisionFinancial.com', 'www.MultiVisionFinancial.com') }}</p>
		<p><strong>Hours:</strong> Mon - Fri: 9 - 5pm (pst)</p>
	</div>
	<img class="sidebarImg img-responsive" src="{{ URL::asset('assets/img/mvf-longbeach-building-loc.jpg') }}" title="USA and Canada" width="290px" height="414">

	

@stop