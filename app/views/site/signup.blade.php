@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
	Signup and Registration - MVFLoans
@stop

@section('page-title')
	<span style="color: #0bd821;">Free</span> Sign Up!
@stop

{{-- Content --}}
@section('content')
	   <h4 class="sidebarH4"> Activation Process</h4>

		<div class="regBox">
			
			<h5 class="animated fadeIn">STEP 1:  Create Account &rarr;</h5>
			<p>After completing the sign up form (on right), check your inbox for your contracts. Email subject <em>"MVF Registration: Agent Contract Forms"</em>.
        
			<h5 class="animated fadeIn">STEP 2: Complete Your Contract</h5>
			<p>Your contract will be in your email inbox (Check your spam). Review &amp; complete all forms listed in the email and <em>fax to (562-590-9393)</em> or <em>email (info@MultiVisionFinancial.com)</em></p>

			<h5 class="animated fadeIn">STEP 3: Return Signed Contract</h5>
			<p><strong>After you complete and send in your paperwork your account will be activated by Director Jim Hewitt or your Inhouse Agent.  Your agent id will be created and you may login to start submitting clients.</strong></p>

			<p style="text-align: center; font-style: italic; font-weight: bold;">There are no charges and no fees to become a MVF Referral Agent!</p>

		</div>

	<div class="col-sm-7 agentFeaturesSignup">

	   <h4 class="sidebarH4" style="font-size: 1.8em;">Referral Agent Center Features</h4> 

		<ul> 
			<li>Track your clients progress</li>
			<li>Up to date information/status</li>
			<li>Easy submission form</li>
			<li>Communicate with your own inhouse agent</li>
			<li>Agent Marketing Resources</li>
                                        <li>Online Meetings every Wednesday @12pm pst</li>
		</ul>

	</div>

	<div class="col-sm-5 animated bounce" style="margin-top: 10px; margin-bottom: 10px;">
		<img title="Agent Features" alt="MVF Sign up" src="{{ URL::asset('assets/img/two-agents-mvf.jpg') }}" width="200" height="210" />
	</div> 

	<div class="hidden-xs" style="clear: both; padding-top: 10px;">
	   <p>Haven't received any of our emails? Remember to check your spam box.</p>
	   <p>If you need any help or have questions please contact us <a title="Contact Us" alt="Contact Us" href="{{ URL::to('contact-us') }}">here</a> or call 888-598-9951</p>
	</div>

@stop

@section('sidebar')
	
    <div class="contactTop">
        <h4>Create Your Account</h4><img src="{{ URL::asset('/assets/img/form-arrow.png') }}" width="" height="" />
    </div>

	<div id="form-wrap" class="homeForm">
        
                    @if ( Session::get('error') )
                        <div class="alert alert-error alert-danger">
                            @if ( is_array(Session::get('error')) )
                                {{ head(Session::get('error')) }}
                            @endif
                        </div>
                    @endif

                    @if ( Session::get('notice') )
                        <div class="alert">{{ Session::get('notice') }}</div>
                    @endif

	<form class="signupForm" method="POST" action="{{{ (Auth::check('UserController@store')) ?: URL::to('signup')  }}}" accept-charset="UTF-8" autocomplete="off">

            {{ Form::token(); }}
            
                <fieldset>

                    <div class="form-group">
                        <label for="first_name">First Name *</label>
                        <input class="form-control" placeholder="First Name" type="text" name="first_name" id="first_name" value="{{{ Input::old('first_name') }}}">
                    </div>

                    <div class="form-group">
                        <label for="last_name">Last Name *</label>
                        <input class="form-control" placeholder="Last Name" type="text" name="last_name" id="last_name" value="{{{ Input::old('last_name') }}}">
                    </div>

                    <div class="form-group">
                        <label for="phone_number">Phone Number *</label>
                        <input class="form-control" placeholder="Phone Number" type="text" name="phone_number" id="phone_number" value="{{{ Input::old('phone_number') }}}" autocomplete="off">
                    </div>

                    <div class="form-group">
                        <label for="email">{{{ Lang::get('confide::confide.e_mail') }}} *</label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.e_mail') }}}" type="text" name="email" id="email" value="{{{ Input::old('email') }}}">
                    </div>

                    <div class="form-group">
                        <label for="password">{{{ Lang::get('confide::confide.password') }}} *</label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password">
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">{{{ Lang::get('confide::confide.password_confirmation') }}} *</label>
                        <input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation">
                    </div>

                    <div class="form-group">
                        <label for="referral_source">How did you hear about us?</label>

                        <select name="referral_source" class="form-control" id="referral_source">

                            <option value="No Referral" selected="selected">Select One</option>

                            <optgroup label="Agent Name">
                                <option value="Anthony Smith">Anthony Smith</option>
                                <option value="Elisa Blackmon">Elisa Blackmon</option>
                                <option value="Janet Williams">Janet Williams</option>
                                <option value="Jim Hewitt">Jim Hewitt</option>
                                <option value="Leo Fisher">Leo Fisher</option>
                                <option value="Danny Fernandez">Danny Fernandez</option>
                                <option value="Monica Briggs">Monica Briggs</option>
                                <option value="Robert Espinoza">Robert Espinoza</option>
                            </optgroup>

                            <optgroup label="Ads/Online">
                                <option value="TV">Television</option>
                                <option value="Newspaper">Newspaper</option>
                                <option value="Flyer">Flyer</option>
                                <option value="Online">Online</option>
                                <option value="Other">Other</option>
                            </optgroup>

                          </select>

                    </div>

                    <div class="form-group">
                        <label for="agent_reason">Why would you be a good referral agent?</label>
                        <textarea class="form-control" placeholder="Enter Message" type="text" name="agent_reason" id="agent_reason">{{{ Input::old('agent_reason') }}}</textarea>
                    </div>


                    <div class="text-center col-sm-12" style="margin-top: 10px;">
                      {{ Form::submit('Click To Submit', array('class' => 'btn btn-primary', 'style' => 'width: 200px;')) }}
                    </div>

                    <p class="text-center notShared">*Your Information will not be shared nor sold. <a href="{{ URL::to('/privacypolicy') }}" style="color:#0000ff;">Privacy Policy</a></p>

                </fieldset>

</form>


</div>
	
	<div class="visible-xs">
		<h4 class="sidebarH4"></h4>
		<p>Haven't received any of our emails? Remember to check your spam box.</p>
		<p>If you need any help or have questions please contact us <a alt="Contact Us" title="Contact Us" href="{{ URL::to('contact-us') }}">here</a> or call 888-598-9951</p>
	</div>

@stop