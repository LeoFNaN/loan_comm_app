<div id="defaultNav" class="navbar navbar-default navbar-inverse navbar-fixed-top">
	<div class="container">
           <div class="navbar-header">

             <div class="logo">
              	<a title="Back to MVF Business Loans" alt="Back to MVF Business Loans" href="https://www.multivisionfinancial.com"><img title="Back to MVF Business Loans" alt="Back to MVF Business Loans" src="{{ asset('assets/img/mvf-logo.jpg') }}" width="160" height="50" /></a>
            </div>
          
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

          <div class="collapse navbar-collapse navbar-ex1-collapse">

           <ul class="nav navbar-nav user-nav">
				<li {{ (Request::is('/') ? ' class="active"' : '') }}><a title="Home" alt="Home" href="{{{ URL::to('') }}}">Home</a></li>
				<li {{ (Request::is('help') ? ' class="active"' : '') }}><a title="Get Help" alt="Get Help" href="{{{ URL::to('help') }}}">Help Center</a></li>
		</ul>

                <ul class="nav navbar-nav user-nav pull-right">

                    @if (Auth::check())

                      @if (Auth::user()->hasRole('admin'))				
					<li class="dropdown{{ (Request::is('admin/dashboard*|admin/roles*') ? ' active' : '') }}">
    						<a title="Your Account" alt="Your Account" class="dropdown-toggle" data-toggle="dropdown" href="{{{ URL::to('admin') }}}">
    							<span class="glyphicon glyphicon-user"></span> {{{ ucfirst(Auth::user()->first_name) }}} Account<span class="caret"></span>
    						</a>
     						<ul class="dropdown-menu">
    							<li{{ (Request::is('admin/dashboard*') ? ' class="active"' : '') }}><a title="Dashboard" alt="Dashboard" href="{{{ URL::to('admin/dashboard') }}}"><span class="icon icon-dashboard"></span> Dashboard</a></li>
    							<li{{ (Request::is('user/profile*') ? ' class="active"' : '') }}><a title="Settings" alt="Settings" href="{{{ URL::to('user/profile/') }}}"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>
    							
    						</ul>

    					</li>

                          <li class="dropdown{{ (Request::is('admin/blogs*|admin/comments*') ? ' class="active"' : '') }}"><a title="News" alt="News" class="dropdown-toggle" data-toggle="dropdown" href="{{{ URL::to('admin/blogs') }}}"><span class="glyphicon glyphicon-list-alt"></span> News <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li {{ (Request::is('admin/blogs*') ? ' class="active"' : '') }}><a title="Manage News" alt="Manage News" href="{{{ URL::to('admin/blogs') }}}"><span class="glyphicon glyphicon-list-alt"></span> Manage News</a></li>

                                    <li{{ (Request::is('admin/comments*') ? ' class="active"' : '') }}><a title="Manage Comments" alt="Manage Comments" href="{{{ URL::to('admin/comments') }}}"><span class="glyphicon glyphicon-bullhorn"></span> Manage Comments</a></li>

                                </ul>
                          </li>

	                          <li><a title="Logout" alt="Logout" href="{{{ URL::to('user/logout') }}}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>

    					@elseif (Auth::user()->hasRole('Agent'))

    						<li {{ (Request::is('user/dashboard*') ? ' class="active"' : '') }}><a title="Dashboard" alt="Dashboard" href="{{{ URL::to('user/dashboard') }}}"><span class="icon icon-dashboard"></span> Dashboard</a></li>

    						<li {{ (Request::is('user/profile*') ? ' class="active"' : '') }}><a title="Profile" alt="Profile" href="{{{ URL::to('user/profile/') }}}"><span class="glyphicon glyphicon-user"></span> Profile</a></li>

    						<li><a title="Logout" alt="Logout" href="{{{ URL::to('user/logout') }}}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>

	    				@elseif ( Auth::user()->hasRole('Inhouse')) 

		    				<li {{ (Request::is('inhouse/dashboard*') ? ' class="active"' : '') }}><a title="Dashboard" alt="Dashboard" href="{{{ URL::to('inhouse/dashboard') }}}"><span class="icon icon-dashboard"></span> Dashboard</a></li>
		    							
		    				<li {{ (Request::is('inhouse/profile*') ? ' class="active"' : '') }}><a title="Profile" alt="Profile" href="{{{ URL::to('inhouse/profile/') }}}"><span class="glyphicon glyphicon-cog"></span> Profile</a></li>
		    							
		    				<li><a title="Logout" alt="Logout" href="{{{ URL::to('user/logout') }}}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>

    					@elseif ( Auth::user()) 

    						<li {{ (Request::is('user/dashboard*') ? ' class="active"' : '') }}><a title="Dashboard" alt="Dashboard" href="{{{ URL::to('user/dashboard') }}}"><span class="icon icon-dashboard"></span> Dashboard</a></li>
	    							
	    					<li {{ (Request::is('user/profile*') ? ' class="active"' : '') }}><a title="Profile" alt="Profile" href="{{{ URL::to('user/profile/') }}}"><span class="glyphicon glyphicon-cog"></span> Profile</a></li>
	    							
	    					<li><a title="Logout" alt="Logout" href="{{{ URL::to('user/logout') }}}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>

                      @endif
                    		
                    @else
                   		<li {{ (Request::is('user/login') ? ' class="active"' : '') }}><a title="Login" alt="Login" href="{{{ URL::to('user/login') }}}">Login</a></li>
                    		<li {{ (Request::is('/signup') ? ' class="active"' : 'class="signupBtn"') }}><a title="Sign Up" alt="Sign Up" href="{{{ URL::to('/signup') }}}">{{{ Lang::get('site.sign_up') }}}</a></li>
                    @endif
                </ul>

		</div>
	</div>
</div>
