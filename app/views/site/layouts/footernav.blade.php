 <div class="navfooterWrap text-center">

          <ul class="nav navbar-nav navbar-footer">

                <li {{ (Request::is('/') ? ' class="active"' : '') }}><a title="Home" alt="Home" href="{{{ URL::to('') }}}">Home</a></li>

                      @if (Auth::check())

                          @if (Auth::user()->hasRole('admin'))  

                                <li><a title="Dashboard" alt="Dashboard" href="{{{ URL::to('admin/dashboard') }}}"><span class="icon icon-dashboard"></span> Dashboard</a></li>
                            
                          @elseif (Auth::user()->hasRole('Agent'))

                                <li><a title="Dashboard" alt="Dashboard" href="{{{ URL::to('user/dashboard') }}}"><span class="icon icon-dashboard"></span> Dashboard</a></li>

                          @elseif ( Auth::user()->hasRole('Inhouse')) 

                                <li><a title="Dashboard" alt="Dashboard" href="{{{ URL::to('inhouse/dashboard') }}}"><span class="icon icon-dashboard"></span> Dashboard</a></li>

                          @endif

                      @else
                      
                            <li {{ (Request::is('user/login') ? ' class="active"' : '') }}><a title="Login" alt="Login" href="{{{ URL::to('user/login') }}}">Login</a></li>
                            
                            <li {{ (Request::is('signup') ? ' class="active"' : '') }}><a title="Sign Up" alt="Sign Up" href="{{{ URL::to('signup') }}}">{{{ Lang::get('site.sign_up') }}}</a></li>

                      @endif

                            <li {{ Request::is('termsandconditions') ? ' class="active"' : '' }}><a title="" alt="" href="{{{ URL::to('termsandconditions') }}}">Terms and Conditions</a></li>

                            <li {{ Request::is('privacypolicy') ? ' class="active"' : '' }}><a title="" alt="" href="{{{ URL::to('privacypolicy') }}}">Privacy Policy</a></li>

                            <li {{ (Request::is('help') ? ' class="active"' : '') }}><a title="Get Help" alt="Get Help" href="{{{ URL::to('help') }}}">Help Center</a></li>

           </ul>
           
    </div>