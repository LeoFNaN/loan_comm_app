<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>
			@section('title')
				MVF - ISO/Referral Agent Center
			@show
		</title>
		<meta name="keywords" content="refferal, iso agents, loan agents, multi vision financial" />
		<meta name="author" content="Multi Vision Financial" />
		<meta name="description" content="Join our Referral Program and earn commissions from who you know. Excellent for financial professionals." />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

        	<link rel="stylesheet" href="{{asset('bootpaper/bootstrap.min.css')}}">
        	<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
        	<link rel="stylesheet" href="{{asset('assets/css/styles.css')}}">
        	<link rel="stylesheet" href="{{asset('assets/css/wysihtml5/prettify.css')}}">
        	<link rel="stylesheet" href="{{asset('assets/css/wysihtml5/bootstrap-wysihtml5.css')}}">
        	<link rel="stylesheet" href="{{asset('assets/css/datatables-bootstrap.css')}}">
        	<link rel="stylesheet" href="{{asset('assets/css/colorbox.css')}}">
        	<link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
        	<link rel="stylesheet" href="{{asset('assets/css/icomoon/style.css')}}">
        	
		<style>
		        body {
		            padding: 60px 0;
		        }
			@section('styles')
		</style>


		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">
		<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">
	</head>

	<body>

	<!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MCHP5G"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MCHP5G');</script>
        <!-- End Google Tag Manager -->

		<div id="wrap">
			
			@include('site.layouts.navigation')

		<div class="pageWrap container">

				 @include('notifications')

				<div role="main">

			    		<h2 class="mainTitle">@section('pageTitle') MVF Referral Agent Center 
			    		@show </h2>

					@yield('content')

				</div>
			 
			</div>

		<div id="push"></div>
		</div>
		<!-- ./wrap -->

		<footer class="clearfix">  
		    <div id="footer">

		    		<div class="col-sm-8 col-sm-offset-2">

			      	@include('site.layouts.footernav')

					<div class="clearfix visible-xs-block"></div>
			      	
			      	<p class="text-muted credit text-center">
			      		&copy; MultiVisionFinancial. All rights reserved. | 235 E Broadway St Suite 940, Long Beach, CA | 888-598-9951
			      	</p>

			      	@include('site.layouts.disclaimer')

		      	</div>

		      	<a href="#" class="scrollToTop"></a>

		    </div>
	    </footer>

		<!-- Javascripts -->
	    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
	    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
	    <script src="{{asset('assets/js/wysihtml5/wysihtml5-0.3.0.js')}}"></script>
	    <script src="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>
	    <script src="{{asset('assets/js/dtables/1.10.4.jquery.dataTables.min.js')}}"></script>
	    <script src="{{asset('assets/js/dtables/datatables-bootstrap.js')}}"></script>
	    <script src="{{asset('assets/js/dtables/datatables.fnReloadAjax.js')}}"></script>
	    <script src="{{asset('assets/js/jquery.colorbox.js')}}"></script>
	    <script src="{{asset('assets/js/prettify.js')}}"></script>
	    <script src="{{asset('assets/js/scrolltotop.js')}}"></script>
	    <script src="{{asset('bootstrap/js/tooltip.js')}}"></script>

    <script type="text/javascript">
    	$('.wysihtml5').wysihtml5();
        $(prettyPrint);
    </script>


	<script type="text/javascript">
		$(document).ready(function(){
		    $(".popover-examples a").popover({
		        placement : 'top'
		    });
		});
	</script>

	<script type="text/javascript">
	$(document).ready(function(){
	    $(".close").click(function(){
	        window.setTimeout(function() { // hide alert message
		    $(".alert").removeClass('in'); 
		}, 5000);
		});
		});  
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			 $("body").tooltip({ selector: '[data-toggle=tooltip]' });
		});
	</script>

        @yield('scripts')
	</body>
</html>
