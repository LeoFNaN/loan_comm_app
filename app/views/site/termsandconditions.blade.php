@extends('site.layouts.default')

{{-- Web site Title --}}
@section('title')
MVFLoans - Terms and Conditions
@stop

{{-- Web site Title --}}
@section('page-title')
Terms and Conditions
@stop

@section('content')
<p>By entering the requested information below and clicking I Accept, you are hereby accepting the Multi Vision Financial Terms and Conditions consenting to receive all information  electronically as a Referral Agent and Independent Contractor.   If you do not accept the agreements as written do not continue.</p>

<p>Your acceptance of the MVF Referral Agents Program:</p>

<p><b>MVF Referral Agents Terms and Conditions:</b></p>
<p>These Terms and Conditions, along with the Order Confirmation/Welcome Letter (collectively, the “Terms,” or the “User Agreement”) describe the terms and conditions on which Multi Vision Financial  (“we”, “our” or “Multi Vision Financial”) offers a user (“Independent Contractor”, “User”, “you”, “your” or “subscriber”) access to the online solution and total solution packages described on its website located at www.multivisionfinancial.com.</p>

<p><b>1. Acceptance of Terms:</b></p>
<p>By submitting your Order, or by accepting the Terms on-line as provided in the welcome e-mail, you agree to the Terms. We may amend the Terms at any time by providing notice to you of any changes. Your sole option, if you do not agree to the changes, will be to terminate the User Agreement within seventeen (17) days of any notice of change. Otherwise, your continued use of our Services will signify your acceptance of any amended Terms. The Terms may not be otherwise amended except in a writing signed by both parties. Further, in the event that a Referral Agent fails to expressly accept the Terms on-line, then the Referral Agent’s use of the Services for any thirty (30) day period shall signify the Referral Agent’s acceptance of the Terms. Throughout this User Agreement, (i) the phrase “in our discretion” or “in its discretion” means Multi Vision Financial’s sole and arbitrary discretion and (ii) the term “including” means “including without limitation.” Multi Vision Financial reserves the right to reject this User Agreement for any reason or no reason, prior to acceptance thereof by Multi Vision Financial. Activation of any Service shall indicate Referral Agents’ acceptance of this User Agreement.</p>

<p><b>2.1. Eligibility.</b></p>
<p>Use of the Services is limited to parties that lawfully can enter into and form contracts under applicable law. Without limiting the foregoing, the Services are not available to minors. Services are not available to Users where use of the Services has been suspended or terminated.</p>

<p><b>2.2. Order Confirmation.</b></p>
<p>You must submit a completed Order on behalf of yourself or the corporation, partnership or other legal entity that will be using the Services. Your Order can be submitted by telephone through one of our sales representatives.  You will receive an Order Confirmation/Welcome Letter which will summarize your Order, allow you to submit all necessary information for us to perform the Services, and provide a link to these Terms (the “Order Confirmation”). By submitting your Order, you agree to these Terms and you also represent that (a) you are eighteen (18) years or older and, if applicable, (b) you are authorized to bind the corporation, partnership or other legal entity that will be using the Services. Your “Order Confirmation” includes your personal information, credit card information and any Services and related fees you select.</p>

<p><b>3. Services.</b></p>

<p><b>3.1. Right to Refuse Services.</b></p>
<p>Multi Vision Financial may refuse the Services to anyone at any time, in our discretion. Multi Vision Financial reserves the right to discontinue, temporarily or permanently, any or all of the Services to anyone at any time, with or without notice. Multi Vision Financial shall not be liable to client or any third-party for any termination of your access to the Services except as set forth herein.</p>

<p><b>3.2. Authority.</b></p>
<p>You expressly grant Multi Vision Financial the authority to act as your agent for the limited purpose of submitting business owner’s contact information to in-house representatives.</p>

<p><b>3.3. Intellectual Property Rights.</b></p>
<p>You represent that you have all necessary rights to use the business name submitted with your Order and that your use of the name will not infringe upon the intellectual property rights of others. You will be solely liable for violations of this Section. In addition, you acknowledge that Multi Vision Financial retains all rights to any domain names, local DID / 800 numbers provided by Multi Vision Financial, website templates, and any other materials of any nature used in the provision of the Services, even if such property was associated exclusively with your business contact information during the term of this User Agreement.</p>

<p><b>3.4. Accurate and Complete Information.</b></p>
<p>You will provide to Multi Vision Financial only true, accurate, current and complete information.</p>

<p><b>3.5. General Compliance with Laws.</b></p>
<p>You will comply with all applicable laws, statutes, ordinances and regulations in your use of the Services.</p>

<p><b>3.6 Cancellation.</b></p>
Web page may be removed from server at the discretion of Multi Vision Financial for inactivity. If no referrals are submitted in a 90 day time period web page may be removed.</p>

<p>All Referral Agent cancellations must be in writing and sent by email to info@multivisionfinancial.com or by mail to the following address:<br/>
Multi Vision Financial<br/>
235 E. Broadway Suite 1020<br/>
Long Beach, CA. 90802</p>

<p><b>4. DID/800 (Toll Free) Telephone Number.</b></p>
<p>At all times hereto, including upon termination or expiration of the User Agreement, any DID/800 Telephone Number (“DID”) which is used  by Referral Agent shall be owned exclusively by Multi Vision Financial. Referral Agent agrees that it has no right, title, or ownership interest in the DID/800 number, but instead is leasing the DID/800# as part of the Services.</p>

<p><b>5. Limitations on Use of Services.</b></p>

<p><b>5.1 Prohibited Uses.</b></p>
<p>You agree to use the Services only for lawful purposes. Multi Vision Financial reserves the right to terminate your Service immediately and without advance notice if 1st Page Exposure, in its sole discretion, believes that you have violated the terms of this User Agreement. Mis-use or mis- representation  of webpage will result in termination.</p>

<p><b>6. Indemnity and Disclaimer</b></p>

<p><b>6.1. Indemnity.</b></p>
<p>You will, at your own expense, indemnify, defend and hold Multi Vision Financial, its entity, authorized representatives, officers, directors, agents, and employees harmless from and against any loss, cost, damages, liability, or expense arising out of or relating to (a) a third-party claim, action or allegation of infringement, misuse or misappropriation based on information, data, files or other content submitted by you to us; (b) any fraud, manipulation, or other breach of this User Agreement by you; (c) any third-party claim, action or allegation brought against Multi Vision Financial arising out of or relating to a dispute with you over the terms and conditions of an agreement or related to the purchase or sale of any goods or services; or (d) your violation of any law or the rights of a third party. Multi Vision Financial will have the right to participate in its defense and hire counsel of its choice, at your expense. You will not settle any action or claims on Multi Vision Financial’s behalf without the prior written consent of Multi Vision Financial.</p>

<p><b>6.2. Disclaimer.</b></p>
<p>Neither Multi Vision Financial nor its entity and authorized representatives will be liable for (i) any loss of business, profits or goodwill, loss of use or data, interruption of business or for any indirect, special, incidental or consequential damages of any character, (however arising, including negligence) arising out of or in connection with this User Agreement even if Multi Vision Financial or authorized representative is aware of the possibility of such damages, or (ii) any damages that result in any way from your use or inability to use the Services, or that result from errors, defects, omissions, delays in operation or transmission, or any other failure of performance of the Services. We do not guarantee sales.</p>

<p><b>7. General.</b></p>

<p><b>7.1. Termination at Multi Vision Financial’s Discretion.</b></p>
<p>In our discretion, we may immediately issue a warning, temporarily suspend, or terminate your use of the Services if you breach any provision of the User Agreement. This Section does not limit any other remedies that may be available to Multi Vision Financial.</p>

<p><b>7.2. No Agency.</b></p>
<p>Except for the limited purpose stated in the ‘Authority’ Section, you and Multi Vision Financial are independent contractors, and no agency, partnership, joint venture, employee-employer or franchisor-franchisee relationship is intended or created by this User Agreement.</p>

<p><b>7.3. Notices.</b></p>
<p>Except as explicitly stated otherwise, any notices will be given by email to info@multivisionfinancial.com or to the email address you provide to Multi Vision Financial in the Order Confirmation/Welcome Letter (in your case), or such other address as the party will specify. Notice will be deemed given twenty four (24) hours after email is sent, unless the sending party is notified that the email address is invalid. Alternatively, we may give you notice by email, certified mail, postage prepaid and return receipt requested, to the address provided to Multi Vision Financial in the Order Confirmation/Welcome Letter. In such case, notice will be deemed given three (3) days after the date of mailing.</p>

<p><b>7.4. Mandatory Mediation Prior to Litigation.</b></p>
<p>Prior to either party filing any legal complaint and/or legal document in any District Court in accordance with Section 7.5, the parties agree to mediate any dispute arising from this User Agreement. Any mediation that takes place in accordance with this section shall take place in Long Beach, California unless Multi Vision Financial agrees otherwise. In order to initiate the mandatory mediation, the aggrieved party shall provide the non-aggrieved party with notice of its intent to mediate the dispute (the “Notice”). The Notice shall be dated, and in writing, and shall provide sufficient details of the dispute to apprise the other party of the basis of the aggrieved party’s claims. Within ten (10) days of the date on the Notice, (i) the non-aggrieved party shall confirm its receipt of the notice with the aggrieved party, (ii) the parties shall agree upon a length of time for the mediation, which shall be at least one (1) day, but not more than three (3) days, (iii) the parties shall schedule a date for mediation with the aggrieved party, which such mediation shall be not less than thirty (30) days from the date listed on the Notice, but not more than ninety (90) days from the date listed on the Notice, and (iv) the parties shall select a mediator. In the event that the parties cannot agree upon a mediator, each party shall select one name from a list of mediators maintained by any bona fide dispute resolution provider or other private mediator, and the two selected mediators shall then choose a third person who will serve as mediator. The parties acknowledge and agree that any mediated settlement agreement may be converted to a judgment and enforced according to the California Rules of Civil Procedure. The parties agree to share the mediator’s fees equally. In the event that either party hereto fails to cooperate in a reasonable manner in the scheduling and/or facilitation of the mediation contemplated herein, then the cooperating party will have the right to recover from the non-cooperating party its costs and reasonable attorneys fees incurred in connection with any subsequent suit, or other proceeding, including costs, fees, and expenses on appeal.</p>

<p><b>7.5. Governing Law.</b></p>
<p>California law will govern this User Agreement, except for the body of law relating to conflicts of law. Subject to Section 7.4, venue for any legal action will be the federal or state courts of Long Beach, CA. The prevailing party in any litigation in connection with this User Agreement will be entitled to recover from the other party its costs and reasonable attorneys’ fees and other expenses.</p>

<p><b>7.6. Waivers.</b></p>
<p>A party’s failure to enforce any provision of this User Agreement shall not be a waiver of the provision or the right to enforce it at a later time.</p>

<p><b>7.7. Entire Agreement.</b></p>
<p>This User Agreement sets forth the entire understanding and agreement between us with respect to the subject matter hereof. You agree that you are not entering into this User Agreement in reliance on any statements or representations other than those set forth herein. If any provision of this User Agreement is held to be invalid or unenforceable, such provision will be struck and the remaining provisions will be enforced.</p>

<p><b>7.8. Conflict.</b></p>
<p>Should the terms of this User Agreement directly conflict with any terms summarized in the Order Confirmation, the terms of the Order Confirmation shall prevail.</p>
@stop