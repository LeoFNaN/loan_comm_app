@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
	Admin Dashboard :: @parent
@stop

@section('pageTitle')
	<span class="icon icon-dashboard"></span> Admin Dashboard
@stop

{{-- Content --}}
@section('content')

	<h3>Agent Management</h3>

	<div class="page-header">
	
		<p>To enable an Agent's dashboard you must click on the Edit <span class="icon icon-pencil"></span> icon under the actions column below.  Then assign a Role, an Agent and Select Yes to activate then click submit.  The Agent will receive an activation email.</p> 

	</div>

	<table id="users" class="table table-striped table-hover order-column compact">
		<thead>
			<tr>
				<th class="col-md-2">Agent ID</th>
				<th class="col-md-2">Name</th>
				<th class="col-md-2">{{{ Lang::get('admin/users/table.email') }}}</th>
				<th class="col-md-2">Role</th>
				<th class="col-md-2">{{{ Lang::get('admin/users/table.activated') }}}</th>
				<th class="col-md-2">{{{ Lang::get('admin/users/table.created_at') }}}</th>
				<th class="col-md-2">{{{ Lang::get('table.actions') }}}</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

@stop

@section('sidebar')

	<div class="innerSidebar">

		<h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Recent Activity <span class="statusUpdateText"> Last 21 Days</span></h4>

		<ul class="recentActivityUl">

			@forelse($paginator as $recent)
			
				<li class="animated fadeIn">

					 <a title="" href={{ URL::to('admin/client/'.$recent->content_id.'/status') }}>

					 	<p class="activityDetails"> 

					 			@if ($recent->user_id == $userLoggedin->id)

						 			You {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@else

						 			Agent {{ $recent->first_name }} {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@endif

					 	</p>

					 </a>
					 <div style="clear: both;"></div>
				</li>

			@empty

				<li>No activity</li>

			@endforelse

			{{ $paginator->links('pagination::slider-3') }}

		</ul>

	</div><div class="clearfix"></div>

	<div class="innerSidebar">

		<h4><span class="icon icon-drawer2"></span> Management</h4>
					<!-- Split button -->
			<div class="btn-group">

			  <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus-sign"></span>  Add Account</button>

			  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
			    <span class="caret"></span>
			    <span class="sr-only">Toggle Dropdown</span>
			  </button>

				  <ul class="dropdown-menu" role="menu">

				    <li><a href="{{{ URL::to('admin/users/create') }}}" class=""><span class="glyphicon glyphicon-plus-sign"></span> Add Agent</a></li>

				    <li><a style="text-decoration: strikethrough;" href="#" class=""><span class="glyphicon glyphicon-plus-sign"></span> Add Client </a></li>

				  </ul>

			</div>	

	</div>

@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
				oTable = $('#users').dataTable( {
				"sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
				"sPaginationType": "bootstrap",
				"oLanguage": {
					"sLengthMenu": "_MENU_ records per page",
					"sProcessing": "<img src='/assets/img/loader.gif'> processing..."
				},
				"aaSorting": [[5, 'asc']],
			   	"bProcessing": true,
		        	"bServerSide": true,
		        	"sAjaxSource": "{{ URL::to('admin/users/data') }}",
			});
		});
	</script>
@stop