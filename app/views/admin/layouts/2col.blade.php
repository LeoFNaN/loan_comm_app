<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>@section('title')MVF - 
	@show
	</title>

	<meta name="keywords" content="@yield('keywords')" />
	<meta name="author" content="@yield('author')" />
	<meta name="description" content="@yield('description')" />
	<meta name="google-site-verification" content="">

	<!-- Dublin Core Metadata : http://dublincore.org/ -->
	<meta name="DC.title" content="Project Name">
	<meta name="DC.subject" content="@yield('description')">
	<meta name="DC.creator" content="@yield('author')">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">

            <link rel="stylesheet" href="{{asset('bootpaper/bootstrap.min.css')}}">
            <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/prettify.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/bootstrap-wysihtml5.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/datatables-bootstrap.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/colorbox.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/styles.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
            <link rel="stylesheet" href="{{asset('assets/css/icomoon/style.css')}}">

	<style>
                	body {
                	   padding: 60px 0;
                	}
	</style>

	@yield('styles')

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body id="adminUI">

    <div class="container">

            @include('admin.layouts.navigation')

    </div>

    <div class="pageWrap container">

	<div class="col-sm-8"  role="main">

		<h2 class="mainTitle">@yield('pageTitle', 'MVF Agent Center')</h2>

		@include('notifications')

		@yield('content')

	</div>

	<aside role="complementary">

		<div class="col-sm-4 inSide">

			@yield('sidebar')

		</div>

	</aside>

	@yield('beforeFooter')

    </div>

    <footer class="clearfix">    

        <div id="footer">

                <div class="col-sm-8 col-sm-offset-2">

                @include('site.layouts.footernav')

                <div class="clearfix visible-xs-block"></div>
                
                <p class="text-muted credit text-center">
                    &copy; MultiVisionFinancial. All rights reserved. | 235 E Broadway St Suite 940, Long Beach, CA | 888-598-9951
                </p>

                @include('site.layouts.disclaimer')

            </div>

            <a href="#" class="scrollToTop"></a>

        </div>
        
    </footer>

    <!-- Javascripts -->
    <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/wysihtml5/wysihtml5-0.3.0.js')}}"></script>
    <script src="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>
    <script src="{{asset('assets/js/dtables/1.10.4.jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/js/dtables/datatables-bootstrap.js')}}"></script>
    <script src="{{asset('assets/js/dtables/datatables.fnReloadAjax.js')}}"></script>
    <script src="{{asset('assets/js/jquery.colorbox.js')}}"></script>
    <script src="{{asset('assets/js/prettify.js')}}"></script>
    <script src="{{asset('assets/js/scrolltotop.js')}}"></script>
    
    <script type="text/javascript">
    	$('.wysihtml5').wysihtml5();
        $(prettyPrint);
    </script>

    @yield('scripts')

</body>

</html>
