<!-- Navbar -->
<div id="defaultNav" class="navbar navbar-default navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

             <div class="logo">
                <a title="Back to MVF Business Loans" alt="Back to MVF Business Loans" href="https://www.multivisionfinancial.com"><img title="Back to MVF Business Loans" alt="Back to MVF Business Loans" src="{{ asset('assets/img/mvf-logo.jpg') }}" width="160" height="50" /></a>
            </div>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">

    		<ul class="nav navbar-nav user-nav">
    	     		<li{{ (Request::is('/') ? ' class="active"' : '') }}><a title="Home" alt="Home" href="{{{ URL::to('/') }}}"> Home</a></li>  
                            <li {{ (Request::is('contact-us') ? ' class="active"' : '') }}><a title="Contact Us" alt="Contact Us" href="{{{ URL::to('contact-us') }}}">Contact Us</a></li>
    		</ul>

    	<ul class="nav navbar-nav user-nav pull-right">
            
    	       <li class="divider-vertical"></li>

    	       <li class="dropdown">
	    		<a class="dropdown-toggle" data-toggle="dropdown" title="Your Account" alt="Your Account" href="#">
	    			   <span class="glyphicon glyphicon-user"></span> {{{ ucfirst(Auth::user()->first_name) }}} Account <span class="caret"></span>
	    		</a>
                        
                        <ul class="dropdown-menu">

                                <li{{ (Request::is('admin/dashboard*') ? ' class="active"' : '') }}><a title="Dashboard" alt="Dashboard" href="{{{ URL::to('admin/dashboard') }}}"><span class="icon icon-dashboard"></span> Dashboard</a></li>

                                <li><a title="Settings" alt="Settings" href="{{{ URL::to('user/profile/') }}}"><span class="glyphicon glyphicon-cog"></span> Settings</a></li>

                            </ul>
                
    		</li>           

                    <li class="dropdown{{ (Request::is('admin/blogs*|admin/comments*') ? ' class="active"' : '') }}">
                        <a class="dropdown-toggle" data-toggle="dropdown" title="News" alt="News" href="{{{ URL::to('admin/blogs') }}}">
                            <span class="glyphicon glyphicon-list-alt"></span> News 
                            <span class="caret"></span></a>

                        <ul class="dropdown-menu">
                            <li {{ (Request::is('admin/blogs') ? ' class="active"' : '') }}>
                                <a title="Manage News" alt="Manage News" href="{{{ URL::to('admin/blogs') }}}"><span class="glyphicon glyphicon-list-alt"></span> Manage News</a>
                            </li>

                            <li {{ (Request::is('admin/comments') ? ' class="active"' : '') }}>
                                <a title="Manage Comments" alt="Manage Comments" href="{{{ URL::to('admin/comments') }}}"><span class="glyphicon glyphicon-bullhorn"></span> Manage Comments</a>
                            </li>

                        </ul>
                    </li>

                    <li><a title="Logout" alt="Logout" href="{{{ URL::to('user/logout') }}}"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>

    		</ul>
    	</div>
        </div>
</div>