@extends('admin.layouts.default')

@section('pageTitle')
Delete Confirmation
@stop

{{-- Content --}}
@section('content')

    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-general" data-toggle="tab">Delete: {{ $user->first_name.' '.$user->last_name }}</a></li>
    </ul>

    <form id="deleteForm" class="form-horizontal" method="post" action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/delete') }}@endif" autocomplete="off">

        {{ Form::token(); }}
        <input type="hidden" name="id" value="{{ $user->id }}" />

        <div class="form-group">
            <div class="controls">
                <button style="margin-left: 15px;" type="submit" class="btn btn-danger">Confirm Delete</button>
                <a  style="margin-left: 10px;" class="btn btn-default" href="{{{ URL::to('admin/dashboard') }}}">Cancel</a>
            </div>
        </div>

    </form>
    
@stop