@extends('admin.layouts.default')

@section('title')
    Create Agent
@stop

@section('pageTitle')
    <span class="glyphicon glyphicon-plus-sign"></span> Create New Agent
@stop

@section('content')

<!-- Tabs -->
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
    <li><a href="#tab-agent" data-toggle="tab">Agent Assignment</a></li>
</ul>

<form class="form-horizontal" method="post" action="@if (isset($user)){{ URL::to('admin/users/create') }}@endif" autocomplete="off">
    
    {{ Form::token(); }}

    <!-- Tabs Content -->
    <div class="tab-content">

        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">

        <div class="form-group {{{ $errors->has('office_id') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="office_id">Agent ID</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="office_id" disabled="disabled" id="office_id" value="Generated After Creation" />
            </div>
        </div>

        <div class="form-group {{{ $errors->has('first_name') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="first_name">First Name</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="first_name" id="first_name" value="{{{ Input::old('first_name', isset($user) ? $user->first_name : null) }}}" />
                {{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('last_name') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="last_name">Last Name</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="last_name" id="last_name" value="{{{ Input::old('last_name', isset($user) ? $user->last_name : null) }}}" />
                {{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
        <label class="col-md-2 control-label" for="email">Email</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="email" id="email" value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
                {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('phone_number') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="phone_number">Phone Number</label>
            <div class="col-md-10">
                <input class="form-control" type="text" name="phone_number" id="phone_number" value="{{{ Input::old('phone_number', isset($user) ? $user->phone_number : null) }}}" />
                {{ $errors->first('phone_number', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="password">Password</label>
            <div class="col-md-10">
                <input class="form-control" type="password" name="password" id="password" value="" />
                {{ $errors->first('password', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="password_confirmation">Password Confirm</label>
            <div class="col-md-10">
                <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="" />
                {{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('activated') || $errors->has('confirm') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="confirm">Activate User?</label>
            <div class="col-md-6">
                @if ($mode == 'create')
                    <select class="form-control" name="confirm" id="confirm">
                        <option value="1"{{{ (Input::old('confirm', 0) === 1 ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
                        <option value="0"{{{ (Input::old('confirm', 0) === 0 ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
                    </select>
                @else
                    <select class="form-control" {{{ ($user->id === Confide::user()->id ? ' disabled="disabled"' : '') }}} name="confirm" id="confirm">
                        <option value="1"{{{ ($user->confirmed ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
                        <option value="0"{{{ ( ! $user->confirmed ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
                    </select>
                @endif
                {{ $errors->first('confirm', '<span class="help-inline">:message</span>') }}
            </div>
        </div>

        <div class="form-group {{{ $errors->has('roles') ? 'error' : '' }}}">
            <label class="col-md-2 control-label" for="roles">Roles</label>
            <div class="col-md-6">
                <select class="form-control" name="roles[]" id="roles[]" multiple>
                @foreach ($roles as $role)
                    @if ($mode == 'create')
                        <option value="{{{ $role->id }}}"{{{ ( in_array($role->id, $selectedRoles) ? ' selected="selected"' : '') }}}>{{{ ucFirst($role->name) }}}</option>
                    @else
                        <option value="{{{ $role->id }}}"{{{ ( array_search($role->id, $user->currentRoleIds()) !== false && array_search($role->id, $user->currentRoleIds()) >= 0 ? ' selected="selected"' : '') }}}>{{{ ucFirst($role->name) }}}</option>
                    @endif
                @endforeach
                </select>

                <span class="help-block">
                    Select a group to assign to the user, remember that a user takes on the permissions of the group they are assigned.
                </span>
            </div>
        </div>


        </div>
        <!-- ./ general tab -->

        <div class="tab-pane" id="tab-agent">
            <!-- Assign To -->
            <div style="margin: 10px 0 10px 0;">

                <div class="form-group {{{ $errors->has('roles') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="assignment">Assigned To Agent</label>
                    <div class="col-md-6">
                        {{ Form::text('agentName', $value = null , $attributes = ['class' => 'form-control', 'id' => 'myid', 'disabled' ,'placeholder' => 'Created by Administrator' ]) }}
                    </div>
                </div>

            </div>

            <div class="form-group {{{ $errors->has('roles') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="assignment">Assign Inhouse Agent </label>
                <div class="col-md-6">

                    <select class="form-control" name="assignment">
                            <option value="Select" selected="selected">Select Agent</option>
                        @foreach ($inhouseAgents as $inhouseAgent)
                            <option value="{{ $inhouseAgent->user_id }}">{{ ucfirst($inhouseAgent->first_name) }}</option>
                        @endforeach
                    </select>

                    <span class="help-block">
                        Select an inhouse agent to assign to the user.
                    </span>

                </div>
                
            </div>


        </div>


    </div>
    <!-- ./ tabs content -->

    <!-- Form Actions -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn btn-success">Create</button>
                <a class="btn btn-default" href="{{{ URL::to('admin/dashboard') }}}">Back</a>
            <button type="reset" class="btn btn-default" style="float: right;">Reset</button>
        </div>
    </div>

</form>
@stop