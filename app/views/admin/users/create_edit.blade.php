@extends('admin.layouts.2col')

@section('title')
	{{ ucfirst($user->first_name) }} - User Profile
@stop

@section('pageTitle')
	Edit {{ $assignedTo == 'inhouse' ? 'Inhouse' : 'Agent' }} Profile
@stop

@section('content')

	<p class="editAgentDetails pull-right"><span>Agent ID:</span> {{ $user->office_id or "N/A" }} | <span>Registered:</span> {{ Carbon::parse($user->created_at)->format('m-d-y') }}</p>

	<!-- Tabs -->
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
		@if($assignedTo == "inhouse")
			
		@else
			<li><a href="#tab-agent" data-toggle="tab">Agent Assignment</a></li>
		@endif
	</ul>

	<form class="form-horizontal" method="post" action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/edit') }}@endif" autocomplete="off">
		
		{{ Form::token(); }}

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">


				<div class="form-group {{{ $errors->has('first_name') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="first_name">First Name</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="first_name" id="first_name" value="{{{ Input::old('first_name', isset($user) ? $user->first_name : null) }}}" />
						{{ $errors->first('first_name', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<div class="form-group {{{ $errors->has('last_name') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="last_name">Last Name</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="last_name" id="last_name" value="{{{ Input::old('last_name', isset($user) ? $user->last_name : null) }}}" />
						{{ $errors->first('last_name', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<!-- Email -->
				<div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="email">Email</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="email" id="email" value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
						{{ $errors->first('email', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<div class="form-group {{{ $errors->has('phone_number') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="phone_number">Phone Number</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="phone_number" id="phone_number" value="{{{ Input::old('phone_number', isset($user) ? $user->phone_number : null) }}}" />
						{{ $errors->first('phone_number', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<!-- Password -->
				<div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="password">Password</label>
					<div class="col-md-10">
						<input class="form-control" type="password" name="password" id="password" value="" />
						{{ $errors->first('password', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<!-- Password Confirm -->
				<div class="form-group {{{ $errors->has('password_confirmation') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="password_confirmation">Password Confirm</label>
					<div class="col-md-10">
						<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="" />
						{{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<!-- Activation Status -->
				<div class="form-group {{{ $errors->has('activated') || $errors->has('confirm') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="confirm">Activate User?</label>
					<div class="col-md-10">
						@if ($mode == 'create')
							<select class="form-control" name="confirm" id="confirm">
								<option value="1"{{{ (Input::old('confirm', 0) === 1 ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
								<option value="0"{{{ (Input::old('confirm', 0) === 0 ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
							</select>
						@else
							<select class="form-control" {{{ ($user->id === Confide::user()->id ? ' disabled="disabled"' : '') }}} name="confirm" id="confirm">
								<option value="1"{{{ ($user->confirmed ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
								<option value="0"{{{ ( ! $user->confirmed ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
							</select>
						@endif
						{{ $errors->first('confirm', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<!-- Groups -->
			<div class="form-group {{{ $errors->has('roles') ? 'error' : '' }}}">
	                <label class="col-md-2 control-label" for="roles">Roles</label>
		                <div class="col-md-10">
			                <select class="form-control" name="roles[]" id="roles[]" multiple>
			                        @foreach ($roles as $role)
										@if ($mode == 'create')
			                        		<option value="{{{ $role->id }}}"{{{ ( in_array($role->id, $selectedRoles) ? ' selected="selected"' : '') }}}>{{{ $role->name }}}</option>
			                        	@else
									<option value="{{{ $role->id }}}"{{{ ( array_search($role->id, $user->currentRoleIds()) !== false && array_search($role->id, $user->currentRoleIds()) >= 0 ? ' selected="selected"' : '') }}}>{{{ $role->name }}}</option>
								@endif
			                        @endforeach
							</select>

							<span class="help-block">
								Select a group to assign to the user, remember that a user takes on the permissions of the group they are assigned.
							</span>
		            	</div>
			</div>

		</div>


		@if($assignedTo == "inhouse")

		@else

			<div class="tab-pane" id="tab-agent">

				<div style="margin: 10px 0 10px 0;">

						<div class="form-group {{{ $errors->has('roles') ? 'error' : '' }}}">
							<label class="col-md-2 control-label" for="assignment">Referred By </label>
								<div class="col-md-10">
									{{ Form::text('referral_source', $value = null, $attributes = ['class' => 'form-control', 'id' => 'myid', 'disabled' ,'placeholder' => ucfirst($user->referral_source) ]) }}
								</div>
						</div>

						@if(count($assignedTo) == "1")

							<div class="form-group {{{ $errors->has('roles') ? 'error' : '' }}}">
							<label class="col-md-2 control-label" for="assignment">Assigned To </label>

								<div class="col-md-10">
									{{ Form::text('agentName', $value = null, $attributes = ['class' => 'form-control', 'id' => 'myid', 'disabled' ,'placeholder' => ucfirst($assignedTo->fullName) ]) }}

								</div>
							</div>

						@else

							<div class="form-group {{{ $errors->has('roles') ? 'error' : '' }}}">
							<label class="col-md-2 control-label" for="assignment">Assigned To </label>
								<div class="col-md-10">
									{{ Form::text('agentName', $value = null , $attributes = ['class' => 'form-control', 'id' => 'myid', 'disabled' ,'placeholder' => 'This user is not assigned to an Inhouse Agent' ]) }}
								</div>
							</div>

						@endif 

				</div>

				<div class="form-group {{{ $errors->has('roles') ? 'error' : '' }}}">
	                		<label class="col-md-2 control-label" for="assignment">Assign Inhouse </label>
			                <div class="col-md-10">
	  						
	  					<select class="form-control" name="assignment">
			                		<option value="Select" selected="selected">Select Agent</option>
			                		@foreach ($inhouseAgents as $inhouseAgent)
								<option value="{{ $inhouseAgent->id }}">{{ ucfirst($inhouseAgent->fullName) }}</option>
							@endforeach
			                	</select>

						<span class="help-block">
							Select an inhouse agent to assign to user.
						</span>

			            	</div>
				</div>


			</div>
		@endif

		</div>


		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<button type="submit" class="btn btn-success">Submit</button>
				<a class="btn btn-default" href="{{{ URL::to('admin/dashboard') }}}">Back</a>
				<button type="reset" class="btn btn-default" style="float: right;">Reset</button>
			</div>
		</div>

	</form>
@stop

@if($assignedTo == "inhouse")

@else
	@section('sidebar')
		<div class="innerSidebar">
			
			<h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Lead Report</h4>

			<div class="col-sm-6" style="text-align: center;">
				<p class="leadCount">{{{ $leadReport->gross_count }}}</p>
				<p class="leadLabel">Total Leads</p>
			</div>

			<div class="col-sm-6" style="text-align: center; ">
				<p class="leadCount">{{{ $leadReport->active_count }}}</p>
				<p class="leadLabel">Active Leads</p>
			</div>

			<div style="clear: both; "></div>

			<div class="" style="text-align: center; margin-top: 30px;">
				<p class="leadCount">{{{ $leadReport->process_rate }}}%</p>
				<p class="leadLabel">Process Rate</p>
			</div>

			<div style="text-align: center; margin-top: 30px;">
				<p class="leadCount">{{{ $leadReport->funded_rate }}}%</p>
				<p class="leadLabel">Funded Rate</p>
			</div>

			<div style="text-align: center; margin-top: 30px;">
				<p class="leadCount">{{{ $leadReport->failure_rate }}}%</p>
				<p class="leadLabel">Failure Rate</p>
			</div>

			<p class="text-center" style="margin-top: 20px;"><a href="{{ route('showListOfClientsAdmin', $user->id) }}" class="btn btn-sm btn-info">View Clients</a></p>

		</div>
	@stop

@endif
