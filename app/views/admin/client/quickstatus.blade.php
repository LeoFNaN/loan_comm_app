@extends('site.layouts.modal')

{{-- Web site Title --}}
@section('title')
	Quick Update - {{ $clientObj->first_name.' '.$clientObj->last_name }}
@stop

{{-- Content --}}
@section('content')

	@if ( ! Auth::check())

		Login <a href="{{{ URL::to('user/login') }}}" title="Login">here</a> to add a comment.<br /><br />
			
	@elseif ( ! $canComment )

		You don't have the correct permissions to add comments.

	@else

		<div class="statusActions quickStatusUpdates">

			@if( ! $currentStatus)

				<strong>Current Status: </strong> Queued In System <br/>
				<strong>Current Comment:</strong> No Comments

			@else

				<strong>Current Status: </strong>{{ $currentStatus->StatusAction->name }} <br/>
				<strong>Current Comment:</strong> {{ $currentStatus->status_content.' by '. $currentStatus->user->first_name .' '. $currentStatus->user->last_name }}

			@endif

			<h5><span class="glyphicon glyphicon-time"></span> Set Status</h5>

		{{ Form::open(array('url' => array('admin/client/'.$clientObj->id.'/quickstatus'))) }}

			<div class="form-group">

				<div class="actionList">
					{{ Form::select('status_action', array('select' => 'Select One', '21' => 'Queued In System', '5' => 'Comment Only') + $allStages, Input::old('status_action', (isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->id : '21')), ['class' => 'form-control', 'id' => 'sel1']) }}
				</div>

				<div class="pull-right noMargin">
					{{ Form::checkbox('notify_agent', 'Yes', true) }} Notify agent of status update/comment
				</div>
				
			</div><div class="clearfix"></div>

		<h5 style="padding-top: 10px;"><span class="icon icon-pencil"></span> Add Comment</h5>

			@if($errors->has())
				<div class="alert alert-danger alert-block">
					<ul>
			@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
			@endforeach
					</ul>
				</div>
			@endif

		<textarea class="input-block-level" rows="4" style="width: 100%; padding: 5px;" name="status_content" id="status_content">{{{Request::old('status_content') }}}</textarea>

			<div class="form-group">

				<div class="col-md-12" style="margin: 10px 0 0 0; padding: 0;">

					<input type="submit" class="btn btn-success" id="submit" value="Submit Update" />
					<element class="btn-cancel close_popup pull-right" style="cursor: pointer;">Cancel</element>

				</div>

			</div>

		</form>
		
	@endif
@stop
