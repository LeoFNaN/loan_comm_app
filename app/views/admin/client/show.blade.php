@extends('site.layouts.dash')

{{-- Web site Title --}}
@section('title')
	{{{ $title }}} 
@stop

@section('pageTitle')
  <span class="glyphicon glyphicon-list"></span> Clients from {{ $userAgent->first_name.' '.$userAgent->last_name }}
@stop

{{-- Content --}}
@section('content')

      <div class="filterLabel">Filter</div>

      <div class="filtersBox">

        {{ Form::select('status_action', array('All' => 'All Statuses', 'Queued In System' => 'Queued In System') + $allStages, Input::old('status_action', (isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->id : '21')), ['id' => 'statusSelect']) }}

        <button id="clearFilter" class="pull-right btn btn-default btn-sm">Clear Filter</button>

  </div>

    <table id="clients" class="table table-striped table-hover">
    <thead>
      <tr>
                  <th class="col-md-2">Name</th>
                  <th class="col-md-2">Email</th>
                  <th class="col-md-2">Added</th>
                  <th class="col-md-2">Updated</th>
                  <th class="col-md-2">Current Status</th>
                  <th class="col-md-2">Actions</th>
      </tr>
    </thead>
      <tbody>
      </tbody>
  </table>
   
@stop

@section('sidebar')

  <div class="innerSidebar">

      <h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Recent Activity <span class="statusUpdateText"> Last 3 Months</span></h4>
      
      <ul class="recentActivityUl">

      		@forelse($paginator as $recent)
			
				<li class="animated fadeIn">

					 <a title="" href={{ URL::to('admin/client/'.$recent->content_id.'/status') }}>

					 	<p class="activityDetails"> 

					 			@if ($recent->user_id == $userLoggedin->id)

						 			You {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@else

						 			Agent {{ $recent->first_name }} {{ $recent->details }} <span class="activityDate">{{{ Carbon::parse($recent->created_at)->diffForHumans() }}} </span>

						 		@endif

					 	</p>

					 </a>
					 <div style="clear: both;"></div>
				</li>

			@empty

				<li>No activity</li>

			@endforelse

			{{ $paginator->links('pagination::slider-3') }}

      </ul>
</div><div class="clearfix"></div>

<div class="innerSidebar">

      <h4 class="sidebarH4"><span class="glyphicon glyphicon-time"></span> Agent  </h4>

      <div class="dashSideAgent" >

        <p><strong>Agent:</strong> {{ $userAgent->first_name.' '.$userAgent->last_name }} </p>
        <p><strong>ID: </strong>{{ $userAgent->office_id }}</p>
        <p><strong>Phone: </strong> {{ $userAgent->phone_number }} </p>
        <p><strong>Email: </strong> {{ $userAgent->email }} </p>
        @if($agent == '')
          <p><strong>Assigned To:</strong> Unassigned</p>
        @else
          <p><strong>Assigned To: </strong>{{ ($agent->first_name.' '.$agent->last_name) }}</p>
        @endif

      </div>

</div>

@stop

{{-- Scripts --}}
@section('scripts')

	<script>
        	var action = "{{ URL::action('AdminUsersController@getListData', $userAgent->id) }}";
	</script>

	{{ HTML::script('assets/js/dtables/dtable.listclients.js') }}

@stop