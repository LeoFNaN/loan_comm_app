@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	Client Profile Page - Status Updates
@stop

@section('pageTitle')
	<span class="glyphicon glyphicon-user"></span> Client Profile <span class="topCurrentStatus pull-right label">Status: {{{ isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->name : 'Queued In System' }}} </span>
@stop

{{-- Content --}}
@section('content')

<div class="col-sm-12 clientProfile">		
	<h5><span class="icon icon-user"></span> Agent Information</h5>

		<div class="text-center">

			<p><a title="Edit Agent" alt="Edit Agent" href="{{ URL::to('admin/users/' . $clientObj->user->id . '/edit' ) }}" >{{ $clientObj->user->first_name.' '.$clientObj->user->last_name }}</a> ({{ $clientObj->user->office_id }}) | {{ $clientObj->user->phone_number }} | {{  HTML::mailto($clientObj->user->email,  $clientObj->user->email) }} </p>
		</div>
</div>

	<div class="col-sm-4 clientProfile">
		<h5><span class="glyphicon glyphicon-th-list"></span> Details</h5>

			<ul class="clientDetailList">

				<li>
					<span>Name:</span> <p>{{ $clientObj->first_name.' '.$clientObj->last_name }}</p>
				</li>

				<li>
					<span>Phone:</span> <p>{{ $clientObj->phone }} </p>
				</li>

				<li>
					<span>Email:</span> 
					<p>@if($clientObj->email == "") Not Available @else {{ HTML::mailto($clientObj->email . '?subject=Hello ' . $clientObj->first_name, $clientObj->email) }} @endif</p>
				</li>

				<li><span>Business Name:</span> 
					<p>@if($clientObj->business_name == "") N/A @else {{ $clientObj->business_name }} @endif</p>
				</li>

				<li><span>Loan Amount:</span> 
					<p>@if($clientObj->loan_amount == "") N/A @else {{ $clientObj->loan_amount }} @endif</p>
				</li>

				<li>
					<span>State:</span> 
					<p>@if($clientObj->state == "" || $clientObj->state == "Select One") N/A @else {{ $clientObj->state }} @endif</p>
				</li>

				<li>
					<span>Years In Business:</span> 
					<p>@if($clientObj->business_years == "") N/A @else {{ $clientObj->business_years }} @endif</p>
				</li>

				<li>
					<span>Monthly Revenue:</span> 
					<p>@if($clientObj->monthly_revenue == "") N/A @else {{ $clientObj->monthly_revenue }} @endif</p>
				</li>

				<li>
					<span>Contact Time:</span> 
					<p>@if($clientObj->callback_time == "" || $clientObj->callback_time == "Select One") N/A @else {{ $clientObj->callback_time }} @endif</p>
				</li>
				
				<li>
					<span>Message/Special Instructions:</span> <p>@if($clientObj->client_message == "") N/A @else {{ $clientObj->client_message }} @endif</p>
				</li> 

			</ul>
			
	</div>

	<div class="col-sm-8 statusUpdates">

		<h5><span class="glyphicon glyphicon-comment"></span> @if($statusUpdates->count()) Status Updates <span class="statusCount">{{ $statusUpdates->getTotal() }}</span> @else Status Updates <span class="statusCount">0</span>  @endif <span class="statusUpdateText"> Recent Update First</span></h5>

		<ul class="statusList">

			@if($statusUpdates->count())

				<?php $evenOdd = 0; ?>

				@foreach($statusUpdates as $status)

					<li>
						
						<p class="status @if($evenOdd == 0) animated flash firstStatus @endif @if($evenOdd % 2 == 0) evenRow @else oddRow @endif">
							{{ $status->status_content }}
						</p>

						<p class="statusMeta @if($evenOdd == 0) animated flash firstStatus @endif  @if($evenOdd % 2 == 0) evenMeta @else oddMeta @endif">
							<span class="glyphicon glyphicon-comment"></span> <span class="statusMetaSpan">By:</span> {{ $status->first_name.' '.$status->last_name }} | <span class="statusMetaSpan">Date:</span> {{{ Carbon::parse($status->created_at)->format('m-d-y') }}}   <span class="statusMetaSpan">at</span> {{{ Carbon::parse($status->created_at)->format('h:i a') }}} | <span class="commentsCurrentStatus">Status: {{{ isset($status->name) ? $status->name : 'Queued In System' }}} </span>
						</p>
						
					</li>

					<?php $evenOdd++; ?>
				@endforeach

			@else 

				No Updates

			@endif
			
			{{ $statusUpdates->links() }}
			<div class="clearfix "></div>
		</ul>

	@if ( ! Auth::check())
			You need to be logged in to add comments.<br /><br />
			Click <a href="{{{ URL::to('user/login') }}}">here</a> to login into your account.
		@elseif ( ! $canComment )
			You don't have the correct permissions to add comments.
		@else

		@if($errors->has())
			<div class="alert alert-danger alert-block">
				<ul>
		@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
		@endforeach
				</ul>
			</div>
		@endif

		<div class="statusActions" style="margin: 10px 0;">

			<h5><span class="glyphicon glyphicon-time"></span> Set Status</h5>

			{{ Form::open(array('url' => array('admin/client/'.$clientObj->id.'/status'))) }}

				<div class="form-group">
					
					<div class="actionList">
						{{ Form::select('status_action', array('select' => 'Select One', '21' => 'Queued In System', '5' => 'Comment Only') + $allStages, Input::old('status_action', (isset($currentStatus->StatusAction->name) ? $currentStatus->StatusAction->id : '21')), ['class' => 'form-control', 'id' => 'sel1']) }}
					</div>

					<div class="pull-right noMargin">
						{{ Form::checkbox('notify_agent', 'Yes', true) }} Notify agent of status update/comment
					</div>

				</div>

		</div>

		<div class="clearfix"></div>

		<!-- Reference views/site/blog/view_post.blade.php -->
		<h5 style="padding-top: 5px;"><span class="icon icon-pencil"></span> Add Comment</h5>

		{{ Form::open(array('url' => array('admin/client/'.$clientObj->id.'/status')  )) }}

			<textarea class="col-md-12 input-block-level" style="border: thin solid #ccc;" rows="4" name="status_content" id="status_content">{{{ Request::old('status_content') }}}</textarea>

			<div class="form-group">
				<div class="col-md-12" style="margin: 10px 0 0 0; padding: 0;">
					<input type="submit" class="btn btn-success" id="submit" value="Submit Update" />

			          	<div class="pull-right">

			               	<a class="btn btn-default" href="{{{ URL::route('showListOfClientsAdmin', array('client' => $clientObj->user->id )) }}}">Go Back to Client List</a>

			          	</div>

				</div>
			</div>

		</form>
		
	@endif

	</div>

@stop