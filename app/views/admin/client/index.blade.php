@extends('admin.layouts.modal')

{{-- Web site Title --}}
@section('title')
	{{{ $title }}} 
@stop

{{-- Content --}}
@section('content')
	  <ul>
       @foreach($user->clients as $client)
           <li>
                Name: {{ $client->first_name }} {{ $client->last_name;}} - <a href="{{{ URL::to('admin/client/'.$client->id.'/status') }}}" class="iframe btn btn-xs btn-default">View Status</a> <br/>
                 <span class="timeStamp" style="font-weight: bold; font-size: .8em;">created:  {{ $client->created_at}} </span> <br/><br/>
             </li>
		@endforeach
    </ul>


@stop