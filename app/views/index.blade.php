@extends('site.layouts.default')

@section('title')
    MVFLoans - Business Loan Processing Center 
@stop

@section('page-title')
    Business Loan Processing Center
@stop

@section('content')

    <div id="homeWrap">

        <div class="col-sm-4 hidden-xs">

            <a title="MVF Agent Processing Center" alt="MVF Agent" href="/"><img class="img-responsive" title="MVF Agent" alt="MVF Agent" src="{{ URL::asset('assets/img/lcsGirl.jpg') }}" width="189" height="694" /></a>

            <img class="img-responsive" style="margin-top: 10px;" title="Fast Loan Process" alt="Fast Loan Process" src="{{ URL::asset('assets/img/fast_loan_process.jpg') }}" width="185" height="135" />

            <img class="img-responsive" style="margin-top: 15px;" title="More Loan Options" alt="More Loan Options" src="{{ URL::asset('assets/img/more-options.jpg') }}" width="185" height="135" />

            <a title="Client Tracking Dashboard - Sign Up" alt="Client Tracking Dashboard - Sign Up" href="{{{ URL::to('/signup') }}}"><img class="img-responsive" style="margin-top: 15px;" title="Client Tracking Dashboard - Sign Up" alt="Client Tracking Dashboard - Sign Up" src="{{ URL::asset('assets/img/client-tracker-dashboard.jpg') }}" width="190" height="190" /></a>

            <img class="img-responsive" style="margin-top: 15px;" title="MVF Headquaters" alt="MVF Headquaters" src="{{ URL::asset('assets/img/left-side-officepic.jpg') }}" width="190" height="350" />

            <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>

            <div class="fb-like" style="margin: 20px 0 0 15px;" data-href="https://www.facebook.com/MultiVisionFinancial" data-layout="standard" data-action="like" data-show-faces="false" data-share="false"></div>

        </div>

        <div class="col-sm-8">

            <h3 class="animated fadeIn" >You Refer Business Owners</h3>

            <div class="visible-xs" style="float: left;">
                
                <img title="Multi Vision Financial" alt="Multi Vision Financial" src="{{ URL::asset('assets/img/lcsGirl.jpg') }}" width="90" height="300" />

            </div> 

            <p class="subHeader">We Do All The Processing &amp; Paperwork</p>

            <p>Our referral agents are earning large amounts of additional income with our leading pay structure. *It's as simple as submitting us an interested business owner lead.</p> 

            <ul class="homeTopList">
                <li>No pyramid schemes</li>
                <li>No bad products to sell</li>
            </ul>

            <p style="clear: both;">Are you an ISO (Independent Sales Organization), merchant sales professional, online marketing person, lead generation expert, business consultant or a broker?  Do you know business owners that need a loan? Do you have access to a network that produces qualified referrals looking for business financing? Then join our Referral Program and earn money from your business relationships. </p>        

            <p style="text-align: center;"> <span style="font-weight: bold; color:
             #ff0000; ">It's Free!</span> <a href="{{ URL::to('/signup') }}" title="Sign Up!" alt="Get Started Today!">Click to sign up</a> or call <strong> 888-598-9951.</strong></p>

            <h3 class="animated fadeIn">You Get Complete Support</h3>

            <ul>
                <li>Currently over 400+ referring agents and agencies</li>
                <li>We never charge your clients any upfront fees</li>
                <li>No sales required, we close the deal for you</li>
                <li>Your own inhouse funding manager</li>
                <li>Over $100M already funded</li>
                <li>No sign up fees or charges to you</li>
            </ul>

            <h3 class="animated fadeIn">Your MVF Team Benefits</h3>

            <ul>
                <li>Track every client through dashboard</li>
                <li>Easy to submit clients</li>
                <li>Inhouse agent gather/receive documents directly from client</li>
                <li>When approved, we will send loan contract to client </li>
                <li>Automatic renewal commissions* </li>
                <li>Most deals funded in 72 hours, you get paid fast </li>
            </ul>

            <h3 class="animated fadeIn">We Know Business Loans</h3>
            <p>Our mission is to empower Financial Professionals by providing complete solutions for their clients business loan needs.  Our friendly team of professionals is dedicated to processing, selling and funding business loans. We build long term relationships based on integrity, honesty and understanding every client's business goals. We treat every client with the utmost care and respect, as it is a direct reflection on our referral agents and our company values.</p>

            <h3 class="animated fadeIn">We Offer Clear Advantages</h3>

            <p>Are you a frustrated business professional chasing paperwork, soliciting lenders, receiving loan turn downs and having deals that can't fund? Sound familiar? Furthermore, do you wonder why the power brokers get better offers and fund more deals? It's simple, they have the relationships and finances to back their clients. We have those relationships and roles defined, and as a MVF - Referral Agent our "Team" approach works to your advantage. You focus on lead generation/prospecting and we will handle 100% of the backend processing.  <strong>You will appreciate that the banks seek our book of business.</strong></p>

            <p>We understand how time consuming the sales and funding cycles of a client can be.  Time is money and your time is precious.  Bank turn downs alone can be detrimental and demoralizing to both you and the client. In many cases we will get tough deals funded while providing more options and at lower rates. The best part, we not only understand the business, we have a lucrative compensation plan and pay excellent commissions.</p>

            <div class="visible-xs" style="float: left; margin-right: 10px;"><img style="margin-top: 5px;" class="sidebarImg" src="{{ URL::asset('assets/img/office-interior-1.jpg') }}" title="MVF Office in Long Beach"></div>

            <p>
                More and more business owners are looking for money as banks and traditional lenders continue to reduce the lines of credit. Referring clients to MVF is a simple, streamlined process. <strong>Our business funding relationships offer clear advantages to your clients in terms of flexible pricing and fast approvals</strong>.  
            </p>

            @if( ! Auth::check())

            @else

                <div class="text-center">
                    @if (Auth::user()->hasRole('admin'))    

                        <a title="Access Your Dashboard" alt="Access Your Dashboard" class="btn btn-primary" href="{{ URL::route('adminDashboard') }}">Access Dashboard</a>

                    @elseif (Auth::user()->hasRole('Agent'))

                        <a title="Access Your Dashboard" alt="Access Your Dashboard" class="btn btn-primary" href="{{ URL::route('agentDashboard') }}">Access Dashboard</a>

                    @elseif ( Auth::user()->hasRole('Inhouse')) 

                        <a title="Access Your Dashboard" alt="Access Your Dashboard" class="btn btn-primary" href="{{ URL::route('inhouseDashboard') }}">Access Dashboard</a>

                    @endif

                </div>

            @endif
        </div>

    </div>
@stop

@section('sidebar')

    <div class="contactTop">
        <h4>Request Information</h4><img src="{{ URL::asset('/assets/img/form-arrow.png') }}" width="" height="" />
    </div>

        <div id="form-wrap" class="homeForm">

        @if($errors->has())
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {{ Form::open(array('url' => '/')) }}

                <fieldset>

                     <div class="form-group">
                        {{ Form::label('name', 'Name *') }}
                        {{ Form::text('name', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('email', 'Email *') }} 
                        {{ Form::email('email', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('phone', 'Phone') }} 
                        {{ Form::text('phone', null, array('class' => 'form-control')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('message', 'Questions/Message *') }} 
                        {{ Form::textarea('message', null, array('class' => 'form-control contactTextArea')) }}
                    </div>

                    <div class="text-center col-sm-12" style="margin-top: 10px;">
                        {{ Form::submit('Submit', array('class' => 'btn btn-primary', 'style' => 'width: 200px;')) }}
                    </div>

                </fieldset>

            {{ Form::close() }}

                <p class="text-center notShared">
                    *Your Information will not be shared nor sold to anyone. <br/><a href="{{ URL::to('/privacypolicy') }}" style="color:#0000ff;">Privacy Policy</a>
                </p>

        </div>

        <div class="col-sm-12 text-center" style="margin-top: 20px; margin-bottom: 20px;">
            <a class="btn btn-success-bright" style="width: 200px;" href="{{ URL::to('/signup') }}" title="Signup Today!" alt="Signup Today!" >or Sign Up Today!</a>
        </div>

        <img class="sidebarImg img-responsive" src="{{ URL::asset('assets/img/businessCapitalBox.jpg') }}" title="More Opportunities">

        <img class="sidebarImg img-responsive" src="{{ URL::asset('assets/img/flag-operations.jpg') }}" title="USA and Canada">

        <a href="{{ URL('http://www.bbb.org/sanjose/business-reviews/financial-consultants-no-license-required/multi-vision-financial-in-long-beach-ca-100107285') }}"><img class="sidebarImg img-responsive" src="{{ URL::asset('assets/img/bbbA.jpg') }}" title="BBB Accredited"></a>

        <h3 style="font-size: 1.25em; font-weight: bold; border-bottom: 2px solid #5BB0FF; width: 92%;">Testimonial</h3>
        <blockquote style="color: #0981EF;">
            "I recommend working with Multi Vision Financial.  These guys are easy to work with and always find my clients money.  They also pay great commissions for doing absolutely no work at all.  I have been referring clients for almost 2 years, thanks MVF!" ~Dane R
        </blockquote>

        <img class="sidebarImg img-responsive hidden-xs" src="{{ URL::asset('assets/img/office-interior-1.jpg') }}" title="MVF Office in Long Beach">

        <p class="text-center"><script type='text/javascript' src='https://sealserver.trustwave.com/seal.js?style=normal'></script></p>

@stop