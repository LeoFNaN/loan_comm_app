{{ HTML::image('assets/img/email/mvf-logo-e.jpg', 'MVF Logo', array('id' => 'MVFLogo')) }}

<h2>Welcome {{ $user->first_name }},</h2>

<p>Thank you for your interest in becoming a Referral Agent for Multi Vision Financial. Please follow these instructions to complete your sign up process. </p>

<p>This link (<a title="Referral Agent Contract Forms" alt="Referral Agent Contract Forms" href="https://www.dropbox.com/sh/resg642brm4iixg/AAAlH4yg1YTUN4i1zp4U954Xa">click here</a>) has 3 forms that need to be completed and signed:</p>

<ol>    
	<li> Referral Agent Contract &amp; Comp Plan</li>
	<li> Referral Agent Role</li>
	<li> W9 Forms</li>
</ol>

<p>Download and complete the 3 forms. Once filled out, fax back to <em>562-590-9393</em> or simply scan and email to <em>info@multivisionfinancial.com</em>.</p>

<p>Once your paperwork is verified we will <strong>enable your online dashboard </strong> so you can start submitting deals.  If you have any questions please contact your Inhouse Agent (If you called in) or Jim Hewitt (Referral Agent Director) at <em>562-513-1961</em>.</p>

<p>Regards,</p>
<table><tr><td style="font-size: 14px;">
<div style="font-size: 14px; margin-top: 5px; margin-bottom: 5px;">O: 888-598-9951</div>
<div style="font-size: 14px; margin-top: 5px; margin-bottom: 5px;">F: 562-590-9393</div>
<div style="font-size: 14px; margin-top: 5px; margin-bottom: 5px;"> {{ HTML::mailto('info@multivisionfinancial.com') }}</span></td></tr>
<tr><td align="left"><img src="http://multivisionfinancial.com/images/horizontal-sig.jpg" width="150" height="50" /><br/><img style="margin-top: 5px; margin-right: 40px;" title="MVFLoans A+ BBB Rating" alt="MVFLoans A+ BBB Rating" src="http://multivisionfinancial.com/images/em/bbbA.jpg" width="184" height="40" /></td></tr>
</table>

<p style="font-size: 9px; font-color: #ccc;">DISCLAIMER: This message is proprietary to Multi Vision Financial, and is intended solely for the use of the individual to whom it is addressed. It may contain privileged or confidential information and should not be circulated or used for any purpose other than for what it is intended. If you have received this message in error, please notify the originator immediately. If you are not the intended recipient, you are notified that you are strictly prohibited from using, copying, altering, or disclosing the contents of this message. Multi Vision Financial accepts no responsibility for loss or damage arising from the use of the information transmitted by this email including damage from computer viruses.</p>