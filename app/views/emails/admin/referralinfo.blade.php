{{ HTML::image('assets/img/email/mvf-logo-e.jpg', 'MVF Logo', array('id' => 'MVFLogo')) }}

<h3>Referral Information and Consultation Request</h3>

<p>{{ $data['name'] }} has requested information and has been sent a Referral Information Email with links to sign up page and PDF's.</p>

<p>-----------------------------------------</p>
<h4>Requestor Details:</h4>
<p>Name: {{ $data['name'] }}</p>
<p>Email: {{ $data['email'] }}</p>
<p>Phone: {{ $data['phone'] }} </p>
<p>Message:</p>
<p>{{ $data['message'] }}</p>
<p>-----------------------------------------</p>

<p>MVFLoans.com</p>