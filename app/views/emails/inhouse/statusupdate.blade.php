{{ HTML::image('assets/img/email/mvf-logo-e.jpg', 'MVF Logo', array('id' => 'MVFLogo')) }}

<h2>New Comment</h2>

<p>Admin: {{$admin->first_name.' '.$admin->last_name}} has posted a new comment to client {{ $client->first_name.' '.$client->last_name }}</p>
<p>[Client Info] Business Name: {{ $client->business_name }} - Referral Agent: {{ $refAgent->first_name.' '.$refAgent->last_name }} </p>

<p><strong>Lastest Comment</strong></p>
<blockquote>"{{ $status->status_content }}"</blockquote>

<p>To view or reply to the comment <a href="{{ URL::route($statusRoute, array($client->id)) }}">click here</a>.</p>

<p>Regards,</p>
<p>Multi Vision Financial</p>

<p style="font-size: 8px; font-color: #ccc;">DISCLAIMER: This message is proprietary to Multi Vision Financial, and is intended solely for the use of the individual to whom it is addressed. It may contain privileged or confidential information and should not be circulated or used for any purpose other than for what it is intended. If you have received this message in error, please notify the originator immediately. If you are not the intended recipient, you are notified that you are strictly prohibited from using, copying, altering, or disclosing the contents of this message. Multi Vision Financial accepts no responsibility for loss or damage arising from the use of the information transmitted by this email including damage from computer viruses.</p>