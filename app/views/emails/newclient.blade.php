<h2>New Client Submission: </h2>

<p>Submitted by Agent: {{$agent->first_name.' '.$agent->last_name.' ('.$agent->office_id.')'}}</p>
<p>Assigned To: {{ $inhouse->first_name.' '.$inhouse->last_name }}</p>

<h3>Client Details:</h3>
<p>Name: {{ $client->first_name.' '.$client->last_name }}</p>
<p>Business Name: {{ $client->business_name }}</p>
<p>Email: {{ $client->email }}</p>
<p>Phone: {{ $client->phone }}</p>
<p>Message: {{ $client->client_message }}

<p>Click <a title="View New Client" href="{{ URL::route($statusRoute, array($client->id)) }}"> here </a> to post an update or view more information.</p>

<p>Multi Vision Financial</p>

<p style="font-size: 8px; font-color: #ccc;">DISCLAIMER: This message is proprietary to Multi Vision Financial, and is intended solely for the use of the individual to whom it is addressed. It may contain privileged or confidential information and should not be circulated or used for any purpose other than for what it is intended. If you have received this message in error, please notify the originator immediately. If you are not the intended recipient, you are notified that you are strictly prohibited from using, copying, altering, or disclosing the contents of this message. Multi Vision Financial accepts no responsibility for loss or damage arising from the use of the information transmitted by this email including damage from computer viruses.</p>