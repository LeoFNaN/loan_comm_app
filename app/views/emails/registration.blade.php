<h2>New Agent Signup: {{$user->first_name}} {{$user->last_name}}</h2>

<p>
	{{$user->first_name}} {{$user->last_name}} has been sent their introduction email, detailing the Contract Forms that need to be completed and faxed/emailed back.
</p>

<p>
	<strong>Contact Information:</strong> <br/>
	Name: {{$user->first_name.' '.$user->last_name}}<br/>
	Phone: {{$user->phone_number}}<br/>
	Email: {{$user->email}}<br/>
	Referred By: {{ $user->referral_source }}<br/>
	Reason: {{ $user->agent_reason }}
</p>

<p>Multi Vision Financial</p>

<p style="font-size: 8px; font-color: #ccc;">DISCLAIMER: This message is proprietary to Multi Vision Financial, and is intended solely for the use of the individual to whom it is addressed. It may contain privileged or confidential information and should not be circulated or used for any purpose other than for what it is intended. If you have received this message in error, please notify the originator immediately. If you are not the intended recipient, you are notified that you are strictly prohibited from using, copying, altering, or disclosing the contents of this message. Multi Vision Financial accepts no responsibility for loss or damage arising from the use of the information transmitted by this email including damage from computer viruses.</p>