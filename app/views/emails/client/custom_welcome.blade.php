<p>Hello {{$client->first_name}},</p>

<p>My name is {{ $inhouse->first_name }} with <strong>Multi Vision Financial</strong>.  I am reaching out to you on behalf of <strong>Business Loans Now</strong>.  I'm available to answer any questions you have via phone or email.  For your convenience, I have included my contact information below.</p>

<h4>If you wish to expedite your approval:</h4>

<p>1. Loan application completed by owner(s) - <a href="https://multivisionfinancial.com/pdf/MVF_Merchant_Application_v3.0.pdf" title="MVF Loan Application">Download PDF</a></p>

<p>2. Please gather at least 3 months of your business bank statements, all pages.</p>

<p>I look forward to speaking with you and hearing more about your business!</p>

<p>Regards,<br>
{{$inhouse->first_name.' '.$inhouse->last_name}} <br>
Phone: {{$inhouse->phone_number}} <br>
Email: {{$inhouse->email}}</p>

<p><strong>Our goal is and always will be to provide professional service with honesty and integrity.</strong></p>

<p style="font-size: 12px;">See why more business owners, brokers, bankers and financial professionals trust us for business loan approvals.  BusinessLoansNow is a direct affiliate of MultiVisionFinancial.com.</p>

<p style="font-size: 12px; font-color: #ccc;">Whether you choose Multi Vision Financial to be your Agent of record or not, we feel it is vital that you choose a reputable broker/agent to work with. A bad/fraudulent experience can ruin your future chances to obtain working capital. Also bad brokers are over stacking and hurting business owners. They do it for their own selfish gains, not caring about your true business success. Absolutely do not pay any company up front fees of any kind!</p>
