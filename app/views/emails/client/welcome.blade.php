<p>Hello {{$client->first_name}},</p>

<p>Thank you for your interest in getting a business loan. I would like to introduce myself. My name is {{$inhouse->first_name }} the Financial Capital Manager at <strong>Multi Vision Financial</strong>.  I am reaching out to you in response to your inquiry by your agent.</p>

<p>I am available to answer any questions you might have via phone or email.  For your convenience, I have included my contact information below.</p>

<h4>If you wish to expedite your approval:</h4>

<p>I will need at least the last <em>3 months of business bank statements (all pages)</em> and a <em>completed loan application (<a href="https://multivisionfinancial.com/pdf/MVF_Merchant_Application_v3.0.pdf"> Download PDF</a>)</em>.  This can speed up your approval and move you to the top of the list of other owners who are still gathering documents.</p>

<p>I look forward to speaking with you soon.</p>

<p>Regards,<br>
{{$inhouse->first_name.' '.$inhouse->last_name}} <br>
Phone: {{$inhouse->phone_number}} <br>
Email: {{$inhouse->email}}</p>

<p><strong>Our goal is and always will be to provide professional service with honesty and integrity.</strong></p>

<p style="font-size: 12px;">See why more business owners, brokers, bankers and financial professionals trust us for business loan approvals. </p>

<p style="font-size: 12px; font-color: #ccc;">Whether you choose Multi Vision Financial to be your Agent of record or not, we feel it is vital that you choose a reputable broker/agent to work with. A bad/fraudulent experience can ruin your future chances to obtain working capital. Also bad brokers are over stacking and hurting business owners. They do it for their own selfish gains, not caring about your true business success. Absolutely do not pay any company up front fees of any kind!</p>
