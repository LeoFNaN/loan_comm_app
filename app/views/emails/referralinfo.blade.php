{{ HTML::image('assets/img/email/mvf-logo-e.jpg', 'MVF Logo', array('id' => 'MVFLogo')) }}

<p>Hello,</p>

<p>Thank you for showing interest in the Multi Vision Financial Referral Program.  To get familiar with our program read our Referral Agent Opportunity program (<a href="https://www.dropbox.com/sh/z4bgzx1ayxk1h9h/AAC5YaeyjlFInEsGgeYP29-6a" title="Referral Agent Opportunity program" alt="Referral Agent Opportunity program">View PDF</a>) to learn more.</p>

<p>When you are ready to join our program head over to our <a href="{{URL::to('signup')}}">Sign Up</a> page to create your account.</p>

<p>Regards,</p>
<p>Multi Vision Financial</p>