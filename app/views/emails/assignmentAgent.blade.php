{{ HTML::image('assets/img/email/mvf-logo-e.jpg', 'MVF Logo', array('id' => 'MVFLogo')) }}

<h2>Inhouse Agent Assignment</h2>

<p>Hello {{ $agent->first_name }},</p>

<p>Your inhouse agent is {{ $inhouse->first_name.' '.$inhouse->last_name }}, if you have any questions or need help you may email {{ $inhouse->first_name }} at {{ $inhouse->email }} or call directly at {{ $inhouse->phone_number }}.  You should expect a call from your inhouse agent shortly.</p>

<p>Regards,</p>
<p>Multi Vision Financial</p>

<p style="font-size: 8px; font-color: #ccc;">DISCLAIMER: This message is proprietary to Multi Vision Financial, and is intended solely for the use of the individual to whom it is addressed. It may contain privileged or confidential information and should not be circulated or used for any purpose other than for what it is intended. If you have received this message in error, please notify the originator immediately. If you are not the intended recipient, you are notified that you are strictly prohibited from using, copying, altering, or disclosing the contents of this message. Multi Vision Financial accepts no responsibility for loss or damage arising from the use of the information transmitted by this email including damage from computer viruses.</p>