{{ HTML::image('assets/img/email/mvf-logo-e.jpg', 'MVF Logo', array('id' => 'MVFLogo')) }}

<h2>Your Account Is Activated!</h2>

<p>Hello {{ $agent->first_name }},</p>

<p>We have received all of your paperwork and you have been accepted as a Referral Agent of Multi Vision Financial.</p>
<p>You may now login to your <a href="{{ URL::to('/user/login') }}" title="MVFLoans Dashboard">dashboard</a> to view your Agent ID, your assigned Inhouse Agent and begin submitting clients to earn your referral commissions.</p>

<p>Regards,</p>
<p>Multi Vision Financial</p>
<p>888-598-9951</p>