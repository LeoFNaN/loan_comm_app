<h2>New Agent Assignment</h2>

<p>Hi {{ $inhouse->first_name }},</p>

<p>You have been assigned Agent {{ $agent->first_name.' '.$agent->last_name }}.  An email has been sent to your agent regarding your contact information.  Login to your dashboard to view your new agent and their clients.</p>

<p>Regards,</p>
<p>Multi Vision Financial</p>

<p style="font-size: 8px; font-color: #ccc;">DISCLAIMER: This message is proprietary to Multi Vision Financial, and is intended solely for the use of the individual to whom it is addressed. It may contain privileged or confidential information and should not be circulated or used for any purpose other than for what it is intended. If you have received this message in error, please notify the originator immediately. If you are not the intended recipient, you are notified that you are strictly prohibited from using, copying, altering, or disclosing the contents of this message. Multi Vision Financial accepts no responsibility for loss or damage arising from the use of the information transmitted by this email including damage from computer viruses.</p>