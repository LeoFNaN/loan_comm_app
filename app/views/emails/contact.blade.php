{{ HTML::image('assets/img/email/mvf-logo-e.jpg', 'MVF Logo', array('id' => 'MVFLogo')) }}

<h3>Contact Us Submission</h3>

<p>Name: {{ $data['name'] }}</p>
<p>Email: {{ $data['email'] }}</p>
<p>Phone: {{ $data['phone'] }}</p>
<p>Message: {{ $data['message'] }}</p>

<p>Regards,</p>
<p>Multi Vision Financial</p>