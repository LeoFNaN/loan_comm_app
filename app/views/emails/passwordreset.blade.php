<h1>{{ Lang::get('confide::confide.email.password_reset.subject') }}</h1>

<p>{{ Lang::get('confide::confide.email.password_reset.greetings', array( 'name' => $user['last_name'])) }},</p>

<p>{{ Lang::get('confide::confide.email.password_reset.body') }}</p>
<a href='{{{ (Auth::check('UserController@getReset', array($token))) ? : URL::to('user/reset/'.$token)  }}}'>
    {{{ (Auth::check('UserController@getReset', array($token))) ? : URL::to('user/reset/'.$token)  }}}
</a>

<p>{{ Lang::get('confide::confide.email.password_reset.farewell') }}</p>

<table><tr><td style="font-size: 14px;">
<div style="font-size: 14px; margin-top: 5px; margin-bottom: 5px;">O: 888-598-9951</div>
<div style="font-size: 14px; margin-top: 5px; margin-bottom: 5px;">F: 562-590-9393</div>
<div style="font-size: 14px; margin-top: 5px; margin-bottom: 5px;"><a href="mailto:info@MVFLoans.com" title="">info@MVFLoans.com</a></span></td></tr>
<tr><td align="left"><img src="http://multivisionfinancial.com/images/horizontal-sig.jpg" width="150" height="50" /><br/><img style="margin-top: 5px; margin-right: 40px;" title="MVFLoans A+ BBB Rating" alt="MVFLoans A+ BBB Rating" src="http://multivisionfinancial.com/images/em/bbbA.jpg" width="184" height="40" /></td></tr>
</table>

<p style="font-size: 8px; font-color: #ccc;">DISCLAIMER: This message is proprietary to Multi Vision Financial, and is intended solely for the use of the individual to whom it is addressed. It may contain privileged or confidential information and should not be circulated or used for any purpose other than for what it is intended. If you have received this message in error, please notify the originator immediately. If you are not the intended recipient, you are notified that you are strictly prohibited from using, copying, altering, or disclosing the contents of this message. Multi Vision Financial accepts no responsibility for loss or damage arising from the use of the information transmitted by this email including damage from computer viruses.</p>