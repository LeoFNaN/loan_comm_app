<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>
				MVFLoans - Site Under Maintenance
		</title>
		<meta name="keywords" content="maintenance, multi vision financial" />
		<meta name="author" content="Multi Vision Financial" />
		<meta name="description" content="We are performing scheduled maintenance.  We will be back soon." />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">

	        <link rel="stylesheet" href="{{asset('bootpaper/bootstrap.min.css')}}">
	        <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
	        <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/prettify.css')}}">
	        <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/bootstrap-wysihtml5.css')}}">
	        <link rel="stylesheet" href="{{asset('assets/css/datatables-bootstrap.css')}}">
	        <link rel="stylesheet" href="{{asset('assets/css/colorbox.css')}}">
	        <link rel="stylesheet" href="{{asset('assets/css/styles.css')}}">
	        <link rel="stylesheet" href="{{asset('assets/css/animate.css')}}">
	        <link rel="stylesheet" href="{{asset('assets/css/icomoon/style.css')}}">

	        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Favicons -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
		<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">
		<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">

	</head>

	<body>

		<div id="wrap">

				<div class="row text-center" style="margin-top: 20px;">
		              	<a title="Back to MVF Business Loans" alt="Back to MVF Business Loans" href="https://www.multivisionfinancial.com"><img title="Back to MVF Business Loans" alt="Back to MVF Business Loans" src="{{ asset('assets/img/mvf-logo-lrg.jpg') }}" width="351" height="80" /></a>
		            </div>

			<div class="text-center">

				<h2>Undergoing Maintenance</h2>
				<p>We are updating the processing center, we'll be up in a few hours. Thanks for your patience.</p>
				
			</div>

		</div>

        </body>

</html>
